﻿using System;
using System.Diagnostics;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// Helps logging entry and exit from a section
    /// </summary>
    public class SectionLogger : IDisposable
    {
        private const string SectionEntry = "SectionEntry";
        private const string SectionExit = "SectionExit";

        public static SectionLogger Enter(ILog log, string sectionName, string sectionFilePath, int sectionLineNumber, bool logElapsedTime = false, params string[] tags)
        {
            var result = new SectionLogger(log, sectionName, sectionFilePath, sectionLineNumber, logElapsedTime, tags);
            result.LogEnter();
            return result;
        }

        public SectionLogger(ILog log, string sectionName, string sectionFilePath, int sectionLineNumber, bool logElapsedTime = false, params string[] tags)
        {
            Log = log;
            SectionName = sectionName;
            SectionFilePath = sectionFilePath;
            SectionLineNumber = sectionLineNumber;
            LogElapsedTime = logElapsedTime;
            Tags = tags;

            if (logElapsedTime)
            {
                Stopwatch = new Stopwatch();
                Stopwatch.Start();
            }
        }

        private readonly ILog Log;
        private readonly string SectionName;
        private readonly string SectionFilePath;
        private readonly int SectionLineNumber;
        private readonly bool LogElapsedTime;
        private readonly string[] Tags;

        private readonly Stopwatch Stopwatch;

        public void LogEnter() =>
            Log.Log($"Entering section {SectionName}", null, SectionName, SectionFilePath, SectionLineNumber, Tags);

        public void LogExit()
        {
            if (Stopwatch == null)
            {
                LogInternal($"Exiting section {SectionName}");
            }
            else
            {
                Stopwatch.Stop();
                var elapsedTime = Stopwatch == null ? (TimeSpan?)null : Stopwatch.Elapsed;
                LogInternal($"Exiting section {SectionName}. Took {elapsedTime}.");
            }
        }

        private void LogInternal(string message) =>
            Log.Log(message, null, SectionName, SectionFilePath, SectionLineNumber, Tags);

        public void Dispose() => LogExit();
    }
}
