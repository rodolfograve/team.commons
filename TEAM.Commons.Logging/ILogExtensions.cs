﻿using System;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Logging
{
    public static class ILogExtensions
    {
        [Obsolete("Use LogMethod or LogSection instead")]
        public static SectionLogger MethodLogger(this ILog log, bool logElapsedTime = false, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags) =>
              LogMethod(log, logElapsedTime, callerMemberName, callerFilePath, callerLineNumber, tags);

        public static SectionLogger LogMethod(this ILog log, bool logElapsedTime = false, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags) =>
              SectionLogger.Enter(log, callerMemberName, callerFilePath, callerLineNumber, logElapsedTime, tags);

        public static SectionLogger LogSection(this ILog log, bool logElapsedTime = false, string sectionName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags) =>
              SectionLogger.Enter(log, sectionName, callerFilePath, callerLineNumber, logElapsedTime, tags);
    }
}
