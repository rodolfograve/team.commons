﻿using System;
using System.Diagnostics.Contracts;
using System.IO;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// Generates one log file per day
    /// </summary>
    public class DailyRollingFileLog : FileLogBase
    {
        public DailyRollingFileLog(
            string rootDirectory,
            string fileName,
            string fileExtension,
            ILogEntrySerializer logEntrySerializer = null,
            IDailyRollingFileLogDateTime dateTime = null,
            IStreamWriterFactory streamWriterFactory = null)
              : base(logEntrySerializer)
        {
            Contract.Requires(rootDirectory != null);
            Contract.Requires(fileName != null);
            Contract.Requires(fileExtension != null);

            BaseFileName = Path.Combine(rootDirectory, fileName + "_");
            FileExtension = fileExtension;
            DateTime = dateTime ?? new SystemDailyRollingFileLogDateTime();
            StreamWriterFactory = streamWriterFactory ?? new FileStreamWriterFactory();
        }

        private readonly string BaseFileName;
        private readonly string FileExtension;
        private readonly IDailyRollingFileLogDateTime DateTime;
        private readonly IStreamWriterFactory StreamWriterFactory;

        private DateTime LastDate;

        private StreamWriter StreamWriter;

        protected override TextWriter GetWriter()
        {
            var today = DateTime.Today;

            if (!LastDate.Equals(today))
            {
                LastDate = today;
                var newFileName = $"{BaseFileName}{today.ToString("yyyy-MM-dd")}.{FileExtension}";

                // TextWriter doesn't dipose the StreamWriter so we need to keep a reference to it for disposing it.
                // StreamWriter does dispose the FileStream so we don't need to keep a reference to that one.
                StreamWriter = StreamWriterFactory.Create(newFileName);
            }
            return StreamWriter;
        }

        protected override void ChildDispose() => StreamWriter?.Dispose();
    }
}
