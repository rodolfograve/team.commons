using System;

namespace TEAM.Commons.Logging
{
    public interface ILogReader : IDisposable
    {
        LogEntry ReadNextLogEntry();
    }
}
