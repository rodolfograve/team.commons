﻿using System;
using System.Diagnostics;
using System.Threading;

namespace TEAM.Commons.Logging
{
    public class LogEntry
    {
        public static LogEntry CreateFromContext(object content, Exception exception, Process process, Thread thread, string callerMemberName, string callerFilePath, int callerLineNumber, string[] tags)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = new LogEntry
            {
                Timestamp = DateTimeOffset.UtcNow,
                Tags = tags,
                Content = content,
                Exception = exception,
                CallerMemberName = callerMemberName,
                CallerFilePath = callerFilePath,
                CallerLineNumber = callerLineNumber,
            };
            try
            {
                result.ThreadId = thread.ManagedThreadId;
                result.ThreadName = thread.Name;
                result.ThreadIsAlive = thread.IsAlive;
                result.ThreadIsBackground = thread.IsBackground;
                result.ThreadIsPoolThread = thread.IsThreadPoolThread;
                result.ThreadState = thread.ThreadState;

                result.ProcessId = process.Id;
                result.ProcessName = process.ProcessName;
                result.ProcessMachineName = process.MachineName;
                result.ProcessTotalProcessorTime = process.TotalProcessorTime;
                result.ProcessUserProcessorTime = process.UserProcessorTime;
                result.ProcessVirtualMemorySize = process.VirtualMemorySize64;
                result.ProcessWorkingSet = process.WorkingSet64;
                result.ProcessThreadsCount = process.Threads.Count;
            }
            catch (InvalidOperationException)
            {
                // Ignore... under some conditions it's not possible to obtain information from a process or thread.
                // There's also no way to capture the information atomically so it seems sensible to try/catch ignoring the exception
            }
            stopwatch.Stop();
            result.EntryCreationTimeCost = stopwatch.Elapsed;
            return result;
        }

        private LogEntry()
        {
        }

        public LogEntry(
            DateTimeOffset timestamp,
            string[] tags,
            object content,
            Exception exception,
            string callerMemberName,
            string callerFilePath,
            int callerLineNumber,
            int threadId,
            string threadName,
            bool threadIsAlive,
            bool threadIsBackground,
            bool threadIsPoolThread,
            System.Threading.ThreadState threadState,
            int? processId,
            string processName,
            string processMachineName,
            TimeSpan? processTotalProcessorTime,
            TimeSpan? processUserProcessorTime,
            long? processVirtualMemorySize,
            long? processWorkingSet,
            int? processThreadsCount,
            StackTrace stackTrace
            )
        {
            Timestamp = timestamp;
            Tags = tags;
            Content = content;
            Exception = exception;
            CallerMemberName = callerMemberName;
            CallerFilePath = callerFilePath;
            CallerLineNumber = callerLineNumber;
            ThreadId = threadId;
            ThreadName = threadName;
            ThreadIsAlive = threadIsAlive;
            ThreadIsBackground = threadIsBackground;
            ThreadIsPoolThread = threadIsPoolThread;
            ThreadState = threadState;
            ProcessId = processId;
            ProcessName = processName;
            ProcessMachineName = processMachineName;
            ProcessTotalProcessorTime = processTotalProcessorTime;
            ProcessUserProcessorTime = processUserProcessorTime;
            ProcessVirtualMemorySize = processVirtualMemorySize;
            ProcessWorkingSet = processWorkingSet;
            ProcessThreadsCount = processThreadsCount;
            StackTrace = stackTrace;
        }

        public DateTimeOffset Timestamp { get; set;}
        public string[] Tags { get; set;}
        public object Content { get; set;}
        public Exception Exception { get; set;}
        public TimeSpan EntryCreationTimeCost { get; set;}
        public string CallerMemberName { get; set;}
        public string CallerFilePath { get; set;}
        public int CallerLineNumber { get; set;}
        public int ThreadId { get; set;}
        public string ThreadName { get; set;}
        public bool ThreadIsAlive { get; set;}
        public bool ThreadIsBackground { get; set;}
        public bool ThreadIsPoolThread { get; set;}
        public System.Threading.ThreadState ThreadState { get; set;}
        public int? ProcessId { get; set;}
        public string ProcessName { get; set;}
        public string ProcessMachineName { get; set;}
        public TimeSpan? ProcessTotalProcessorTime { get; set;}
        public TimeSpan? ProcessUserProcessorTime { get; set;}
        public long? ProcessVirtualMemorySize { get; set;}
        public long? ProcessWorkingSet { get; set;}
        public int? ProcessThreadsCount { get; set;}

        public StackTrace StackTrace { get; set;}
    }
}
