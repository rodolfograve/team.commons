﻿using System;

namespace TEAM.Commons.Logging
{
    public class SystemDailyRollingFileLogDateTime : IDailyRollingFileLogDateTime
    {
        public DateTime Today => DateTime.Today;
    }
}
