﻿using System;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// Allows for the composition of logs
    /// </summary>
    public class CompositeLog : ILog
    {
        /// <summary>
        /// Creates an instance of this class which will log to all the <paramref name="logs"/>
        /// </summary>
        /// <param name="logs">The logs to which this instance should log.</param>
        public CompositeLog(params ILog[] logs)
        {
            Logs = logs;
        }

        private readonly ILog[] Logs;

        public void Log(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags)
        {
            foreach (var log in Logs)
            {
                log.Log(content, exception, callerMemberName, callerFilePath, callerLineNumber, tags);
            }
        }

        /// <summary>
        /// Disposes all the <see cref="Logs"/>
        /// </summary>
        public void Dispose()
        {
            foreach (var log in Logs)
            {
                log.Dispose();
            }
        }
    }
}
