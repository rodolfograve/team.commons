﻿using System.IO;

namespace TEAM.Commons.Logging
{
    public interface IStreamWriterFactory
    {
        StreamWriter Create(string fileName);
    }
}
