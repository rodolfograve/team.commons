﻿using Newtonsoft.Json;
using System.IO;

namespace TEAM.Commons.Logging
{
    public class JsonLogEntrySerializer : ILogEntrySerializer
    {
        public JsonLogEntrySerializer(JsonSerializer serializer)
        {
            Serializer = serializer;
        }

        private readonly JsonSerializer Serializer;

        public void Serialize(LogEntry logEntry, TextWriter textWriter) =>
            Serializer.Serialize(textWriter, logEntry);
    }
}
