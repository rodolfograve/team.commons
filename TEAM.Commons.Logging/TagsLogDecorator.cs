﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// Adds a set of tags to all log entries.
    /// </summary>
    public class TagsLogDecorator : LogDecoratorBase
    {
        public TagsLogDecorator(ILog toDecorate, bool disposeToDecorate = true, params string[] tags)
            : base(toDecorate, disposeToDecorate)
        {
            Tags = tags;
        }

        private readonly string[] Tags;

        public override void Log(object content, Exception exception = null, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = 0, params string[] tags) =>
            ToDecorate.Log(content, exception, callerMemberName, callerFilePath, callerLineNumber, tags.Length == 0 ? Tags : Tags.Concat(tags).ToArray());
    }
}
