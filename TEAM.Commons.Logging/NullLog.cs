﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TEAM.Commons.Logging
{
    public class NullLog : ILog
    {
        public void Log(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags)
        {
            // Do nothing
        }

        public Task LogAsync(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags)
        {
            return Task.Run(() => { });
        }

        public void Dispose()
        {
            // Do nothing
        }
    }
}
