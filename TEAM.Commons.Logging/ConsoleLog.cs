﻿using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TEAM.Commons.Logging
{
    public class ConsoleLog : ILog
    {
        private static readonly ConsoleColor[] DefaultColors = new[] { ConsoleColor.White, ConsoleColor.Cyan, ConsoleColor.Magenta, ConsoleColor.Yellow, ConsoleColor.Green };

        public static ConsoleLog WithRandomColorsPerTag(params ConsoleColor[] colors)
        {
            if (colors.Length == 0)
            {
                colors = DefaultColors;
            }
            var map = new ConcurrentDictionary<string, ConsoleColor>();
            return new ConsoleLog(x => map.GetOrAdd(x, colors[map.Values.Count % colors.Length]));
        }

        public ConsoleLog(Func<string, ConsoleColor> tagsToColor = null)
        {
            TagsToColor = tagsToColor;
            InitialColor = Console.ForegroundColor;
        }

        private readonly ConsoleColor InitialColor;
        private readonly Func<string, ConsoleColor> TagsToColor;

        private bool IsDisposed;

        public void Log(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags)
        {
            CheckDispose();
            LogInternal(content, exception, tags);
        }

        public async Task LogAsync(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags)
        {
            CheckDispose();
            await Task.Run(() => LogInternal(content, exception, tags));
        }

        private void LogInternal(object content, Exception exception, string[] tags)
        {
            var needsColor = TagsToColor != null && tags.Length > 0;
            if (needsColor)
            {
                Console.ForegroundColor = TagsToColor(tags[0]);
            }
            if (exception == null)
            {
                Console.WriteLine(content);
            }
            else
            {
                Console.WriteLine(content + "-> EXCEPTION: " + exception);
            }
            if (needsColor)
            {
                Console.ForegroundColor = InitialColor;
            }
        }

        private void CheckDispose()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }


        public void Dispose()
        {
            Log($"Disposing this {GetType().Name} instance. Any attempts to log after this will fail");
            IsDisposed = true;
        }
    }
}
