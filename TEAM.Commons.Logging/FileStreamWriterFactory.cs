﻿using System.IO;

namespace TEAM.Commons.Logging
{
    public class FileStreamWriterFactory : IStreamWriterFactory
    {
        public StreamWriter Create(string fileName) =>
            new StreamWriter(File.Open(fileName, FileMode.Append, FileAccess.Write, FileShare.Read));
    }
}
