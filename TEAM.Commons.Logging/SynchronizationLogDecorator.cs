﻿using System;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// Makes an ILog safe to use from multiple threads
    /// </summary>
    public class SynchronizationLogDecorator : LogDecoratorBase
    {
        /// <summary>
        /// Creates an instance of this class
        /// </summary>
        /// <param name="toDecorate"></param>
        /// <param name="disposeToDecorate">Are we responsible for disposing <paramref name="toDecorate"/>?</param>
        public SynchronizationLogDecorator(ILog toDecorate, bool disposeToDecorate = true)
            : base(toDecorate, disposeToDecorate)
        {
        }

        private readonly object Lock = new object();

        /// <summary>
        /// Logs making sure only one thread is logging on this instance at a time.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="exception"></param>
        /// <param name="callerMemberName"></param>
        /// <param name="callerFilePath"></param>
        /// <param name="callerLineNumber"></param>
        /// <param name="tags"></param>
        public override void Log(object content, Exception exception = null, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = 0, params string[] tags)
        {
            lock (Lock)
            {
                ToDecorate.Log(content, exception, callerMemberName, callerFilePath, callerLineNumber, tags);
            }
        }

        /// <summary>
        /// Disposes this instance and ToDecorate if DisposeToDecorate is true
        /// </summary>
        public override void Dispose()
        {
            lock (Lock)
            {
                base.Dispose();
            }
        }
    }
}
