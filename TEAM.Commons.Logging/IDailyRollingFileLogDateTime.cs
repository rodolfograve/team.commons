﻿using System;

namespace TEAM.Commons.Logging
{
    public interface IDailyRollingFileLogDateTime
    {

        DateTime Today { get; }

    }
}
