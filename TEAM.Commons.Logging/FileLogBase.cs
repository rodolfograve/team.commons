﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// Not thread safe. Use a SynchronizationLogDecorator to create a thread-safe log.
    /// </summary>
    public abstract class FileLogBase : ILog
    {
        protected FileLogBase(ILogEntrySerializer logEntrySerializer = null)
        {
            LogEntrySerializer = logEntrySerializer ?? new JsonLogEntrySerializer(JsonSerializer.CreateDefault());
            CurrentProcess = Process.GetCurrentProcess();
        }

        protected readonly ILogEntrySerializer LogEntrySerializer;
        protected readonly Process CurrentProcess;
        protected string CurrentFileName;
        protected bool IsDisposed;

        protected abstract TextWriter GetWriter();

        public void Log(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags)
        {
            CheckDispose();
            var newEntry = LogEntry.CreateFromContext(content, exception, CurrentProcess, Thread.CurrentThread, callerMemberName, callerFilePath, callerLineNumber, tags);
            try
            {
                var writer = GetWriter();
                LogEntrySerializer.Serialize(newEntry, writer);
                writer.WriteLine();
                writer.Flush();
            }
            catch (Exception ex)
            {
                throw new LoggingFailedException(newEntry, ex);
            }
        }

        protected void CheckDispose()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                ChildDispose();
                CurrentProcess?.Dispose();
            }
        }

        protected abstract void ChildDispose();
    }
}
