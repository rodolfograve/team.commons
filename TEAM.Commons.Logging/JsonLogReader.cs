﻿using Newtonsoft.Json;
using System.IO;

namespace TEAM.Commons.Logging
{
    public class JsonLogReader : LogReaderBase
    {

        public JsonLogReader(string filePath)
        {
            FileReader = new StreamReader(File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            Serializer = JsonSerializer.CreateDefault();
        }

        private readonly StreamReader FileReader;
        private readonly JsonSerializer Serializer;

        protected override string ReadNextLogEntryString() => FileReader.ReadLine();

        protected override LogEntry BuildLogEntryFromLine(string line) => JsonConvert.DeserializeObject<LogEntry>(line);

        public override void Dispose() => FileReader?.Dispose();
    }
}
