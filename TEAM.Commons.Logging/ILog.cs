﻿using System;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Logging
{
    /// <summary>
    /// A modern interface for logging
    /// </summary>
    public interface ILog : IDisposable
   {
        /// <summary>
        /// Logs an object and an (optional) associated exception.
        /// </summary>
        /// <param name="content">The content of the log entry</param>
        /// <param name="exception">The (optional) associated exception</param>
        /// <param name="callerMemberName"></param>
        /// <param name="callerFilePath"></param>
        /// <param name="callerLineNumber"></param>
        /// <param name="tags">Any tags associated with the entry</param>
        void Log(object content, Exception exception = null, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0, params string[] tags);
    }
}
