﻿using System;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Logging
{
    public abstract class LogDecoratorBase : ILog
    {
        protected LogDecoratorBase(ILog toDecorate, bool disposeToDecorate)
        {
            if (toDecorate == null) throw new ArgumentNullException(nameof(toDecorate));

            ToDecorate = toDecorate;
            DisposeToDecorate = disposeToDecorate;
        }

        /// <summary>
        /// The log being decorated
        /// </summary>
        protected readonly ILog ToDecorate;

        /// <summary>
        /// Are we responsible for disposing the <see cref="ToDecorate"/> instance?
        /// </summary>
        protected readonly bool DisposeToDecorate;

        public abstract void Log(object content, Exception exception = null, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = 0, params string[] tags);

        public virtual void Dispose()
        {
            if (DisposeToDecorate)
            {
                ToDecorate.Dispose();
            }
        }
    }
}
