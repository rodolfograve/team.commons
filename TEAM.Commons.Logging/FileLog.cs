﻿using System;
using System.IO;

namespace TEAM.Commons.Logging
{
    public class FileLog : FileLogBase
    {
        public FileLog(string filePath, ILogEntrySerializer logEntrySerializer)
              : base(logEntrySerializer)
        {
            FilePath = filePath;
        }

        private readonly string FilePath;
        private TextWriter Writer;
        private readonly object WriterLock = new object();

        protected override TextWriter GetWriter()
        {
            if (Writer == null)
            {
                Writer = new StreamWriter(File.Open(FilePath, FileMode.Append, FileAccess.Write, FileShare.Read));
            }
            return Writer;
        }

        protected override void ChildDispose() => Writer?.Dispose();
    }
}
