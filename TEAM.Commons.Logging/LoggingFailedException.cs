﻿using System;

namespace TEAM.Commons.Logging
{
    public class LoggingFailedException : Exception
    {

        public LoggingFailedException(LogEntry failedLogEntry, Exception innerException)
            : base ($"Failed to log [{Format(failedLogEntry)}]", innerException)
        {
        }

        private static string Format(LogEntry logEntry)
        {
            return $"{logEntry.CallerMemberName}@({logEntry.CallerFilePath}:{logEntry.CallerLineNumber}) - {logEntry.Content} - {logEntry.Exception}";
        }

    }
}
