﻿using System.IO;

namespace TEAM.Commons.Logging
{
    public interface ILogEntrySerializer
    {

        /// <summary>
        /// Serializes the logEntry in the textWriter
        /// </summary>
        void Serialize(LogEntry logEntry, TextWriter textWriter);

    }
}
