﻿using System;

namespace TEAM.Commons.Logging
{
    public abstract class LogReaderBase : ILogReader
    {

        protected LogReaderBase()
        {
            NextEntryNumberToRead = 1;
        }

        private int NextEntryNumberToRead;

        protected abstract string ReadNextLogEntryString();

        protected abstract LogEntry BuildLogEntryFromLine(string line);

        public LogEntry ReadNextLogEntry()
        {
            var entryStr = ReadNextLogEntryString();
            try
            {
                LogEntry result = null;
                if (entryStr != null)
                {
                    result = BuildLogEntryFromLine(entryStr);
                }
                NextEntryNumberToRead++;
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to parse entry #{NextEntryNumberToRead}={entryStr}", ex);
            }
        }

        public virtual void Dispose()
        {
        }

    }
}
