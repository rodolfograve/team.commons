﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    /// <summary>
    /// Transport used to send messages.
    /// </summary>
    [ContractClass(typeof(ISendingTransportContract))]
    public interface ISendingTransport
    {
        /// <summary>
        /// Sends a message to the configured endpoint.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <returns>The transport message that got sent.</returns>
        TransportMessage Send(object message);

        /// <summary>
        /// Sends all the specified messages to the configured endpoint.
        /// </summary>
        /// <param name="messages">The messages to send.</param>
        /// <returns>The transport message that got sent.</returns>
        TransportMessage Send(object[] messages);

    }

    [ContractClassFor(typeof(ISendingTransport))]
    public abstract class ISendingTransportContract : ISendingTransport
    {
       public TransportMessage Send(object message)
       {
           Contract.Requires(message != null);
           Contract.Ensures(Contract.Result<TransportMessage>() != null);
           Contract.Ensures(Contract.Result<TransportMessage>().Messages[0] == message);
           return default(TransportMessage);
       }

       public TransportMessage Send(object[] messages)
       {
           Contract.Requires(messages != null);
           Contract.Requires(messages.Length > 0);
           Contract.Ensures(Contract.Result<TransportMessage>() != null);
           Contract.Ensures(Contract.Result<TransportMessage>().Messages == messages);
           return default(TransportMessage);
       }
    }

}
