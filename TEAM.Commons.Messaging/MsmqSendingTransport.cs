﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging.Msmq
{

    /// <summary>
    /// Transport used to send messages to a non-transactional MSMQ queue.
    /// </summary>
    public class MsmqSendingTransport : MsmqTransportBase, ISendingTransport
    {

       public MsmqSendingTransport(string outputQueuePath, IMessageSerializer messageSerializer)
          : base(outputQueuePath, messageSerializer)
       {
          Contract.Requires(!String.IsNullOrEmpty(outputQueuePath), "outputQueuePath is null or empty.");
          Contract.Requires(messageSerializer != null, "messageSerializer is null.");
       }

        public virtual TransportMessage Send(object message)
        {
            return Send(new object[1] { message });
        }

        public virtual TransportMessage Send(object[] messages)
        {
            var transportMessage = BuildTransportMessage(messages);
            SendTransportMessage(transportMessage);
            return transportMessage;
        }

        protected virtual void SendTransportMessage(TransportMessage transportMessage)
        {
           Contract.Requires(transportMessage != null, "transportMessage is null.");
           
           var msmqMessage = BuildMsmqMessage(transportMessage);
           SendMsmqMessage(msmqMessage);
           transportMessage.UniqueId = msmqMessage.Id;
        }

        protected virtual Message BuildMsmqMessage(TransportMessage transportMessage)
        {
            Contract.Requires(transportMessage != null, "transportMessage is null.");
            Contract.Requires(transportMessage.Messages != null);
            Contract.Requires(transportMessage.Messages.Length > 0);
            Contract.Ensures(Contract.Result<Message>() != null);
           
           var result = new Message();
           result.Label = "TEAM.Messaging-" + transportMessage.Messages[0].GetType().Name;
           result.Recoverable = true;

           var serializedMessage = MessageSerializer.Serialize(transportMessage);

           result.BodyStream.Write(serializedMessage, 0, serializedMessage.Length);
           return result;
        }

        protected virtual void SendMsmqMessage(Message message)
        {
           Contract.Requires(message != null, "message is null.");
           
           Queue.Send(message, MessageQueueTransactionType.Automatic);
        }

        protected virtual TransportMessage BuildTransportMessage(object[] messages)
        {
           Contract.Requires(messages != null && messages.Length != 0, "messages is null or empty.");
           Contract.Ensures(Contract.Result<TransportMessage>() != null);
           var result = TransportMessage.Create(messages);
           return result;
        }

        protected override MessageQueue CreateQueue(string queuePath)
        {
           return new MessageQueue(queuePath, QueueAccessMode.Send);
        }
    }
}
