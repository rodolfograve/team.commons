﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    [ContractClass(typeof(IMessageHandlersContainerContract))]
    public interface IMessageHandlersContainer
    {
        /// <summary>
        /// Gets an instance of the registered message handler for the specified message.
        /// </summary>
        /// <param name="message">The message for which a message handler must be returned.</param>
        /// <param name="processingContext">The processing context.</param>
        /// <returns>The instance of the message handler. Throws an exception when it cannot be found.</returns>
        MessageHandlerInfo GetMessageHandlerFor(object message, IMessageProcessingContext processingContext);
    }

    [ContractClassFor(typeof(IMessageHandlersContainer))]
    public abstract class IMessageHandlersContainerContract : IMessageHandlersContainer
    {
        public MessageHandlerInfo GetMessageHandlerFor(object message, IMessageProcessingContext processingContext)
        {
            Contract.Requires(message != null);
            Contract.Requires(processingContext != null);
            Contract.Ensures(Contract.Result<MessageHandlerInfo>() != null);
            return default(MessageHandlerInfo);
        }
    }

}
