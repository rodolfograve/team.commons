﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging.Msmq
{
    [ContractClass(typeof(MsmqTransportBaseContract))]
    public abstract class MsmqTransportBase : IDisposable
    {

       public MsmqTransportBase(string queuePath, IMessageSerializer messageSerializer)
       {
          Contract.Requires(!String.IsNullOrEmpty(queuePath), "queuePath is null or empty.");
          Contract.Requires(messageSerializer != null, "messageSerializer is null.");

          if (!queuePath.Contains("DIRECT="))
          { // We can only check if the queue exists if it's on the local machine. This is not the best way to tell but it's a quick fix. Need to re-visit and do it properly.
             if (!MessageQueue.Exists(queuePath))
             {
                throw new InvalidOperationException("Can't find queue with path '" + queuePath +
                                                    "'. Can't create a MsmqTransport on a non-existing queue. Check that the queue exists and that this process has the permissions required to access it.");
             }
          }
          Queue = CreateQueue(queuePath);
          MessageSerializer = messageSerializer;
       }

        protected readonly MessageQueue Queue;
        protected readonly IMessageSerializer MessageSerializer;

        public string QueuePath { get { return Queue.Path; } }

        protected abstract MessageQueue CreateQueue(string queuePath);

        public virtual void Dispose()
        {
            if (Queue != null)
            {
                Queue.Close();
            }
        }

    }

    [ContractClassFor(typeof(MsmqTransportBase))]
    public abstract class MsmqTransportBaseContract : MsmqTransportBase
    {
        public MsmqTransportBaseContract(string queuePath, IMessageSerializer messageSerializer)
            : base(queuePath, messageSerializer)
        {
        }

        protected override MessageQueue CreateQueue(string queuePath)
        {
            Contract.Requires(!String.IsNullOrEmpty(queuePath), "queuePath is null or empty.");
            Contract.Ensures(Contract.Result<MessageQueue>() != null);
            return default(MessageQueue);
        }
    }

}
