﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace TEAM.Commons.Messaging
{
    public class MaxRetryAttemptsErrorHandlingStrategy : IErrorHandlingStrategy
    {

        public MaxRetryAttemptsErrorHandlingStrategy(int maxRetryAttempts, TimeSpan delayBetweenRetryAttempts)
        {
            MaxRetryAttempts = maxRetryAttempts;
            DelayBetweenRetryAttempts = delayBetweenRetryAttempts;
            CanBeRetried = false;
        }

        public readonly int MaxRetryAttempts;
        public readonly TimeSpan DelayBetweenRetryAttempts;

        public bool CanBeRetried { get; protected set; }

        public void ProcessFailure(object message, Exception ex, IMessageProcessingContext processingContext)
        {
            CanBeRetried = processingContext.RetryCount < MaxRetryAttempts;
            if (CanBeRetried && !TimeSpan.Zero.Equals(DelayBetweenRetryAttempts))
            {
                Thread.Sleep(DelayBetweenRetryAttempts);
            }
        }
    }
}
