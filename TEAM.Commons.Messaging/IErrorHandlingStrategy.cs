﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    public interface IErrorHandlingStrategy
    {
        bool CanBeRetried { get; }

        void ProcessFailure(object message, Exception ex, IMessageProcessingContext processingContext);
    }
}
