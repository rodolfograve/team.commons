﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging
{
    public static class TypeExtensions
    {

       public static IEnumerable<Type> GetImplementedMessageHandlers(this Type target)
       {
          Contract.Requires(target != null, "target is null.");

          var genericMessageHandlerType = typeof(IMessageHandler<>);
          return target.GetInterfaces().Where(x => x.IsGenericType && x.GetGenericTypeDefinition() == genericMessageHandlerType);
       }

    }
}
