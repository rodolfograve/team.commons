﻿using System;
using Autofac;

namespace TEAM.Commons.Messaging
{
    public class AutofacMessageHandlersContainerAdapter : IMessageHandlersContainer
    {
        public AutofacMessageHandlersContainerAdapter(IContainer container)
        {
            Container = container;
        }

        protected readonly IContainer Container;

        public virtual MessageHandlerInfo GetMessageHandlerFor(object message, IMessageProcessingContext processingContext)
        {
            try
            {
                var messageType = message.GetType();
                var messageHandlerType = typeof(IMessageHandler<>);
                messageHandlerType = messageHandlerType.MakeGenericType(messageType);
                var handlerInstance = Container.Resolve(messageHandlerType);

                var handleMethodInfo = messageHandlerType.GetMethod("Handle", new[] { messageType, typeof(IMessageProcessingContext) });
                return new MessageHandlerInfo(
                                                () => handleMethodInfo.Invoke(handlerInstance, new[] { message, processingContext }),
                                                handlerInstance
                                             );
            }
            catch (Exception ex)
            {
                throw new MessageHandlerNotFoundException(message, ex);
            }
        }
    }
}
