﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging.InProcess
{
    /// <summary>
    /// Thread-safe implementation that allows in-process message-based communication.
    /// </summary>
    public class InProcessTransport : ISendingTransport, IReceivingTransport, IDisposable
    {
        public InProcessTransport()
        {
            ReceptionQueue = new BlockingCollection<TransportMessage>();
        }

        protected readonly BlockingCollection<TransportMessage> ReceptionQueue;

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
           Contract.Invariant(ReceptionQueue != null);
        }

        public virtual TransportMessage Send(object message)
        {
            return Send(new object[1] { message });
        }

        public virtual TransportMessage Send(object[] messages)
        {
            var transportMessage = TransportMessage.Create(messages);
            transportMessage.UniqueId = Guid.NewGuid().ToString();

            lock (ReceptionQueue)
            {
                ReceptionQueue.Add(transportMessage);
                Monitor.Pulse(ReceptionQueue);
            }
            return transportMessage;
        }

        public virtual TransportMessage WaitForMessage(TimeSpan timeout)
        {
            TransportMessage result = null;
            if (ReceptionQueue.TryTake(out result, timeout))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Number of messages waiting to be received.
        /// </summary>
        public virtual int PendingMessagesCount
        {
            get { return ReceptionQueue.Count; }
        }

        public virtual bool BlockUntilMessageIsAvailable(TimeSpan timeout)
        {
            var result = ReceptionQueue.Count > 0;
            if (ReceptionQueue.Count == 0)
            {
                lock (ReceptionQueue)
                {
                    if (Monitor.Wait(ReceptionQueue, timeout))
                    {
                        result = ReceptionQueue.Count > 0;
                    }
                }
            }
            return result;
        }

        public virtual TransportMessage TryReceiveCurrentAvailableMessage()
        {
            TransportMessage result = null;
            ReceptionQueue.TryTake(out result);
            return result;
        }

        public void Dispose()
        {
            if (ReceptionQueue != null)
                ReceptionQueue.Dispose();
        }

    }
}
