﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    /// <summary>
    /// Transport used to receive messages from an endpoint.
    /// </summary>
    [ContractClass(typeof(IReceivingTransportContract))]
    public interface IReceivingTransport
    {

        /// <summary>
        /// Blocks the current thread until a message is received or <paramref name="timeout"/> elapses.
        /// </summary>
        /// <param name="timeout">The max time span to wait before returning null because no message was available.</param>
        /// <returns>The received message or null if no message is received before <paramref name="timeout"/> elapses.</returns>
        TransportMessage WaitForMessage(TimeSpan timeout);

        /// <summary>
        /// Blocks the calling thread until a message becomes available or the specified timeout occurs. Use this method to avoid having an open transaction while waiting for a message to arrive.
        /// </summary>
        /// <param name="timeout">The max time span to wait before returning false because no message was available.</param>
        /// <returns>True if there is a message available. False if it's not and the timeout occurred.</returns>
        bool BlockUntilMessageIsAvailable(TimeSpan timeout);

        /// <summary>
        /// Attempts to get the current available message if it's still available. Returns null if it's not. Use this method when you want to receive messages in a transaction.
        /// </summary>
        /// <returns>The first available transport message. Null if there isn't any.</returns>
        TransportMessage TryReceiveCurrentAvailableMessage();
    }

    [ContractClassFor(typeof(IReceivingTransport))]
    public abstract class IReceivingTransportContract : IReceivingTransport
    {
        public TransportMessage WaitForMessage(TimeSpan timeout)
        {
            return default(TransportMessage);
        }

        public bool BlockUntilMessageIsAvailable(TimeSpan timeout)
        {
            return default(bool);
        }

        public void BlockUntilMessageIsAvailable()
        {
        }

        public TransportMessage TryReceiveCurrentAvailableMessage()
        {
            return default(TransportMessage);
        }
    }

}
