﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageSentEventArgs : MessageSendingEventArgsBase
    {

        public MessageSentEventArgs(DateTime timestampUtc, object message, TransportMessage transportMessage, MessageRoutingInfo routingInfo)
            : base (timestampUtc, message, transportMessage, routingInfo)
        {

        }

    }
}
