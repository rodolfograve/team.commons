﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    public class CriticalMessageBusException : ApplicationException
    {

        public CriticalMessageBusException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }
}
