﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public abstract class EventArgsBase : EventArgs
    {

        public EventArgsBase(DateTime timestampUtc)
        {
            TimestampUtc = timestampUtc;
        }

        public readonly DateTime TimestampUtc;

    }
}
