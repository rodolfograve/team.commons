﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    [ContractClass(typeof(IMessageHandlerContract<>))]
    public interface IMessageHandler<TMessage>
    {
        void Handle(TMessage message, IMessageProcessingContext processingContext);
    }

    [ContractClassFor(typeof(IMessageHandler<>))]
    public abstract class IMessageHandlerContract<TMessage> : IMessageHandler<TMessage>
    {
       public void Handle(TMessage message, IMessageProcessingContext processingContext)
       {
          Contract.Requires(message != null);
          Contract.Requires(processingContext != null);
       }
    }

}
