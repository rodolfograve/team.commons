﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace TEAM.Commons.Messaging.InProcess
{
    /// <summary>
    /// Thread-safe implementation that allows inter-process communications.
    /// </summary>
    public class InProcessTransportWithSerializer : ISendingTransport, IReceivingTransport
    {
        public InProcessTransportWithSerializer(IMessageSerializer messageSerializer)
        {
            if (messageSerializer == null) throw new ArgumentNullException("messageSerializer");
            MessageSerializer = messageSerializer;
            ReceptionQueue = new BlockingCollection<byte[]>();
        }

        protected readonly IMessageSerializer MessageSerializer;
        protected readonly BlockingCollection<byte[]> ReceptionQueue;

        public virtual TransportMessage Send(object message) => Send(new object[1] { message });

        public virtual TransportMessage Send(object[] messages)
        {
            var transportMessage = TransportMessage.Create(messages);
            transportMessage.UniqueId = Guid.NewGuid().ToString();

            lock (ReceptionQueue)
            {
                var serializedMessageBytes = MessageSerializer.Serialize(transportMessage);
                ReceptionQueue.Add(serializedMessageBytes);
                Monitor.Pulse(ReceptionQueue);
            }
            return transportMessage;
        }

        public virtual TransportMessage WaitForMessage(TimeSpan timeout)
        {
            byte[] result = null;
            if (ReceptionQueue.TryTake(out result, timeout))
            {
                var transportMessage = MessageSerializer.Deserialize(result);
                return transportMessage;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Number of messages waiting to be received.
        /// </summary>
        public virtual int PendingMessagesCount => ReceptionQueue.Count;

        public virtual bool BlockUntilMessageIsAvailable(TimeSpan timeout)
        {
            var result = ReceptionQueue.Count > 0;
            if (ReceptionQueue.Count == 0)
            {
                lock (ReceptionQueue)
                {
                    if (Monitor.Wait(ReceptionQueue, timeout))
                    {
                        result = ReceptionQueue.Count > 0;
                    }
                }
            }
            return result;
        }

        public virtual TransportMessage TryReceiveCurrentAvailableMessage()
        {
            byte[] result = null;
            ReceptionQueue.TryTake(out result);
            if (result == null) return null;
            else
            {
                var transportMessage = MessageSerializer.Deserialize(result);
                return transportMessage;
            }
        }
    }
}
