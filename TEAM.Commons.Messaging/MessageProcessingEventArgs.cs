﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageProcessingEventArgs : MessageProcessingEventArgsBase
    {

        public MessageProcessingEventArgs(DateTime timestampUtc, object message, TransportMessage transportMessage, MessageHandlerInfo handler)
            : base(timestampUtc, message, transportMessage, handler)
        {
        }

    }
}
