﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging
{
    public class EndpointNotFoundByIdException : ApplicationException
    {

       public EndpointNotFoundByIdException(object id, Exception innerException)
          : base("Couldn't find the MessageRoutingInfo for endpoint with Id='" + id + "'. Did you add this endpoint to this DictionaryMessageMap?", innerException)
       {
          Contract.Requires(id != null, "id is null.");
          Contract.Requires(innerException != null, "innerException is null.");
       }

    }
}
