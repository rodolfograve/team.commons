﻿using System.Reflection;

[assembly: AssemblyTitle("TEAM.Commons.Messaging")]
[assembly: AssemblyDescription("TEAM's inter-process communication tools.")]
