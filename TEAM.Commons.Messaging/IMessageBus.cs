﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace TEAM.Commons.Messaging
{
    [ContractClass(typeof(IMessageBusContract))]
    public interface IMessageBus
    {
        /// <summary>
        /// Starts the message processing thread. While the thread is running all messages received on the endpoint are processed.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the MessageBus once it finishes processing the current message (and all its possible retries). No new messages will be processed while the MessageBus is stopping.
        /// </summary>
        void Stop();

        /// <summary>
        /// Publishes message <paramref name="message"/> to the configured endpoints, using the configured transaction options.
        /// </summary>
        /// <param name="message">The message to send.</param>
        void Publish(object message);

        /// <summary>
        /// Publishes all the <paramref name="messages"/> to the configured endpoints, using the configured transaction options. All messages are sent in the same transaction.
        /// </summary>
        /// <param name="message">The messages to send.</param>
        void Publish(object[] messages);

        /// <summary>
        /// Sends <paramref name="message"/> directly to the specified endpoint.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="endpointId">The id of the endpoint where the message must be sent to.</param>
        void Send(object message, object endpointId);

        /// <summary>
        /// Raised before a message is processed.
        /// Don't try to stop the message bus from its own thread. You'll cause a deadlock.
        /// </summary>
        event Action<MessageProcessingEventArgs> MessageProcessing;

        /// <summary>
        /// Raised after a message has been processed OK. Exclusive with <seealso cref="MessageProcessingFailed"/>.
        /// Don't try to stop the message bus from its own thread. You'll cause a deadlock.
        /// </summary>
        event Action<MessageProcessedEventArgs> MessageProcessed;

        /// <summary>
        /// Raised after the processing of a message fails. Exclusive with <seealso cref="MessageProcessed"/>.
        /// Don't try to stop the message bus from its own thread. You'll cause a deadlock.
        /// </summary>
        event Action<MessageProcessingFailedEventArgs> MessageProcessingFailed;

        /// <summary>
        /// Raised before sending a message.
        /// Don't try to stop the message bus from its own thread. You'll cause a deadlock.
        /// </summary>
        event Action<MessageSendingEventArgs> MessageSending;

        /// <summary>
        /// Raised after a message has been sent OK. Notice that when sending more than one message in the same transaction,
        /// if the transaction fails the event was already raised but the message sending will be canceled.
        /// Don't try to stop the message bus from its own thread. You'll cause a deadlock.
        /// </summary>
        event Action<MessageSentEventArgs> MessageSent;

        /// <summary>
        /// Raised when the MessageBus is stopped. Useful to check if there was some fatal exception.
        /// Don't try to stop the message bus from its own thread. You'll cause a deadlock.
        /// </summary>
        event Action<MessageBusStoppedEventArgs> Stopped;

    }

    [ContractClassFor(typeof(IMessageBus))]
    public abstract class IMessageBusContract : IMessageBus
    {
       public void Start()
       {
       }

       public void Stop()
       {
       }

       public void Publish(object message)
       {
           Contract.Requires(message != null);
       }

       public void Publish(object[] messages)
       {
           Contract.Requires(messages != null);
           Contract.Requires(messages.Length > 0);
       }

       public void Send(object message, object endpointId)
       {
           Contract.Requires(message != null);
           Contract.Requires(endpointId != null);
       }

       public event Action<MessageProcessingEventArgs> MessageProcessing;

       public event Action<MessageProcessedEventArgs> MessageProcessed;

       public event Action<MessageProcessingFailedEventArgs> MessageProcessingFailed;

       public event Action<MessageSendingEventArgs> MessageSending;

       public event Action<MessageSentEventArgs> MessageSent;

       public event Action<MessageBusStoppedEventArgs> Stopped;
    }

}