﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TEAM.Commons.Messaging
{
   public class MessageHandlerNotFoundException : CriticalMessageBusException
   {

      public MessageHandlerNotFoundException(object offendingMessage, Exception innerException)
         : base("Couldn't get the handler for message of type " + offendingMessage.GetType().FullName, innerException)
      {
         Contract.Requires(offendingMessage != null);
         OffendingMessage = offendingMessage;
      }

      public readonly object OffendingMessage;

   }
}
