﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging
{
    public class DictionaryMessageMap : IMessageMap
    {
        public DictionaryMessageMap()
        {
            Map = new Dictionary<Type, MessageRoutingInfo>();
            EndpointsMap = new Dictionary<object, MessageRoutingInfo>();
        }

        protected readonly IDictionary<Type, MessageRoutingInfo> Map;
        protected readonly IDictionary<object, MessageRoutingInfo> EndpointsMap;

        public MessageRoutingInfo GetRoutingInfoForMessageType(Type messageType)
        {
            try
            {
                var result = Map[messageType];
                return result;
            }
            catch (Exception ex)
            {
               throw new MessageRoutingInfoNotFoundException(messageType, ex);
            }
        }

        public MessageRoutingInfo GetRoutingInfoForEndpoint(object id)
        {
           try
           {
              var result = EndpointsMap[id];
              return result;
           }
           catch (Exception ex)
           {
              throw new EndpointNotFoundByIdException(id, ex);
           }
        }

        public bool ContainsRoutingInfoForMessageType(Type messageType)
        {
            return Map.ContainsKey(messageType);
        }

        public bool ContainsRoutingInfoForEndpoint(object endpointId)
        {
            return EndpointsMap.ContainsKey(endpointId);
        }

        public void AddMessageMap<TMessage>(object endpointId)
        {
            Contract.Requires(endpointId != null);
            Contract.Ensures(ContainsRoutingInfoForMessageType(typeof(TMessage)));

            var endpointRoutingInfo = GetRoutingInfoForEndpoint(endpointId);
            Map.Add(typeof(TMessage), endpointRoutingInfo);
        }

        public void AddEndpoint(object endpointId, MessageRoutingInfo routingInfo)
        {
            Contract.Requires(endpointId != null, "id is null.");
            Contract.Requires(routingInfo != null, "routingInfo is null.");
            Contract.Ensures(ContainsRoutingInfoForEndpoint(endpointId));

            EndpointsMap.Add(endpointId, routingInfo);
        }

        public MessageRoutingInfo this[Type key]
        {
            get
            {
                return Map[key];
            }
            set
            {
                Map[key] = value;
            }
        }

    }
}
