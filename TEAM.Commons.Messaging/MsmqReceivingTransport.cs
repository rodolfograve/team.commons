﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Threading;

namespace TEAM.Commons.Messaging.Msmq
{

    /// <summary>
    /// Transport used to receive messages from a non-transactional MSMQ queue.
    /// </summary>
    public class MsmqReceivingTransport : MsmqTransportBase, IReceivingTransport
    {
       public MsmqReceivingTransport(string inputQueuePath, IMessageSerializer messageSerializer)
          : base(inputQueuePath, messageSerializer)
       {
          Contract.Requires(!String.IsNullOrEmpty(inputQueuePath), "inputQueuePath is null or empty.");
          Contract.Requires(messageSerializer != null, "messageSerializer is null.");
       }

        protected Message CurrentAvailableMessage;
        protected object CurrentAvailableMessageLocker = new object();

        protected virtual TimeSpan GetSafeTimeSpan(TimeSpan timeout)
        {
            return timeout == Timeout.InfiniteTimeSpan ? MessageQueue.InfiniteTimeout : timeout;
        }

        public virtual TransportMessage WaitForMessage(TimeSpan timeout)
        {
            return ReceiveFromQueue(timeout);
        }

        protected virtual Message TryPeek(TimeSpan timeout)
        {
            Message result = null;
            try
            {
                result = Queue.Peek(GetSafeTimeSpan(timeout));
            }
            catch (MessageQueueException ex)
            {
                if (!HandleExpectedMessageQueueException(ex))
                {
                    throw new Exception("Error trying to peek a message from queue '" + Queue.Path + "'", ex);
                }
            }
            return result;
        }

        protected virtual TransportMessage ReceiveFromQueue(TimeSpan timeout)
        {
            TransportMessage result = null;
            Message msmqMessage = null;
            try
            {
                msmqMessage = Queue.Receive(GetSafeTimeSpan(timeout), MessageQueueTransactionType.Automatic);
            }
            catch (MessageQueueException ex)
            {
                if (!HandleExpectedMessageQueueException(ex))
                {
                    throw;
                }
            }

            if (msmqMessage != null)
            {
                result = ExtractTransportMessageFrom(msmqMessage);
            }
            return result;
        }

        public virtual bool BlockUntilMessageIsAvailable(TimeSpan timeout)
        {
            lock (CurrentAvailableMessageLocker)
            {
                CurrentAvailableMessage = TryPeek(timeout);
                return CurrentAvailableMessage != null;
            }
        }

        /// <summary>
        /// Atomic operation for getting the CurrentAvailableMessage.Id
        /// </summary>
        /// <returns></returns>
        protected virtual string GetCurrentAvailableMessageId()
        {
            string result = null;
            lock (CurrentAvailableMessageLocker)
            {
                if (CurrentAvailableMessage != null)
                {
                    result = CurrentAvailableMessage.Id;
                }
            }
            return result;
        }

        [DebuggerNonUserCode]
        public virtual TransportMessage TryReceiveCurrentAvailableMessage()
        {
            TransportMessage result = null;
            Message msmqMessage = null;
            string currentAvailableMessageId = GetCurrentAvailableMessageId();

            if (currentAvailableMessageId != null)
            {
                try
                {
                    msmqMessage = Queue.ReceiveById(currentAvailableMessageId, MessageQueueTransactionType.Automatic);
                }
                catch (InvalidOperationException)
                {
                    // The message was read by another process/thread.
                }
                catch (MessageQueueException ex)
                {
                    if (!HandleExpectedMessageQueueException(ex))
                    {
                        throw;
                    }
                }

                if (msmqMessage != null)
                {
                    result = ExtractTransportMessageFrom(msmqMessage);
                }
            }
            return result;
        }

        protected virtual TransportMessage ExtractTransportMessageFrom(Message msmqMessage)
        {
           Contract.Requires(msmqMessage != null, "msmqMessage is null.");
           Contract.Ensures(Contract.Result<TransportMessage>() != null);
           
           var messageContent = new byte[msmqMessage.BodyStream.Length];
           msmqMessage.BodyStream.Read(messageContent, 0, messageContent.Length);
           var result = MessageSerializer.Deserialize(messageContent);
           result.UniqueId = msmqMessage.Id;
           return result;
        }

        protected override MessageQueue CreateQueue(string queuePath)
        {
            return new MessageQueue(queuePath, QueueAccessMode.Receive);
        }

        protected virtual bool HandleExpectedMessageQueueException(MessageQueueException ex)
        {
           Contract.Requires(ex != null, "ex is null.");
           
           switch (ex.MessageQueueErrorCode)
           {
              case MessageQueueErrorCode.QueueDeleted:
                 {
                    Queue.Close();
                    throw new Exception("The Queue '" + Queue.Path + "' has been deleted or is not accessible.");
                 }
              case MessageQueueErrorCode.IOTimeout:
                 {
                    //timeout passed and no message was received
                    return true;
                 }
              default:
                 {
                    return false;
                 }
           }
        }

    }
}
