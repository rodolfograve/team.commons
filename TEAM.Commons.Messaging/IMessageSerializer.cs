﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TEAM.Commons.Messaging
{
    public interface IMessageSerializer
    {

        byte[] Serialize(TransportMessage message);

        TransportMessage Deserialize(byte[] message);

    }
}
