﻿using System;
using System.Transactions;
using System.Diagnostics.Contracts;
using System.Diagnostics;

namespace TEAM.Commons.Messaging
{
    [SwitchAttribute("TEAM.Commons.Messaging.MessageBus", typeof(SourceSwitch))]
    public class SingleThreadMessageBus : IMessageBus
    {

        public SingleThreadMessageBus(string id,
                          IReceivingTransport receivingTransport,
                          IMessageHandlersContainer messageHandlersContainer,
                          IMessageMap messageMap,
                          TransactionOptions? transactionOptions = null,
                          Func<IErrorHandlingStrategy> errorHandlingStrategyFactory = null,
                          TimeSpan? pollingInterval = null)
        {
            Contract.Requires(!String.IsNullOrEmpty(id), "id is null or empty.");
            Contract.Requires(receivingTransport != null, "receivingTransport is null.");
            Contract.Requires(messageHandlersContainer != null, "messageHandlersContainer is null.");
            Contract.Requires(messageMap != null, "messageMap is null.");

            Contract.Ensures(Id == "MessageBus-" + id);
            Contract.Ensures(ReceivingTransport == receivingTransport);
            Contract.Ensures(MessageHandlersContainer == messageHandlersContainer);
            Contract.Ensures(MessageMap == messageMap);
            Contract.Ensures(TransactionOptions == transactionOptions);
            Contract.Ensures(ErrorHandlingStrategyFactory != null);
            Contract.Ensures(PollingInterval == (pollingInterval ?? TimeSpan.FromMilliseconds(1500)));

            TraceSource = new TraceSource("TEAM.Commons.Messaging.MessageBus", SourceLevels.Information)
            {
                Switch = new SourceSwitch("TEAM.Commons.Messaging.MessageBus", "Verbose")
            };
            Trace.TraceInformation("MessagBus tracing configured. See http://msdn.microsoft.com/en-us/library/system.diagnostics.tracesource.aspx for reference on how to enable it. SourceSwitch='TEAM.Commons.Messaging.MessageBus', TraceSource='TEAM.Commons.Messaging.MessageBus'");
            TraceSource.TraceInformation("'TEAM.Commons.Messaging.MessageBus' tracing has been correctly enabled with level='" + TraceSource.Switch.Level + "'");

            Id = "MessageBus-" + id;
            ReceivingTransport = receivingTransport;
            MessageHandlersContainer = messageHandlersContainer;
            MessageMap = messageMap;
            TransactionOptions = transactionOptions;
            ErrorHandlingStrategyFactory = errorHandlingStrategyFactory ?? (() => new MaxRetryAttemptsErrorHandlingStrategy(5, TimeSpan.FromSeconds(3)));
            PollingInterval = pollingInterval ?? TimeSpan.FromMilliseconds(1500);
        }

        protected readonly TraceSource TraceSource;

        protected readonly string Id;
        protected readonly IReceivingTransport ReceivingTransport;
        protected readonly IMessageHandlersContainer MessageHandlersContainer;
        protected readonly IMessageMap MessageMap;
        protected readonly TransactionOptions? TransactionOptions;
        protected readonly Func<IErrorHandlingStrategy> ErrorHandlingStrategyFactory;
        protected readonly TimeSpan PollingInterval;

        public event Action<MessageProcessingEventArgs> MessageProcessing;
        public event Action<MessageProcessedEventArgs> MessageProcessed;
        public event Action<MessageProcessingFailedEventArgs> MessageProcessingFailed;
        public event Action<MessageSendingEventArgs> MessageSending;
        public event Action<MessageSentEventArgs> MessageSent;

        public event Action<MessageBusStoppedEventArgs> Stopped;
        protected bool StopRequested;

        protected DelegateWorkerThread WorkerThread;

        public virtual void Start()
        {
            var processId = Process.GetCurrentProcess().Id;
            TraceSource.TraceStartWithCallerInfo("Starting the MessageBus from thread with Id='" + Id + "' inside process with Id='" + processId + "'");
            if (WorkerThread == null)
            {
                WorkerThread = new DelegateWorkerThread(Id, ReceiveAndHandleMessage);
                WorkerThread.Stopped += (x, e) => RaiseStopped(e.Error);
                WorkerThread.Start();
            }
            else
            {
                throw new InvalidOperationException("This service has already been started.");
            }
            TraceSource.TraceStopWithCallerInfo();
        }

        public virtual void Publish(object[] messages)
        {
            TraceSource.TraceEventWithCallerInfo(TraceEventType.Start, "messages='" + messages.PrettyFormat() + "'");
            TransactionScope tx = null;
            try
            {
                if (TransactionOptions.HasValue)
                {
                    tx = new TransactionScope(TransactionScopeOption.RequiresNew, TransactionOptions.Value);
                }
                foreach (var message in messages)
                {
                    MessageRoutingInfo routingInfo = null;
                    try
                    {
                        routingInfo = MessageMap.GetRoutingInfoForMessageType(message.GetType());
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error getting the MessageRoutingInfo for message " + message.PrettyFormat(), ex);
                    }
                    SendMessageWithRoutingInfo(message, routingInfo);
                }
                if (tx != null)
                {
                    tx.Complete();
                }
            }
            catch (Exception ex)
            {
                var message = "Error in the transaction for publishing messages " + messages.PrettyFormat();
                TraceSource.TraceCriticalWithCallerInfo(ex, message);
                throw new ApplicationException(message, ex);
            }
            finally
            {
                if (tx != null)
                {
                    tx.Dispose();
                }
                TraceSource.TraceStopWithCallerInfo();
            }
        }

        public virtual void Publish(object message)
        {
            Publish(new object[] { message });
        }

        public virtual void Send(object message, object endpointId)
        {
            TraceSource.TraceStartWithCallerInfo("message='" + message.PrettyFormat() + "', endpointId='" + endpointId + "'");
            TransactionScope tx = null;
            try
            {
                if (TransactionOptions.HasValue)
                {
                    tx = new TransactionScope(TransactionScopeOption.Required, TransactionOptions.Value);
                }
                MessageRoutingInfo routingInfo = null;
                try
                {
                    routingInfo = MessageMap.GetRoutingInfoForEndpoint(endpointId);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error getting the MessageRoutingInfo for message " + message.PrettyFormat(), ex);
                }

                SendMessageWithRoutingInfo(message, routingInfo);

                if (tx != null)
                {
                    tx.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in the transaction for sending messages " + message.PrettyFormat(), ex);
            }
            finally
            {
                if (tx != null)
                {
                    tx.Dispose();
                }
                TraceSource.TraceStopWithCallerInfo();
            }
        }

        protected virtual void SendMessageWithRoutingInfo(object message, MessageRoutingInfo routingInfo)
        {
            Contract.Requires(message != null, "message is null.");
            Contract.Requires(routingInfo != null, "routingInfo is null.");

            try
            {
                RaiseMessageSending(message, routingInfo);
                var transportMessage = routingInfo.SendingTransport.Send(message);
                RaiseMessageSent(message, transportMessage, routingInfo);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error sending message " + message.PrettyFormat(), ex);
            }
        }

        protected virtual void ReceiveAndHandleMessage()
        {
            TraceSource.TraceStartWithCallerInfo();
            TransportMessage transportMessage = null;
            TransactionScope tx = null;
            
            bool messageProcessedOk = false;
            int retryCount = 0;
            TraceSource.TraceStartWithCallerInfo("Creating instance of ErrorHandlingStrategy");
            var errorHandlingStrategy = ErrorHandlingStrategyFactory();
            TraceSource.TraceStopWithCallerInfo("Creating instance of ErrorHandlingStrategy");

            do
            {
                try
                {
                    TraceSource.TraceStartWithCallerInfo("Blocking thread until a message is available in the queue");
                    if (
                            !StopRequested && WorkerThread != null && !WorkerThread.IsStopped &&
                            ReceivingTransport.BlockUntilMessageIsAvailable(PollingInterval) &&
                            !StopRequested && WorkerThread != null && !WorkerThread.IsStopped
                       )
                    {
                        TraceSource.TraceStopWithCallerInfo("Blocking thread until a message is available in the queue. A message is available");

                        if (TransactionOptions.HasValue)
                        { // Create new ambient transaction
                            TraceSource.TraceStartWithCallerInfo("Creating transaction scope");
                            tx = new TransactionScope(TransactionScopeOption.Required, TransactionOptions.Value);
                            TraceSource.TraceStopWithCallerInfo("Creating transaction scope");
                        }
                        TraceSource.TraceStartWithCallerInfo("Reading message from the queue");
                        transportMessage = ReceivingTransport.TryReceiveCurrentAvailableMessage();
                        TraceSource.TraceStopWithCallerInfo("Reading message from the queue");

                        if (transportMessage == null)
                        { // The message was read by another process... ignore this and wait for another message
                            TraceSource.TraceEventWithCallerInfo(TraceEventType.Information, "The message is no longer available in the queue. It's probably been read by another process/thread reading from the same queue");
                            if (tx != null)
                            {
                                TraceSource.TraceStartWithCallerInfo("Completing current transaction");
                                tx.Complete();
                                TraceSource.TraceStopWithCallerInfo("Completing current transaction");
                            }
                        }
                        else
                        { // We got the message
                            TraceSource.TraceStartWithCallerInfo("Processing transport message. retryCount='" + retryCount + "'");
                            if (transportMessage.Messages == null)
                            {
                                var ex = new ApplicationException("There is a message in the queue with a wrong format (not a Transport Message). This might happen if the sender is using a different IMessageSerializer or if some other process is writing messages directly (not using the MsmqMessageTransport)");
                                TraceSource.TraceCriticalWithCallerInfo(ex);
                                throw ex;
                            }
                            var messageProcessingContext = BuildMessageProcessingContext(retryCount);
                            int messageCount = 0;
                            foreach (var message in transportMessage.Messages)
                            {
                                messageCount++;
                                TraceSource.TraceStartWithCallerInfo("Processing message " + messageCount + " of " + transportMessage.Messages.Length);
                                MessageHandlerInfo handler = null;
                                try
                                { // Find the registered handler for this message. A failure at this point is critical because
                                    // it means the MessageBus is not configured properly and we're receiving some messages
                                    // we can't process. Stop inmediatly and cancel the transaction so that the message is not removed
                                    // from the ReceivingTransport and can be processed next time to Start.
                                    TraceSource.TraceStartWithCallerInfo("Getting message handler for message");
                                    handler = MessageHandlersContainer.GetMessageHandlerFor(message, messageProcessingContext);
                                    TraceSource.TraceStopWithCallerInfo("Getting message handler for message");
                                }
                                catch (Exception fatalEx)
                                {
                                    var ex = new MessageBusConfigurationErrorsException("Cannot find a handler for this message. Make sure you have registered a handler for every message this MessageBus can receive. The MessageBus will be stopped now and no further messages will be processed. Offending message is " + message + ". The transport message is " + transportMessage.PrettyFormat(), fatalEx);
                                    TraceSource.TraceCriticalWithCallerInfo(ex);
                                    throw ex;
                                }

                                using (new TransactionScope(TransactionScopeOption.Suppress))
                                { // Raise the MessageProcessing event outside any transaction. We want to keep a record that this message was
                                    // processing even if the actual processing fails.
                                    RaiseMessageProcessing(message, transportMessage, handler);
                                }

                                Exception fatalException = null;
                                try
                                {
                                    TraceSource.TraceStartWithCallerInfo("Invoking handler of message");
                                    handler.HandlerAction.Invoke();
                                    messageProcessedOk = true;
                                    TraceSource.TraceStopWithCallerInfo("Invoked handler of message OK");
                                }
                                catch (Exception ex)
                                { // A failure here IS fatal. Message handlers are NOT supposed to throw exceptions unless there is some fatal error.
                                    TraceSource.TraceErrorWithCallerInfo(ex, "Invocation of the handler caused an exception");
                                    fatalException = ex;
                                }

                                if (fatalException == null) // The message was processed OK
                                { // Raise the MessageProcessed event inside the ambient transaction. If this event fails the transaction should be aborted
                                    // to ensure all subscribers receive all the notifications.
                                    if (!messageProcessingContext.AbortCurrentMessageRequested)
                                    {
                                        RaiseMessageProcessed(message, transportMessage, handler);
                                        if (tx != null)
                                        { // Commit the transaction so that message is removed from the queue.
                                            tx.Complete();
                                        }
                                    }
                                }
                                else
                                {
                                    using (new TransactionScope(TransactionScopeOption.Suppress))
                                    { // Raise the MessageProcessingFailed event outside any transaction. We want to keep a record that the processing of this message failed.
                                        RaiseMessageProcessingFailed(message, transportMessage, handler, fatalException);
                                    }

                                    TraceSource.TraceStartWithCallerInfo("Invoking error handling strategy");
                                    errorHandlingStrategy.ProcessFailure(message, fatalException, messageProcessingContext);
                                    TraceSource.TraceStopWithCallerInfo("Invoked error handling strategy. CanBeRetried='" + errorHandlingStrategy.CanBeRetried + "'");

                                    if (errorHandlingStrategy.CanBeRetried)
                                    {
                                        TraceSource.TraceStartWithCallerInfo("Message can be retried. Increasing retryCount. Current value is '" + retryCount + "'");
                                        retryCount++;
                                        TraceSource.TraceStopWithCallerInfo("retryCount increased to '" + retryCount + "'");
                                    }
                                    else
                                    {
                                        var ex = new ApplicationException("A poison message has caused the message bus to stop. Processing was retried " + retryCount + " times before giving up. Make sure no exceptions are thrown from your message handler unless they are really an exception, in which case you must either fix the application sending the (poison)message so that it doesn't send it any more; or the message handler so that it handles this message properly", fatalException);
                                        TraceSource.TraceCriticalWithCallerInfo(ex);
                                        throw ex;
                                    }
                                }
                                TraceSource.TraceStopWithCallerInfo("Processing message " + messageCount + " of " + transportMessage.Messages.Length);
                            }
                            TraceSource.TraceStopWithCallerInfo("Processing transport message. retryCount='" + retryCount + "'");
                        }
                    }
                    else
                    {
                        TraceSource.TraceStopWithCallerInfo("Blocking thread until a message is available in the queue. No message was available during the polling interval('" + PollingInterval + "')");
                    }
                }
                finally
                {
                    if (tx != null)
                    {
                        try
                        {
                            TraceSource.TraceStartWithCallerInfo("Disposing transaction");
                            tx.Dispose();
                            TraceSource.TraceStopWithCallerInfo("Disposed the transaction");
                        }
                        catch (TransactionAbortedException)
                        { // The transaction was already aborted by some nested transaction scope. Do nothing.
                            // Ideally TransactionScope would have a "IsAborted" property to check before disposing, but it doesn't.
                        }
                        tx = null;
                    }
                    TraceSource.TraceStopWithCallerInfo("Attempt to process message. retryCount is now '" + retryCount + "'");
                }
            } while (
                         !messageProcessedOk &&
                         !StopRequested && WorkerThread != null && !WorkerThread.IsStopped &&
                         transportMessage != null &&
                         errorHandlingStrategy.CanBeRetried
                    );

            TraceSource.TraceStopWithCallerInfo();
        }

        public virtual void Stop()
        {
            StopRequested = true;
            if (WorkerThread != null && !WorkerThread.IsStopped)
            {
                WorkerThread.StopAsync().Wait();
            }
        }

        protected virtual IMessageProcessingContext BuildMessageProcessingContext(int retryCount)
        {
            return new MessageProcessingContext(this, retryCount);
        }

        protected void RaiseMessageSending(object message, MessageRoutingInfo routingInfo)
        {
            if (MessageSending != null)
            {
                MessageSending(new MessageSendingEventArgs(DateTime.UtcNow, message, null, routingInfo));
            }
        }

        protected void RaiseMessageSent(object message, TransportMessage transportMessage, MessageRoutingInfo routingInfo)
        {
            if (MessageSent != null)
            {
                MessageSent(new MessageSentEventArgs(DateTime.UtcNow, message, transportMessage, routingInfo));
            }
        }

        protected void RaiseMessageProcessing(object message, TransportMessage transportMessage, MessageHandlerInfo handler)
        {
            try
            {
                if (MessageProcessing != null)
                {
                    MessageProcessing(new MessageProcessingEventArgs(DateTime.UtcNow, message, transportMessage, handler));
                }
            }
            catch (Exception fatalEx)
            { // A failure here is critical because it means clients subscribers were not notified of a message processing event
                // and this could potentially lead to non-registered actions. So, throw the exception.
                throw new CriticalMessageBusException("Error raising the MessageProcessing event. The MessageBus will be stopped now and no further messages will be processed", fatalEx);
            }
        }

        protected void RaiseMessageProcessed(object message, TransportMessage transportMessage, MessageHandlerInfo handler)
        {
            try
            { // Raise the MessageProcessed event inside the transaction. If this event fails we want to cancel the whole transaction
                // because we'll have a processed message that wasn't properly notified to subscribers.
                if (MessageProcessed != null)
                {
                    MessageProcessed(new MessageProcessedEventArgs(DateTime.UtcNow, message, transportMessage, handler));
                }
            }
            catch (Exception fatalEx)
            { // A failure here is critical because it means clients subscribers were not notified of a message processed event
                // and this could potentially lead to non-registered actions.
                throw new CriticalMessageBusException("Error raising the MessageProcessed event. The MessageBus will be stopped now and no further messages will be processed", fatalEx);
            }
        }

        protected void RaiseMessageProcessingFailed(object message, TransportMessage transportMessage, MessageHandlerInfo handler, Exception nonFatalException)
        {
            TraceSource.TraceStartWithCallerInfo();
            try
            { // Raise the MessageProcessingFailed event inside the transaction. If this event fails we want to cancel the whole transaction
                // because we'll have a processed message that wasn't properly notified to subscribers.
                if (MessageProcessingFailed != null)
                {
                    MessageProcessingFailed(new MessageProcessingFailedEventArgs(DateTime.UtcNow, message, transportMessage, handler, nonFatalException));
                }
            }
            catch (Exception fatalEx)
            { // A failure here is critical because it means clients subscribers were not notified of a message processed event
                // and this could potentially lead to non-registered actions.
                var ex = new CriticalMessageBusException("Error raising the MessageProcessingFailed event. The MessageBus will be stopped now and no further messages will be processed. The offending message is " + message.PrettyFormat() + ". The transport message is " + transportMessage.PrettyFormat(), fatalEx);
                TraceSource.TraceCriticalWithCallerInfo(ex);
                throw ex;
            }
            TraceSource.TraceStopWithCallerInfo();
        }

        protected void RaiseStopped(Exception ex)
        {
            if (Stopped != null)
            {
                Stopped(ex == null ? new MessageBusStoppedEventArgs() : new MessageBusStoppedEventArgs(ex));
            }
        }

    }
}
