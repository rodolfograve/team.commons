﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageProcessingEventArgsBase : MessageEventArgsBase
    {
        public MessageProcessingEventArgsBase(DateTime timestampUtc, object message, TransportMessage transportMessage, MessageHandlerInfo handler)
            : base (timestampUtc, message, transportMessage)
        {
            Handler = handler;
        }

        public readonly MessageHandlerInfo Handler;

    }
}
