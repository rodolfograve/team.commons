﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageProcessingFailedEventArgs : MessageProcessingEventArgsBase
    {

        public MessageProcessingFailedEventArgs(DateTime timestampUtc, object message, TransportMessage transportMessage, MessageHandlerInfo handler, Exception exception)
            : base(timestampUtc, message, transportMessage, handler)
        {
            Exception = exception;
        }

        public readonly Exception Exception;

    }
}
