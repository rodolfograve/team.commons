﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageProcessingContext : IMessageProcessingContext
    {

        public MessageProcessingContext(IMessageBus bus, int retryCount = 0)
        {
            Contract.Requires(bus != null);
            Contract.Ensures(Bus == bus);
            Contract.Ensures(RetryCount == retryCount);

            Bus = bus;
            RetryCount = retryCount;
        }

        protected readonly IMessageBus Bus;
        public bool AbortCurrentMessageRequested { get; protected set; }

        public void Publish(object message)
        {
            Bus.Publish(message);
        }

        public void Publish(object[] messages)
        {
            Bus.Publish(messages);
        }

        public void Send(object message, object endpointId)
        {
           Bus.Send(message, endpointId);
        }

        public void AbortCurrentMessage()
        {
            AbortCurrentMessageRequested = true;
        }

        public int RetryCount { get; private set; }

    }
}
