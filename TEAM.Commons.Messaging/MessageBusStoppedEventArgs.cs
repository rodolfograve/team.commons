﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageBusStoppedEventArgs : EventArgs
    {

        public MessageBusStoppedEventArgs()
        {
        }

        public MessageBusStoppedEventArgs(Exception ex)
        {
            Exception = ex;
        }

        public readonly Exception Exception;

    }
}
