﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging
{
    public static class AutofacContainerBuilderMessagesHandlerContainerExtensions
    {
        
        /// <summary>
        /// Registers the specified type as a message handler for all its implemented IMessageHandler.
        /// </summary>
        /// <typeparam name="TMessageHandler">The type of the message handler to register.</typeparam>
        /// <param name="target">The ContainerBuilder that this method extends.</param>
        public static void RegisterMessageHandlerType<TMessageHandler>(this ContainerBuilder target)
        {
            Contract.Requires(target != null, "target is null.");
            
            target.RegisterType<TMessageHandler>().As(GetImplementedMessageHandlersAndFailIfNone(typeof(TMessageHandler)));
        }

        /// <summary>
        /// Registers the specified type as a message handler for all its implemented IMessageHandler, using the specified factory delegate.
        /// </summary>
        /// <typeparam name="TMessageHandler">The type of the message handler to register.</typeparam>
        /// <param name="target">The ContainerBuilder that this method extends.</param>
        /// <param name="delegate">The factory delegate for this handler.</param>
        public static void RegisterMessageHandler<TMessageHandler>(this ContainerBuilder target, Func<IComponentContext, TMessageHandler> @delegate)
        {
            Contract.Requires(target != null, "target is null.");
            Contract.Requires(@delegate != null, "@delegate is null.");

            target.Register<TMessageHandler>(@delegate).As(GetImplementedMessageHandlersAndFailIfNone(typeof(TMessageHandler)));
        }

        private static Type[] GetImplementedMessageHandlersAndFailIfNone(Type messageHandlerType)
        {
            Contract.Requires(messageHandlerType != null, "messageHandlerType is null.");

            var result = messageHandlerType.GetImplementedMessageHandlers().ToArray();

            if (!result.Any())
            {
                throw new InvalidOperationException("The type '" + messageHandlerType.FullName + "' can't be registered as a MessageHandler because it doesn't implement IMessageHandler<TMessage>.");
            }
            return result;
        }
    }
}
