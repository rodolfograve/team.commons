﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TEAM.Commons.Messaging
{
    [ContractClass(typeof(IMessageMapContract))]
    public interface IMessageMap
    {
        [Pure]
        MessageRoutingInfo GetRoutingInfoForMessageType(Type messageType);
        
        [Pure]
        MessageRoutingInfo GetRoutingInfoForEndpoint(object endpointId);

        [Pure]
        bool ContainsRoutingInfoForMessageType(Type messageType);

        [Pure]
        bool ContainsRoutingInfoForEndpoint(object endpointId);

    }

    [ContractClassFor(typeof(IMessageMap))]
    public abstract class IMessageMapContract : IMessageMap
    {
        
        public MessageRoutingInfo GetRoutingInfoForMessageType(Type messageType)
        {
            Contract.Requires(messageType != null);
            Contract.Requires(ContainsRoutingInfoForMessageType(messageType));
            Contract.Ensures(Contract.Result<MessageRoutingInfo>() != null);
            throw new NotImplementedException();
        }

        public MessageRoutingInfo GetRoutingInfoForEndpoint(object endpointId)
        {
            Contract.Requires(ContainsRoutingInfoForEndpoint(endpointId));
            Contract.Ensures(Contract.Result<MessageRoutingInfo>() != null);
            throw new NotImplementedException();
        }

        public bool ContainsRoutingInfoForMessageType(Type messageType)
        {
            Contract.Requires(messageType != null);
            throw new NotImplementedException();
        }

        public bool ContainsRoutingInfoForEndpoint(object endpointId)
        {
            Contract.Requires(endpointId != null);
            throw new NotImplementedException();
        }
    }

    
}
