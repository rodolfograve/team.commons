﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageHandlerInfo
    {

        public MessageHandlerInfo(Action handlerAction, object handlerInstance)
        {
            HandlerAction = handlerAction;
            HandlerInstance = handlerInstance;
        }

        public readonly Action HandlerAction;

        public readonly object HandlerInstance;

    }
}
