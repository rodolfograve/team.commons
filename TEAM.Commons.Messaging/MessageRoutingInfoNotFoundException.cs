﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Messaging
{
    public class MessageRoutingInfoNotFoundException : ApplicationException
    {

       public MessageRoutingInfoNotFoundException(Type messageType, Exception innerException)
          : base("Couldn't find the MessageRoutingInfo for message of type '" + messageType.FullName + "'. Did you add this type to this DictionaryMessageMap?", innerException)
       {
          Contract.Requires(messageType != null, "messageType is null.");
          Contract.Requires(innerException != null, "innerException is null.");
       }

    }
}
