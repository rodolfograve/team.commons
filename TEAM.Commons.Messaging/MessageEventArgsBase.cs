﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageEventArgsBase : EventArgsBase
    {

        public MessageEventArgsBase(DateTime timestampUtc, object message, TransportMessage transportMessage)
            : base (timestampUtc)
        {
            Message = message;
            TransportMessage = transportMessage;
        }

        public readonly object Message;

        public readonly TransportMessage TransportMessage;

    }
}
