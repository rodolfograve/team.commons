﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace TEAM.Commons.Messaging
{
   public class TransportMessage
   {

      public static TransportMessage Create(object[] messages)
      {
         var result = new TransportMessage()
         {
            Messages = messages,
            SentAt = DateTime.Now,
            OriginServerHostName = Environment.MachineName,
            OriginApplicationId = (Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly()).FullName
         };
         return result;
      }

      public string UniqueId { get; set; }

      public object[] Messages { get; set; }

      public DateTime? SentAt { get; set; }

      public string OriginApplicationId { get; set; }

      public string OriginServerHostName { get; set; }

   }
}
