﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Threading.Tasks;

namespace TEAM.Commons.Messaging
{
    [Obsolete("This class is Work In Progress. Do not use it unless you're testing it!")]
    public class DataflowMessageBus : IMessageBus, IDisposable
    {

        public DataflowMessageBus(string inputQueuePath, IMessageSerializer messageSerializer)
        {
            InputQueue = new MessageQueue(inputQueuePath, false, false, QueueAccessMode.Receive);
            MessageSerializer = messageSerializer;
            Stopwatch = new Stopwatch();
        }

        private readonly MessageQueue InputQueue;
        private readonly IMessageSerializer MessageSerializer;

        private readonly Stopwatch Stopwatch;

        public event Action<MessageProcessingEventArgs> MessageProcessing;
        public event Action<MessageProcessedEventArgs> MessageProcessed;
        public event Action<MessageProcessingFailedEventArgs> MessageProcessingFailed;
        public event Action<MessageSendingEventArgs> MessageSending;
        public event Action<MessageSentEventArgs> MessageSent;
        public event Action<MessageBusStoppedEventArgs> Stopped;

        public TimeSpan RunningTime { get { return Stopwatch.Elapsed; } }
        public int MessagesProcessed { get; private set; }

        public bool StopRequested { get; private set; }

        private Task ProcessingTask;

        public void Start()
        {
            Stopwatch.Start();
            ProcessingTask = Task.Run(async () =>
               {
                   while (!StopRequested)
                   {
                       await ReceiveFromQueueAndHandle();
                   }
                   Stopwatch.Stop();
               });
        }

        private Task ReceiveFromQueueAndHandle()
        {
            return Task.Factory.FromAsync<Message>(InputQueue.BeginReceive(), InputQueue.EndReceive).ContinueWith(t => HandleMessage(t));
        }

        public void Stop()
        {
            StopRequested = true;
        }

        public Task StopAsync()
        {
            StopRequested = true;
            InputQueue.Close(); // Stop the BeginRequest that might be already in process
            return ProcessingTask;
        }

        public void Publish(object message)
        {
            throw new NotImplementedException();
        }

        public void Publish(object[] messages)
        {
            throw new NotImplementedException();
        }

        public void Send(object message, object endpointId)
        {
            throw new NotImplementedException();
        }

        private Task HandleMessage(Task<Message> completedReceiveTask)
        {
            if (completedReceiveTask.IsFaulted)
            {
                if (StopRequested)
                {
                    return Task.Run(() => { }); // Empty task
                }
                else
                {
                    return completedReceiveTask; // Return the same failed task so that we propagate the error
                }
            }
            else
            {
                return Task.Run(() =>
                {
                    MessagesProcessed++;
                    //Console.WriteLine(message.PrettyFormat());
                });
            }
        }

        public void Dispose()
        {
            if (InputQueue != null)
                InputQueue.Dispose();
            if (ProcessingTask != null)
            {
                ProcessingTask.Dispose();
                ProcessingTask = null;
            }
        }

    }
}
