﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageProcessedEventArgs : MessageProcessingEventArgsBase
    {

        public MessageProcessedEventArgs(DateTime timestampUtc, object message, TransportMessage transportMessage, MessageHandlerInfo handler)
            : base (timestampUtc, message, transportMessage, handler)
        {
        }

    }
}
