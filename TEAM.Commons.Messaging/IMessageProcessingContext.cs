﻿using System;

namespace TEAM.Commons.Messaging
{
    public interface IMessageProcessingContext
    {
        void Publish(object message);
        void Publish(object[] messages);
        void Send(object message, object endpointId);
        void AbortCurrentMessage();
        bool AbortCurrentMessageRequested { get; }
        int RetryCount { get; }
    }
}
