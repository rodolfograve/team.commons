﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Messaging
{
    public class MessageSendingEventArgsBase : MessageEventArgsBase
    {
        public MessageSendingEventArgsBase(DateTime timestampUtc, object message, TransportMessage transportMessage, MessageRoutingInfo routingInfo)
            : base (timestampUtc, message, transportMessage)
        {
            RoutingInfo = routingInfo;
        }

        protected readonly MessageRoutingInfo RoutingInfo;
    }
}
