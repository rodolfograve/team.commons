﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace TEAM.Commons.Messaging
{
    public class MessageBusConfigurationErrorsException : CriticalMessageBusException
    {

        public MessageBusConfigurationErrorsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }
}
