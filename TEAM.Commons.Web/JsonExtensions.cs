﻿using System.Web.Script.Serialization;

public static class JsonExtensions
{

    public static string ToJson(this object target)
    {
        var serializer = new JavaScriptSerializer();
        var result = serializer.Serialize(target);
        return result;
    }

}
