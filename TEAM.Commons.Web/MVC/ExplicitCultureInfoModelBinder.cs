﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web.Mvc;

namespace TEAM.Commons.Web.MVC
{
    public class ExplicitCultureInfoModelBinder<T> : CultureInfoModelBinderBase<T>
    {

        public ExplicitCultureInfoModelBinder(CultureInfo cultureInfo)
        {
            CultureInfo = cultureInfo;
        }

        protected readonly CultureInfo CultureInfo;

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return BindModelUsingCultureInfo(controllerContext, bindingContext, CultureInfo);
        }
    }
}
