﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace TEAM.Commons.Web.MVC.SingleActionControllers
{
   public class SingleActionControllerActionInvoker : ControllerActionInvoker
   {

      protected override ActionDescriptor FindAction(ControllerContext controllerContext, ControllerDescriptor controllerDescriptor, string actionName)
      {
         return base.FindAction(controllerContext, controllerDescriptor, "Execute");
      }

   }
}
