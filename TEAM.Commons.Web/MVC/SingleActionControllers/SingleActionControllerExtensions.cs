﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TEAM.Commons.Web.MVC.SingleActionControllers;
using System.Diagnostics.Contracts;

public static class SingleActionControllerExtensions
{

    /// <summary>
    /// Returns an anchor element (a element) that contains the virtual path of the specified Single Action Controller.
    /// </summary>
    /// <param name="target">The HTML helper instance that this method extends.</param>
    /// <param name="linkText">The inner text of the anchor element.</param>
    /// <param name="singleActionController">The type of the Single Action Controller.</param>
    /// <param name="protocol">The protocol for the URL, such as "http" or "https".</param>
    /// <param name="hostName">The host name for the URL.</param>
    /// <param name="fragment">The URL fragment name (the anchor name).</param>
    /// <param name="routeValues">An object that contains the parameters for a route. You can use routeValues
    /// to provide the parameters that are bound to the action method parameters. 
    /// The routeValues parameter is merged with the original route values and overrides 
    /// them.</param>
    /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
    /// <returns>An anchor element (a element).</returns>
    public static MvcHtmlString ActionLink(this HtmlHelper target, string linkText, Type singleActionController, string protocol = null, string hostName = null, string fragment = null, object routeValues = null, object htmlAttributes = null)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(linkText), "linkText is null or empty.");
        Contract.Requires(singleActionController != null, "singleActionController is null.");
        
        return target.ActionLink(linkText, SingleActionControllerFactory.GetAction(singleActionController), SingleActionControllerFactory.GetController(singleActionController), protocol, hostName, fragment, routeValues, htmlAttributes);
    }

    /// <summary>
    /// Returns an anchor element (a element) that contains the virtual path of the specified Single Action Controller.
    /// </summary>
    /// <param name="target">The HTML helper instance that this method extends.</param>
    /// <typeparam name="TSingleActionController">The type of the Single Action Controller.</param>
    /// <param name="linkText">The inner text of the anchor element.</param>
    /// <param name="protocol">The protocol for the URL, such as "http" or "https".</param>
    /// <param name="hostName">The host name for the URL.</param>
    /// <param name="fragment">The URL fragment name (the anchor name).</param>
    /// <param name="routeValues">An object that contains the parameters for a route. You can use routeValues
    /// to provide the parameters that are bound to the action method parameters. 
    /// The routeValues parameter is merged with the original route values and overrides 
    /// them.</param>
    /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element.</param>
    /// <returns>An anchor element (a element).</returns>
    public static MvcHtmlString ActionLink<TSingleActionController>(this HtmlHelper target, string linkText, string protocol = null, string hostName = null, string fragment = null, object routeValues = null, object htmlAttributes = null)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(linkText), "linkText is null or empty.");
        
        return target.ActionLink(linkText, typeof(TSingleActionController), protocol, hostName, fragment, routeValues, htmlAttributes);
    }

    /// <summary>
    /// Invokes the specified child Single Action Controller using the specified parameters and renders the result in line in the parent view.
    /// </summary>
    /// <param name="target">The HTML helper instance that this method extends.</param>
    /// <param name="singleActionController">The type of the Single Action Controller.</param>
    /// <param name="routeValues">An object that contains the parameters for a route. You can use routeValues
    /// to provide the parameters that are bound to the action method parameters. 
    /// The routeValues parameter is merged with the original route values and overrides 
    /// them.</param>
    public static void RenderAction(this HtmlHelper target, Type singleActionController, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(singleActionController != null, "singleActionController is null.");
        
        target.RenderAction(SingleActionControllerFactory.GetAction(singleActionController), SingleActionControllerFactory.GetController(singleActionController), routeValues);
    }

    /// <summary>
    /// Invokes the specified child Single Action Controller using the specified parameters and renders the result in line in the parent view.
    /// </summary>
    /// <param name="target">The HTML helper instance that this method extends.</param>
    /// <typeparam name="TSingleActionController">The type of the Single Action Controller.</typeparam>
    /// <param name="routeValues">An object that contains the parameters for a route. You can use routeValues
    /// to provide the parameters that are bound to the action method parameters. 
    /// The routeValues parameter is merged with the original route values and overrides 
    /// them.</param>
    public static void RenderAction<TSingleActionController>(this HtmlHelper target, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        
        target.RenderAction(typeof(TSingleActionController), routeValues);
    }

    /// <summary>
    ///  Invokes the specified Single Action Controller using the specified parameters and returns the result as an HTML string.
    /// </summary>
    /// <param name="target">The HTML helper instance that this method extends.</param>
    /// <param name="singleActionController">The type of the Single Action Controller to invoke.</param>
    /// <param name="routeValues">An object that contains the parameters for a route. You can use routeValues
    /// to provide the parameters that are bound to the action method parameters. 
    /// The routeValues parameter is merged with the original route values and overrides 
    /// them.</param>
    /// <returns>The Single Action Controller result as an HTML string.</returns>
    public static MvcHtmlString Action(this HtmlHelper target, Type singleActionController, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(singleActionController != null, "singleActionController is null.");
        
        return target.Action(SingleActionControllerFactory.GetAction(singleActionController), SingleActionControllerFactory.GetController(singleActionController), routeValues);
    }

    /// <summary>
    ///  Invokes the specified Single Action Controller using the specified parameters and returns the result as an HTML string.
    /// </summary>
    /// <param name="target">The HTML helper instance that this method extends.</param>
    /// <typeparam name="TSingleActionController">The type of the Single Action Controller to invoke.</typeparam>
    /// <param name="routeValues">An object that contains the parameters for a route. You can use routeValues
    /// to provide the parameters that are bound to the action method parameters. 
    /// The routeValues parameter is merged with the original route values and overrides 
    /// them.</param>
    /// <returns>The Single Action Controller result as an HTML string.</returns>
    public static MvcHtmlString Action<TSingleActionController>(this HtmlHelper target, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        
        return target.Action(typeof(TSingleActionController), routeValues);
    }

    /// <summary>
    /// Generates a fully qualified URL to a Single Action Controller by using the specified Single Action Controller and route values.
    /// </summary>
    /// <param name="target">The URL helper instance that this method extends.</param>
    /// <param name="singleActionController">The type of the Single Action Controller.</param>
    /// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved
    /// through reflection by examining the properties of the object. The object
    /// is typically created by using object initializer syntax.</param>
    /// <returns>The fully qualified URL to an action method.</returns>
    public static string Action(this UrlHelper target, Type singleActionController, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(singleActionController != null, "singleActionController is null.");
        
        return target.Action(SingleActionControllerFactory.GetAction(singleActionController), SingleActionControllerFactory.GetController(singleActionController), routeValues);
    }

    /// <summary>
    /// Generates a fully qualified URL to a Single Action Controller by using the specified Single Action Controller and route values.
    /// </summary>
    /// <param name="target">The URL helper instance that this method extends.</param>
    /// <typeparam name="TSingleActionController">The type of the Single Action Controller.</typeparam>
    /// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved
    /// through reflection by examining the properties of the object. The object
    /// is typically created by using object initializer syntax.</param>
    /// <returns>The fully qualified URL to an action method.</returns>
    public static string Action<TSingleActionController>(this UrlHelper target, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        
        return target.Action(typeof(TSingleActionController), routeValues);
    }

    /// <summary>
    /// Redirects to the specified action using the Single Action Controller type.
    /// </summary>
    /// <param name="target">The Controller that this method extends</param>
    /// <param name="singleActionController">The type of the Single Action Controller.</param>
    /// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved
    /// through reflection by examining the properties of the object. The object
    /// is typically created by using object initializer syntax.</param>
    /// <returns>The redirect result object</returns>
    public static RedirectToRouteResult RedirectToAction(this Controller target, Type singleActionController, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(singleActionController != null, "singleActionController is null.");
        
        var routeValuesDictionary = new RouteValueDictionary(routeValues);
        routeValuesDictionary.Add("action", SingleActionControllerFactory.GetAction(singleActionController));
        routeValuesDictionary.Add("controller", SingleActionControllerFactory.GetController(singleActionController));
        return new RedirectToRouteResult(routeValuesDictionary);
    }

    /// <summary>
    /// Redirects to the specified action using the Single Action Controller type.
    /// </summary>
    /// <param name="target">The Controller that this method extends</param>
    /// <typeparam name="TSingleActionController">The type of the Single Action Controller.</typeparam>
    /// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved
    /// through reflection by examining the properties of the object. The object
    /// is typically created by using object initializer syntax.</param>
    /// <returns>The redirect result object</returns>
    public static RedirectToRouteResult RedirectToAction<TSingleActionController>(this Controller target, object routeValues = null)
    {
        Contract.Requires(target != null, "target is null.");
        
        return target.RedirectToAction(typeof(TSingleActionController), routeValues);
    }
}
