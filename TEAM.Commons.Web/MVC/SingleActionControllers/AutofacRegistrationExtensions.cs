﻿using System;
using System.Linq;
using System.Web.Mvc;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Features.Scanning;
using Autofac;
using System.Reflection;
using TEAM.Commons.Web.MVC.SingleActionControllers;
using System.Diagnostics.Contracts;

public static class AutofacRegistrationExtensions
{

     private static readonly Type ControllerType = typeof(Controller);
     private static readonly Type ActionResultType = typeof(ActionResult);

     private static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> RegisterSingleActionControllersInAssemblies(this ContainerBuilder builder, params Assembly[] controllerAssemblies)
     {
         return builder.RegisterAssemblyTypes(controllerAssemblies).Where(delegate(Type t)
         {
            var result = ControllerType.IsAssignableFrom(t);
            if (result)
            {
               var executeMethods = t.GetMethods().Where(x => x.Name == "Execute" && x.ReturnType != null && ActionResultType.IsAssignableFrom(x.ReturnType));
               result = executeMethods.Count() == 1;
            }
            return result;
         }).WithProperty(new NamedPropertyParameter("ActionInvoker", new SingleActionControllerActionInvoker()));
     }

     /// <summary>
     /// Registers all classes in the specified assembly that inherit from System.Web.Mvc.Controller
     /// and have a public 'Execute' method returning ActionResult, associating them to the SingleActionControllerActionInvoker.
     /// </summary>
     /// <param name="builder">The ContainerBuilder that this method extends.</param>
     /// <param name="singleActionControllersAssembly">The assembly that contains all the Single Action Controllers.</param>
     /// <param name="singleActionControllersBaseNamespace">The base namespace for all the controllers.</param>
     public static void RegisterTypesForSingleActionControllers(this ContainerBuilder builder, Assembly singleActionControllersAssembly, string singleActionControllersBaseNamespace)
     {
         Contract.Requires(builder != null, "builder is null.");
         Contract.Requires(singleActionControllersAssembly != null, "singleActionControllersAssembly is null.");
         Contract.Requires(!String.IsNullOrEmpty(singleActionControllersBaseNamespace), "singleActionControllersBaseNamespace is null or empty.");
         
         builder.RegisterSingleActionControllersInAssemblies(singleActionControllersAssembly);
         builder.Register(x => new SingleActionControllerFactory(singleActionControllersAssembly.FullName, singleActionControllersBaseNamespace)).As<IControllerFactory>();
     }

}
