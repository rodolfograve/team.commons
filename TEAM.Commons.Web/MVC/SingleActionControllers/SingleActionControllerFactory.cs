﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Web.MVC.SingleActionControllers
{
    public class SingleActionControllerFactory : DefaultControllerFactory
    {

        public SingleActionControllerFactory(string singleActionControllersAssembly, string singleActionControllersBaseNamespace)
        {
            SingleActionControllersAssembly = singleActionControllersAssembly;
            SingleActionControllersBaseNamespace = singleActionControllersBaseNamespace;
        }

        protected readonly string SingleActionControllersAssembly;
        protected readonly string SingleActionControllersBaseNamespace;

        protected override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
           var controllerTypeName = GetSingleActionControllerTypeNameFrom(controllerName, (string)requestContext.RouteData.Values["action"]);
           var result = Type.GetType(controllerTypeName, false, true);
           return result;
        }

        public virtual string GetSingleActionControllerTypeNameFrom(string controllerName, string actionName)
        {
           Contract.Requires(!String.IsNullOrEmpty(controllerName), "controllerName is null or empty.");
           Contract.Requires(!String.IsNullOrEmpty(actionName), "actionName is null or empty.");
           
           try
           {
              var result = string.Format("{0}.{1}.{2}, {3}", SingleActionControllersBaseNamespace, controllerName, actionName, SingleActionControllersAssembly);
              return result;
           }
           catch (Exception ex)
           {
              throw new ApplicationException("Cannot extract controller parts from controllerName='" + controllerName + "'. Have you mapped your route using the 'MapSingleActionControllersRoute' extension method?", ex);
           }
        }

        public static string GetController(Type singleActionController)
        {
           Contract.Requires(singleActionController != null, "singleActionController is null.");
           
           var result = singleActionController.Namespace.Split('.').Last();
           return result;
        }

        public static string GetAction(Type singleActionController)
        {
           Contract.Requires(singleActionController != null, "singleActionController is null.");
           
           var result = singleActionController.Name;
           return result;
        }

    }
}
