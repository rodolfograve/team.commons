﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Globalization;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Web.MVC
{
    public abstract class CultureInfoModelBinderBase<T> : IModelBinder
    {

        public abstract object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext);

        protected object BindModelUsingCultureInfo(ControllerContext controllerContext, ModelBindingContext bindingContext, CultureInfo cultureInfo)
        {
           Contract.Requires(controllerContext != null, "controllerContext is null.");
           Contract.Requires(bindingContext != null, "bindingContext is null.");
           Contract.Requires(cultureInfo != null, "cultureInfo is null.");
           
           var valueResult =
               bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

           var modelState = new ModelState { Value = valueResult };
           object actualValue = null;

           try
           {
              actualValue = (T)ConvertExtensions.ChangeType<T>(
                  string.IsNullOrWhiteSpace(valueResult.AttemptedValue) ? null : valueResult.AttemptedValue,
                  cultureInfo
              );
           }
           catch (Exception e)
           {
              modelState.Errors.Add(e);
           }

           bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
           return actualValue;
        }
    }
}
