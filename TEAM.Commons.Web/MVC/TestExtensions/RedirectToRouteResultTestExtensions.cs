﻿using System;
using System.Web.Mvc;
using FluentAssertions;
using System.Web.Routing;
using TEAM.Commons.Web.MVC.SingleActionControllers;
using System.Diagnostics.Contracts;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class RedirectToRouteResultTestExtensions
{

    public static RedirectToRouteResult IsPermanent(this RedirectToRouteResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        target.Permanent.Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectToRouteResult IsNotPermanent(this RedirectToRouteResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        target.Permanent.Should().BeFalse(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectToRouteResult WithRouteName(this RedirectToRouteResult target, string expectedRouteName, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        target.RouteName.Should().Be(expectedRouteName, reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectToRouteResult WithRouteValues(this RedirectToRouteResult target, RouteValueDictionary expectedRouteValues, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(expectedRouteValues != null);
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        object value;
        foreach (var expectedRouteValue in expectedRouteValues)
        {
            target.RouteValues.TryGetValue(expectedRouteValue.Key, out value).Should().BeTrue(reason, reasonArgs);
            value.Should().Be(expectedRouteValue.Value, reason.SanitizeForAssertionUsage(), reasonArgs);
        }
        return target;
    }

    public static RedirectToRouteResult WithRouteValues(this RedirectToRouteResult target, object expectedRouteValues, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        return target.WithRouteValues(new RouteValueDictionary(expectedRouteValues), reason.SanitizeForAssertionUsage(), reasonArgs);
    }

    public static RedirectToRouteResult Contains<TValue>(this RedirectToRouteResult target, string expectedKey, TValue expectedValue, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        object value;
        target.RouteValues.TryGetValue(expectedKey, out value).Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
        if (value != null)
        {
           value.Should().BeOfType<TValue>(reason.SanitizeForAssertionUsage(), reasonArgs);
        }
        expectedValue.Should().Be(value, reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectToRouteResult Contains<TValue>(this RedirectToRouteResult target, string expectedKey, Func<TValue, bool> predicate, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        object value;
        target.RouteValues.TryGetValue(expectedKey, out value).Should().BeTrue(reason, reasonArgs);
        if (value != null)
        {
           value.Should().BeOfType<TValue>(reason.SanitizeForAssertionUsage(), reasonArgs);
        }
        value.Should().Match(x => predicate((TValue)x), reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectToRouteResult ContainsNull(this RedirectToRouteResult target, string key, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        object value;
        target.RouteValues.TryGetValue(key, out value).Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
        value.Should().BeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectToRouteResult WithAction(this RedirectToRouteResult target, string actionName, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        return target.Contains<string>("action", actionName, reason.SanitizeForAssertionUsage(), reasonArgs);
    }

    public static RedirectToRouteResult WithController(this RedirectToRouteResult target, string controllerName, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        return target.Contains<string>("controller", controllerName, reason.SanitizeForAssertionUsage(), reasonArgs);
    }

    public static RedirectToRouteResult WithSingleActionController(this RedirectToRouteResult target, Type singleActionController, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(singleActionController != null);
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        var controller = SingleActionControllerFactory.GetController(singleActionController);
        var action = SingleActionControllerFactory.GetAction(singleActionController);
        return target.WithController(controller, reason.SanitizeForAssertionUsage(), reasonArgs).WithAction(action, reason, reasonArgs);
    }

    public static RedirectToRouteResult WithSingleActionController<TSingleActionController>(this RedirectToRouteResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);

        return target.WithSingleActionController(typeof(TSingleActionController), reason.SanitizeForAssertionUsage(), reasonArgs);
    }

}
