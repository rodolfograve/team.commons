﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TEAM.Commons.Web.MVC.TestExtensions
{
   public static class SanitizeCurlyBracketsForAssertionUsageExtensions
   {

      public static string SanitizeForAssertionUsage(this string reason)
      {
         return reason == null ? reason : reason.Replace("{", "{{").Replace("}", "}}");
      }

   }
}
