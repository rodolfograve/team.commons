﻿using System.Linq;
using System.Diagnostics.Contracts;
using System.Web.Mvc;
using FluentAssertions;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class ActionResultTestExtensions
{

    public static ViewResult AssertIsView(this ActionResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        
        return target.AssertIs<ViewResult>(reason, reasonArgs);
    }

    public static JsonResult AssertIsJson(this ActionResult target, string reason = null, params object[] reasonArgs)
    {
       Contract.Requires(target != null, "target is null.");
       Contract.Ensures(Contract.Result<JsonResult>() == target);

       return target.AssertIs<JsonResult>(reason, reasonArgs);
    }

    public static RedirectResult AssertIsRedirect(this ActionResult target, string reason = null, params object[] reasonArgs)
    {
       Contract.Requires(target != null, "target is null.");
       Contract.Ensures(Contract.Result<RedirectResult>() == target);

       return target.AssertIs<RedirectResult>(reason, reasonArgs);
    }

    public static RedirectToRouteResult AssertIsRedirectToRoute(this ActionResult target, string reason = null, params object[] reasonArgs)
    {
       Contract.Requires(target != null, "target is null.");
       Contract.Ensures(Contract.Result<RedirectToRouteResult>() == target);
       
       return target.AssertIs<RedirectToRouteResult>(reason, reasonArgs);
    }

    public static HttpStatusCodeResult AssertIsHttpStatusCode(this ActionResult target, string reason = null, params object[] reasonArgs)
    {
       Contract.Requires(target != null, "target is null.");
       Contract.Ensures(Contract.Result<HttpStatusCodeResult>() == target);
       
       return target.AssertIs<HttpStatusCodeResult>(reason, reasonArgs);
    }

    public static TResult AssertIs<TResult>(this ActionResult target, string reason = null, params object[] reasonArgs) where TResult : ActionResult
    {
        Contract.Ensures(Contract.Result<TResult>() == target);

        target.Should().NotBeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        target.Should().BeOfType<TResult>(reason.SanitizeForAssertionUsage(), reasonArgs);
        return (TResult)target;
    }

}
