﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class ControllerTestExtensions
{
    /// <summary>
    /// Exercises a controller action validating the provided model first, making sure the ModelState contains all the expected errors. The model is provided as an already-built instance.
    /// </summary>
    /// <typeparam name="TModel">The type of the model. No need to specify it if the compiler can infer it.</typeparam>
    /// <param name="target">The controller that this method extends.</param>
    /// <param name="action">The method of the controller to invoke.</param>
    /// <param name="model">The model to pass as an argument to the method once the validation is performed.</param>
    /// <returns>The result of executing the action method.</returns>
    public static ActionResult ExerciseWithBuiltModel<TModel>(this Controller target, Func<TModel, ActionResult> action, TModel model)
    {
        var validationContext = new ValidationContext(model, null, null);
        var validationResults = new List<ValidationResult>();
        Validator.TryValidateObject(model, validationContext, validationResults);

        foreach (var validationResult in validationResults)
        {
            foreach (var memberName in validationResult.MemberNames)
            {
                target.ModelState.AddModelError(memberName, validationResult.ErrorMessage);
            }
        }

        var result = action(model);
        return result;
    }

    /// <summary>
    /// Exercises a controller action validating the model first, making sure the ModelState contains all the expected errors. The model is provided as a NameValueCollection
    /// </summary>
    /// <typeparam name="TModel">The type of the model. No need to specify it if the compiler can infer it.</typeparam>
    /// <param name="target">The controller that this method extends.</param>
    /// <param name="action">The method of the controller to invoke.</param>
    /// <param name="values">The key/value pairs that will be passed to the ModelBinder in order to build the TModel instance that will be passed into the action method.</param>
    /// <returns>The result of executing the action method.</returns>
    public static ActionResult ExerciseWithRawValues<TModel>(this Controller target, Func<TModel, ActionResult> action, NameValueCollection values)
    {
        return ExerciseWithRawValues<TModel>(target, action, values, null);
    }

    public static ActionResult ExerciseWithRawValues<TModel>(this Controller target, Func<TModel, ActionResult> action, NameValueCollection values, NameValueCollection files)
    {
        var filesCollection = new List<FakeHttpPostedFileBase>();
        if (files != null)
        {
            foreach (var fileKey in files.AllKeys)
            {
                filesCollection.Add(new FakeHttpPostedFileBase(fileKey, files[fileKey]));
            }
        }
        var httpContext = new FakeHttpContext(new FakeHttpRequest(new FakeHttpFileCollectionBase(filesCollection)));
        target.ControllerContext = new ControllerContext()
        {
            Controller = target,
            HttpContext = httpContext,
            RequestContext = null,
            RouteData = null
        };
        IModelBinder binder = ModelBinders.Binders.GetBinder(typeof(TModel));
        ModelBindingContext bindingContext = new ModelBindingContext()
        {
            FallbackToEmptyPrefix = true,
            ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, typeof(TModel)),
            ModelName = null,
            ModelState = target.ModelState,
            PropertyFilter = (name => { return true; }),
            ValueProvider = new NameValueCollectionValueProvider(values, null)
        };
        var model = (TModel)binder.BindModel(target.ControllerContext, bindingContext);
        var result = action(model);
        return result;
    }

}
