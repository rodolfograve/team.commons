﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using FluentAssertions;
using System.Diagnostics.Contracts;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class HttpStatusCodeResultTestExtensions
{

   public static HttpStatusCodeResult WithCode(this HttpStatusCodeResult target, HttpStatusCode expectedStatusCode, string reason = null, params string[] reasonArgs)
   {
      Contract.Requires(target != null, "target is null.");
      Contract.Ensures(Contract.Result<HttpStatusCodeResult>() == target);

      target.StatusCode.Should().Be((int)expectedStatusCode, reason.SanitizeForAssertionUsage(), reasonArgs);
      return target;
   }

   public static HttpStatusCodeResult WithStatusDescription(this HttpStatusCodeResult target, string expectedStatusDescription, string reason = null, params string[] reasonArgs)
   {
      Contract.Requires(target != null, "target is null.");
      Contract.Ensures(Contract.Result<HttpStatusCodeResult>() == target);

      target.StatusDescription.Should().Be(expectedStatusDescription, reason.SanitizeForAssertionUsage(), reasonArgs);
      return target;
   }

}
