﻿using System.Web.Mvc;
using FluentAssertions;
using System.Diagnostics.Contracts;
using System;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class JsonResultTestExtensions
{

    public static JsonResult WithAllowGet(this JsonResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<JsonResult>() == target);

        target.JsonRequestBehavior.Should().Be(JsonRequestBehavior.AllowGet, reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static JsonResult WithDenyGet(this JsonResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<JsonResult>() == target);

        target.JsonRequestBehavior.Should().Be(JsonRequestBehavior.DenyGet, reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static JsonResult WithNullData(this JsonResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<JsonResult>() == target);

        target.Data.Should().BeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static JsonResult WithData<TData>(this JsonResult target, Action<TData> constraints = null, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<JsonResult>() == target);

        target.Data.Should().NotBeNull().And.BeOfType<TData>(reason.SanitizeForAssertionUsage(), reasonArgs);

        if (constraints != null)
        {
            constraints((TData)target.Data);
        }
        return target;
    }
}
