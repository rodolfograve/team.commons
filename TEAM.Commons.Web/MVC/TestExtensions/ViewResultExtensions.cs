﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Web.Mvc;
using FluentAssertions;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class ViewResultExtensions
{

    public static ViewResult WithName(this ViewResult target, string expectedName, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        target.ViewName.Should().BeEquivalentTo(expectedName, reason.SanitizeForAssertionUsage(), reasonArgs);

        return target;
    }

    public static ViewResult WithMasterName(this ViewResult target, string expectedMasterName, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        target.MasterName.Should().BeEquivalentTo(expectedMasterName, reason.SanitizeForAssertionUsage(), reasonArgs);

        return target;
    }

    public static ViewResult WithNullModel(this ViewResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        target.Model.Should().BeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static ViewResult WithModel<TModel>(this ViewResult target, Action<TModel> constraints = null, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        target.Model.Should().NotBeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        target.Model.Should().BeOfType<TModel>(reason.SanitizeForAssertionUsage(), reasonArgs);

        if (constraints != null)
        {
            constraints((TModel)target.Model);
        }
        return target;
    }

    public static ViewResult WithViewData(this ViewResult target, Action<ViewDataDictionary> constraints, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        target.ViewData.Should().NotBeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        if (constraints != null)
        {
            constraints(target.ViewData);
        }
        return target;
    }

    public static ViewResult WithModelState(this ViewResult target, Action<ModelStateDictionary> constraints, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        target.ViewData.Should().NotBeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        target.ViewData.ModelState.Should().NotBeNull(reason.SanitizeForAssertionUsage(), reasonArgs);
        if (constraints != null)
        {
            constraints(target.ViewData.ModelState);
        }
        return target;
    }

    public static ViewResult WithModelStateErrorMessages(this ViewResult target, IDictionary<string, string> expectedValues, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        return target.WithModelState(x =>
        {
            foreach (var expectedValue in expectedValues)
            {
                ModelState result;
                x.TryGetValue(expectedValue.Key, out result).Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
                result.Errors.Should().HaveCount(1, reason.SanitizeForAssertionUsage(), reasonArgs);
                result.Errors[0].ErrorMessage.Should().BeEquivalentTo(expectedValue.Value);
            }
        }, reason, reasonArgs);
    }

    public static ViewResult WithModelStateErrors(this ViewResult target, IDictionary<string, Action<ModelState>> constraints, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<ViewResult>() == target);

        return target.WithModelState(x =>
        {
            foreach (var expectedValue in constraints)
            {
                ModelState result;
                x.TryGetValue(expectedValue.Key, out result).Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
                result.Errors.Should().HaveCount(1, reason.SanitizeForAssertionUsage(), reasonArgs);
                expectedValue.Value(result);
            }
        }, reason, reasonArgs);
    }

}
