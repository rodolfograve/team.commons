﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TEAM.Commons.Web.MVC.TestExtensions
{
    public class FakeHttpPostedFileBase : HttpPostedFileBase
    {

        public FakeHttpPostedFileBase(string fileName, string content)
            : base ()
        {
            FileNameInternal = fileName;
            ContentInternal = content;
        }

        protected readonly string FileNameInternal;
        protected readonly string ContentInternal;

        public override string FileName
        {
            get
            {
                return FileNameInternal;
            }
        }

        public override int ContentLength
        {
            get
            {
                return ContentInternal.Length;
            }
        }


    }
}
