﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TEAM.Commons.Web.MVC.TestExtensions
{
    public class FakeHttpRequest : HttpRequestBase
    {

        public FakeHttpRequest(HttpFileCollectionBase fileCollection)
        {
            FileCollection = fileCollection;
        }

        protected readonly HttpFileCollectionBase FileCollection;

        public override HttpFileCollectionBase Files
        {
            get
            {
                return FileCollection;
            }
        }

    }
}
