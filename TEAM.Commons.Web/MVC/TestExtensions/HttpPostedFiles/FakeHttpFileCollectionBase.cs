﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TEAM.Commons.Web.MVC.TestExtensions
{
    public class FakeHttpFileCollectionBase : HttpFileCollectionBase
    {

        public FakeHttpFileCollectionBase(IEnumerable<HttpPostedFileBase> files)
        {
            Files = files;
        }

        protected readonly IEnumerable<HttpPostedFileBase> Files;

        public override HttpPostedFileBase this[string name]
        {
            get
            {
                return Files.SingleOrDefault(x => x.FileName == name);
            }
        }

    }
}
