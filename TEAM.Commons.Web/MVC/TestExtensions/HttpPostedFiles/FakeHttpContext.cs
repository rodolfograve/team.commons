﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TEAM.Commons.Web.MVC.TestExtensions
{
    public class FakeHttpContext : HttpContextBase
    {

        public FakeHttpContext(HttpRequestBase request)
        {
            RequestInternal = request;
        }

        protected readonly HttpRequestBase RequestInternal;

        public override HttpRequestBase Request
        {
            get
            {
                return RequestInternal;
            }
        }

    }
}
