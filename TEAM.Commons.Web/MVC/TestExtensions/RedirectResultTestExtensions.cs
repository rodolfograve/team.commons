﻿using System;
using System.Diagnostics.Contracts;
using System.Web.Mvc;
using FluentAssertions;
using TEAM.Commons.Web.MVC.TestExtensions;

public static class RedirectResultTestExtensions
{

    public static RedirectResult IsPermanent(this RedirectResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectResult>() == target);

        target.Permanent.Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectResult IsNotPermanent(this RedirectResult target, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectResult>() == target);

        target.Permanent.Should().BeFalse(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectResult WithUrl(this RedirectResult target, string expectedUrl, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<RedirectResult>() == target);

        target.Url.Should().Be(expectedUrl, reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectResult WithUrlMatching(this RedirectResult target, Func<string, bool> predicate, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(predicate != null, "predicate is null.");
        Contract.Ensures(Contract.Result<RedirectResult>() == target);

        predicate(target.Url).Should().BeTrue(reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

    public static RedirectResult WithUrlMatching(this RedirectResult target, string wildcardPattern, string reason = null, params object[] reasonArgs)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(wildcardPattern), "wildcardPattern is null or empty.");
        Contract.Ensures(Contract.Result<RedirectResult>() == target);

        target.Url.Should().Match(wildcardPattern, reason.SanitizeForAssertionUsage(), reasonArgs);
        return target;
    }

}
