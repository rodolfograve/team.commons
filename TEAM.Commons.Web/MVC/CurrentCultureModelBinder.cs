﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Globalization;

namespace TEAM.Commons.Web.MVC
{
    public class CurrentCultureModelBinder<T> : CultureInfoModelBinderBase<T>
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return BindModelUsingCultureInfo(controllerContext, bindingContext, CultureInfo.CurrentCulture);
        }
    }
}
