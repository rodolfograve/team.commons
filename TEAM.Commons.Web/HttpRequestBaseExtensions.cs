﻿using System.Web;
using System.Diagnostics.Contracts;

public static class HttpRequestBaseExtensions
{
    /// <summary>
    /// Calculates the absolute URL to the root of the application based on the existing request. Notice that the server name will be extracted
    /// from the request so if it was made to 'localhost' this method would return "localhost/...."
    /// </summary>
    /// <param name="target">The HttpRequestBase instance that this method extends.</param>
    /// <returns>The URL to the root of this application, based on the current request.</returns>
   public static string GetApplicationRootUrl(this HttpRequestBase target)
   {
      Contract.Requires(target != null, "target is null.");
      Contract.Requires(target.Url.AbsoluteUri.Length - target.Url.Query.Length - target.Url.AbsolutePath.Length >= 0);
      
      var absoluteUri = target.Url.AbsoluteUri;
      var absolutePath = target.Url.AbsolutePath;
      var query = target.Url.Query;
      var url = absoluteUri.Substring(0, absoluteUri.Length - query.Length - absolutePath.Length) + target.ApplicationPath;
      if (!url.EndsWith("/"))
         url += "/";

      return url;
   }

}
