﻿using System.Reflection;

[assembly: AssemblyTitle("TEAM.Commons.Web")]
[assembly: AssemblyDescription("TEAM's extensions to MVC and WebForms, including Single Action Controller.")]
