﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.IntegrationTests.Messaging
{
    public class TestMessage
    {

        public Guid GuidProp { get; set; }

        public int PropInt { get; set; }

        public int? PropNullableInt { get; set; }

        public string PropString { get; set; }

        public DateTime PropDateTime { get; set; }

        public DateTime? PropNullableDateTime { get; set; }

    }
}
