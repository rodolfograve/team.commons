﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using FluentAssertions;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.IntegrationTests.Messaging
{
    public static class TestHelper
    {
        public static void EnsureQueue(string queuePath, bool transactional)
        {
            if (MessageQueue.Exists(queuePath))
            {
                MessageQueue.Delete(queuePath);
            }
            MessageQueue.Create(queuePath, transactional);
        }

        public static void DeleteQueue(string queuePath)
        {
            if (MessageQueue.Exists(queuePath))
            {
                MessageQueue.Delete(queuePath);
            }
        }

        public static void MessageShouldMatch(TestMessage expected, TestMessage actual)
        {
            Contract.Requires(expected != null, "expected is null.");
            Contract.Requires(actual != null, "actual is null.");
            
            actual.PropDateTime.Should().Be(expected.PropDateTime);
            actual.PropInt.Should().Be(expected.PropInt);
            actual.PropNullableDateTime.Should().Be(expected.PropNullableDateTime);
            actual.PropNullableInt.Should().Be(expected.PropNullableInt);
            actual.PropString.Should().Be(expected.PropString);
        }

        public static TestMessage BuildTestMessageWithRandomValues()
        {
            var result = new TestMessage()
            {
                GuidProp = Guid.NewGuid(),
                PropDateTime = new DateTime(2011, 11, 21, 10, 15, 46),
                PropInt = 10,
                PropNullableDateTime = null,
                PropNullableInt = null,
                PropString = "Test"
            };
            return result;
        }

    }
}
