﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons.Messaging.Msmq;
using System.Messaging;
using FluentAssertions;
using System.Transactions;
using System.Threading;
using TEAM.Commons.Tests.Messaging.Infrastructure;

namespace TEAM.Commons.IntegrationTests.Messaging
{
    [TestClass]
    public class MsmqTransportsFixture
    {

        public const string TestNonTransactionalQueuePath = ".\\Private$\\team.commons.messaging.integrationtests_NonTransactional";
        public const string TestTransactionalQueuePath = ".\\Private$\\team.commons.messaging.integrationtests_Transactional";
        public const string ShouldntExistQueuePath = ".\\Private$\team.commons.messaging._ThisQueueShouldntExist_";

        [ClassInitialize]
        public static void InitializeEnvironment(TestContext context)
        {
            TestHelper.DeleteQueue(ShouldntExistQueuePath);

            TestHelper.EnsureQueue(TestNonTransactionalQueuePath, false);
            TestHelper.EnsureQueue(TestTransactionalQueuePath, true);
        }

        [ClassCleanup]
        public static void CleanUpEnvironment()
        {
            TestHelper.DeleteQueue(TestNonTransactionalQueuePath);
            TestHelper.DeleteQueue(TestTransactionalQueuePath);
        }

        [TestCleanup]
        public void CleanUpQueues()
        {
            var queue = new MessageQueue(TestNonTransactionalQueuePath);
            queue.Purge();

            queue = new MessageQueue(TestTransactionalQueuePath);
            queue.Purge();
        }

        [TestMethod]
        public void Should_fail_SendingTransport_creation_if_queue_doesnt_exist()
        {
            Action a = () => new MsmqSendingTransport(ShouldntExistQueuePath, new JsonMessageSerializer());
            a.ShouldThrow<InvalidOperationException>();
        }

        [TestMethod]
        public void Should_fail_ReceivingTransport_creation_if_queue_doesnt_exist()
        {
            Action a = () => new MsmqReceivingTransport(ShouldntExistQueuePath, new JsonMessageSerializer());
            a.ShouldThrow<InvalidOperationException>();
        }

        [TestMethod]
        public void Should_send_and_receive_non_transactional_typed_message_with_a_unique_id_assigned()
        {
            var sendingSut = new MsmqSendingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            var message = TestHelper.BuildTestMessageWithRandomValues();
            sendingSut.Send(message);

            var underlyingQueue = new MessageQueue(TestNonTransactionalQueuePath, QueueAccessMode.ReceiveAndAdmin);

            var allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().HaveCount(1);

            var receivingSut = new MsmqReceivingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            var receivedMessage = receivingSut.WaitForMessage(TimeSpan.FromMilliseconds(1));

            receivedMessage.Should().NotBeNull();
            receivedMessage.UniqueId.Should().NotBeNullOrEmpty();
            receivedMessage.Messages.Should().HaveCount(1);

            receivedMessage.Messages[0].Should().BeOfType<TestMessage>();

            var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];

            TestHelper.MessageShouldMatch(message, receivedTestMessage);
        }

        [TestMethod]
        public void Should_send_and_receive_transactional_typed_message_receiving_inside_transaction()
        {
            var sut = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var message = TestHelper.BuildTestMessageWithRandomValues();
            using (var tx = new TransactionScope())
            {
                sut.Send(message);
                tx.Complete();
            }

            var underlyingQueue = new MessageQueue(TestTransactionalQueuePath, QueueAccessMode.ReceiveAndAdmin);

            var allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().HaveCount(1);

            var receivingSut = new MsmqReceivingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            using (var tx = new TransactionScope())
            {
                var receivedMessage = receivingSut.WaitForMessage(TimeSpan.FromMilliseconds(1));

                receivedMessage.Should().NotBeNull();
                receivedMessage.UniqueId.Should().NotBeNullOrEmpty();
                receivedMessage.Messages.Should().HaveCount(1);

                receivedMessage.Messages[0].Should().BeOfType<TestMessage>();

                var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];

                TestHelper.MessageShouldMatch(message, receivedTestMessage);

                tx.Complete();
            }

            allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().BeEmpty();
        }

        [TestMethod]
        public void Should_send_and_receive_transactional_typed_message_receiving_outside_transaction()
        {
            var sut = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var message = TestHelper.BuildTestMessageWithRandomValues();
            using (var tx = new TransactionScope())
            {
                sut.Send(message);
                tx.Complete();
            }

            var underlyingQueue = new MessageQueue(TestTransactionalQueuePath, QueueAccessMode.ReceiveAndAdmin);

            var allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().HaveCount(1);

            var receivingSut = new MsmqReceivingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var receivedMessage = receivingSut.WaitForMessage(TimeSpan.FromMilliseconds(1));

            receivedMessage.Should().NotBeNull();
            receivedMessage.UniqueId.Should().NotBeNullOrEmpty();
            receivedMessage.Messages.Should().HaveCount(1);

            receivedMessage.Messages[0].Should().BeOfType<TestMessage>();

            var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];
            TestHelper.MessageShouldMatch(message, receivedTestMessage);
        }

        [TestMethod]
        public void Should_not_deliver_transactional_message_if_transaction_fails()
        {
            var sut = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var message = TestHelper.BuildTestMessageWithRandomValues();
            using (new TransactionScope())
            {
                sut.Send(message);
            }
            
            var underlyingQueue = new MessageQueue(TestTransactionalQueuePath, QueueAccessMode.ReceiveAndAdmin);
            
            var allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().BeEmpty();
        }

        [TestMethod]
        public void Should_not_receive_transactional_message_if_transaction_fails()
        {
            var sut = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var message = TestHelper.BuildTestMessageWithRandomValues();
            using (var tx = new TransactionScope())
            {
                sut.Send(message);
                tx.Complete();
            }

            var underlyingQueue = new MessageQueue(TestTransactionalQueuePath, QueueAccessMode.ReceiveAndAdmin);

            var allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().HaveCount(1);

            var receivingSut = new MsmqReceivingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            using (new TransactionScope())
            {
                var receivedMessage = receivingSut.WaitForMessage(TimeSpan.FromMilliseconds(1));

                receivedMessage.Should().NotBeNull();
                receivedMessage.UniqueId.Should().NotBeNullOrEmpty();
                receivedMessage.Messages.Should().HaveCount(1);

                receivedMessage.Messages[0].Should().BeOfType<TestMessage>();

                var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];
                TestHelper.MessageShouldMatch(message, receivedTestMessage);
            }
            Thread.Sleep(1); // Force a delay because sometimes the message is not picked up by the coming GetAllMessages call... I've checked manually and the message is there.
            allMessages = underlyingQueue.GetAllMessages();
            allMessages.Should().HaveCount(1, "The message should be in the queue because the read transaction failed");
        }

        [TestMethod]
        public void Should_send_and_receive_non_transactional_anonymous_message()
        {
            var sendingSut = new MsmqSendingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            var message = new TestMessage
            {
                GuidProp = Any.Guid(),
                PropDateTime = new DateTime(2011, 11, 21, 10, 15, 46),
                PropInt = Any.Int(),
                PropString = Any.String()
            };
            sendingSut.Send(message);

            var receivingSut = new MsmqReceivingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            var receivedTransportMessage = receivingSut.WaitForMessage(Timeout.InfiniteTimeSpan);

            receivedTransportMessage.Should().NotBeNull();
            receivedTransportMessage.UniqueId.Should().NotBeNullOrEmpty();
            receivedTransportMessage.Messages.Should().HaveCount(1);

            var receivedMessage = receivedTransportMessage.Messages[0];
            receivedMessage.AllPublicPropertiesEquals(message).Should().BeTrue();
        }

        [TestMethod]
        public void Receiving_transport_should_block_thread_and_return_false_if_timeout_occurs_before_a_message_becomes_available()
        {
            var sut = new MsmqReceivingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            DateTime startedAt = DateTime.Now;
            sut.BlockUntilMessageIsAvailable(TimeSpan.FromMilliseconds(300)).Should().BeFalse();
            DateTime.Now.Subtract(startedAt).TotalMilliseconds.Should().BeGreaterOrEqualTo(100);
        }

        [TestMethod]
        public void Receiving_transport_should_block_thread_and_return_true_when_a_non_transactional_message_becomes_available()
        {
            var failed = false;
            EventWaitHandle wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var message = TestHelper.BuildTestMessageWithRandomValues();

            var sendingTransport = new MsmqSendingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());

            var sut = new MsmqReceivingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            var blockingThread = new ThreadStart(() =>
            {
                try
                {
                    sut.BlockUntilMessageIsAvailable(Timeout.InfiniteTimeSpan);
                    var receivedMessage = sut.TryReceiveCurrentAvailableMessage();

                    receivedMessage.Should().NotBeNull();
                    var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];
                    TestHelper.MessageShouldMatch(message, receivedTestMessage);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    failed = true;
                }
                finally
                {
                    wait.Set();
                }
            });

            new Thread(blockingThread).Start();
            sendingTransport.Send(message);
            wait.WaitOne();
            if (failed)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Receiving_transport_should_block_thread_return_true_and_receive_when_a_transactional_message_becomes_available()
        {
            var failed = false;
            EventWaitHandle wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var message = TestHelper.BuildTestMessageWithRandomValues();

            var sendingTransport = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());

            var sut = new MsmqReceivingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var blockingThread = new ThreadStart(() =>
            {
                try
                {
                    sut.BlockUntilMessageIsAvailable(Timeout.InfiniteTimeSpan);
                    using (var tx = new TransactionScope())
                    {
                        var receivedMessage = sut.TryReceiveCurrentAvailableMessage();

                        receivedMessage.Should().NotBeNull();
                        var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];
                        TestHelper.MessageShouldMatch(message, receivedTestMessage);
                        tx.Complete();
                    }
                }
                catch
                {
                    failed = true;
                }
                finally
                {
                    wait.Set();
                }
            });

            new Thread(blockingThread).Start();
            using (var tx = new TransactionScope())
            {
                sendingTransport.Send(message);
                tx.Complete();
            }
            wait.WaitOne();
            if (failed)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TryReceiveCurrentAvailableMessage_should_leave_transactional_message_in_the_queue_if_the_transaction_fails()
        {
            var failed = false;
            EventWaitHandle wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var message = TestHelper.BuildTestMessageWithRandomValues();

            var sendingTransport = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());

            var sut = new MsmqReceivingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            var blockingThread = new ThreadStart(() =>
            {
                try
                {
                    sut.BlockUntilMessageIsAvailable(Timeout.InfiniteTimeSpan);
                    using (new TransactionScope())
                    {
                        var receivedMessage = sut.TryReceiveCurrentAvailableMessage();

                        receivedMessage.Should().NotBeNull();
                        var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];
                        TestHelper.MessageShouldMatch(message, receivedTestMessage);
                        // Transaction fails
                    }
                    var underlyingQueue = new MessageQueue(TestTransactionalQueuePath, QueueAccessMode.ReceiveAndAdmin);

                    var allMessages = underlyingQueue.GetAllMessages();
                    allMessages.Should().HaveCount(1);
                }
                catch
                {
                    failed = true;
                }
                finally
                {
                    wait.Set();
                }
            });

            new Thread(blockingThread).Start();
            using (var tx = new TransactionScope())
            {
                sendingTransport.Send(message);
                tx.Complete();
            }
            wait.WaitOne();
            if (failed)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Should_block_and_receive_non_transactional_typed_message()
        {
            var sendingSut = new MsmqSendingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            var message = TestHelper.BuildTestMessageWithRandomValues();
            sendingSut.Send(message);

            var receivingSut = new MsmqReceivingTransport(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            receivingSut.BlockUntilMessageIsAvailable(Timeout.InfiniteTimeSpan);

            var receivedMessage = receivingSut.TryReceiveCurrentAvailableMessage();

            receivedMessage.Should().NotBeNull();
            receivedMessage.UniqueId.Should().NotBeNullOrEmpty();
            receivedMessage.Messages.Should().HaveCount(1);

            receivedMessage.Messages[0].Should().BeOfType<TestMessage>();

            var receivedTestMessage = (TestMessage)receivedMessage.Messages[0];

            TestHelper.MessageShouldMatch(message, receivedTestMessage);
        }

    }
}
