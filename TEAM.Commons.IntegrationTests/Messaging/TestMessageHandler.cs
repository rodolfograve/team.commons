﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.IntegrationTests.Messaging
{
    public class TestMessageHandler : IMessageHandler<TestMessage>
    {

        public TestMessageHandler(Action<TestMessage> delegatedAction)
        {
            DelegatedAction = delegatedAction;
        }

        protected readonly Action<TestMessage> DelegatedAction;

        public void Handle(TestMessage message, IMessageProcessingContext processingContext)
        {
            DelegatedAction(message);
        }
    }
}
