﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.IntegrationTests.Messaging
{
   public class EmptyTestMessageHandler : IMessageHandler<EmptyTestMessage>
   {

       public EmptyTestMessageHandler(Action<EmptyTestMessage, IMessageProcessingContext> action)
       {
           Action = action;
       }

       public EmptyTestMessageHandler(Action<EmptyTestMessage> action)
       {
           Action = (m, _) => action(m);
       }

       protected readonly Action<EmptyTestMessage, IMessageProcessingContext> Action;

      public void Handle(EmptyTestMessage message, IMessageProcessingContext processingContext)
      {
         Action(message, processingContext);
      }
   }
}
