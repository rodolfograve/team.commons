﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using TEAM.Commons.Messaging;
using Autofac;
using System.Threading;
using TEAM.Commons.Messaging.Msmq;
using System.Messaging;
using System.Transactions;
using System.Diagnostics.Contracts;
using TEAM.Commons.Tests.Messaging.Infrastructure;

namespace TEAM.Commons.IntegrationTests.Messaging
{
    [TestClass]
    public class SingleThreadMessageBusFixture
    {

        public const string TestNonTransactionalQueuePath = ".\\Private$\\team.commons.messaging.messagebus.integrationtests_NonTransactional";
        public const string TestTransactionalQueuePath = ".\\Private$\\team.commons.messaging.messagebus.integrationtests_Transactional";

        [ClassInitialize]
        public static void InitializeEnvironment(TestContext context)
        {
            TestHelper.EnsureQueue(TestNonTransactionalQueuePath, false);
            TestHelper.EnsureQueue(TestTransactionalQueuePath, true);
        }

        [ClassCleanup]
        public static void CleanUpEnvironment()
        {
            TestHelper.DeleteQueue(TestNonTransactionalQueuePath);
            TestHelper.DeleteQueue(TestTransactionalQueuePath);
        }

        [TestCleanup]
        public void CleanUpQueues()
        {
            var queue = new MessageQueue(TestNonTransactionalQueuePath);
            queue.Purge();

            queue = new MessageQueue(TestTransactionalQueuePath);
            queue.Purge();
        }

        protected SingleThreadMessageBus BuildMessageBus(IReceivingTransport transport, ContainerBuilder builder)
        {
            Contract.Requires(transport != null, "transport is null.");
            Contract.Requires(builder != null, "builder is null.");

            var messageHandlersContainer = new AutofacMessageHandlersContainerAdapter(builder.Build());
            var map = new DictionaryMessageMap();

            var result = new SingleThreadMessageBus("Test", transport, messageHandlersContainer, map, new TransactionOptions()
                {
                    IsolationLevel = IsolationLevel.ReadCommitted
                },
                () => new MaxRetryAttemptsErrorHandlingStrategy(5, TimeSpan.Zero));
            return result;
        }

        protected MsmqReceivingTransport BuildTransactionalReceivingTransport()
        {
            var result = new MsmqReceivingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            return result;
        }

        protected MsmqSendingTransport BuildTransactionalSendingTransport()
        {
            var result = new MsmqSendingTransport(TestTransactionalQueuePath, new JsonMessageSerializer());
            return result;
        }

        [TestMethod]
        public void Should_process_incoming_messages_when_started()
        {
            var wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var count = 0;

            Action processMessageAction = () =>
                {
                    count++;
                    if (count >= 5)
                    {
                        wait.Set();
                    }
                };

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMessageHandler>(x => new TestMessageHandler(m => processMessageAction()));

            var receivingTransport = BuildTransactionalReceivingTransport();
            var sendingTransport = BuildTransactionalSendingTransport();

            var sut = BuildMessageBus(receivingTransport, builder);
            sut.Start();

            var sendMessagesThread = new ThreadStart(() =>
            {
                try
                {
                    using (var tx = new TransactionScope())
                    {
                        sendingTransport.Send(new TestMessage());
                        sendingTransport.Send(new TestMessage());
                        sendingTransport.Send(new TestMessage());
                        sendingTransport.Send(new TestMessage());
                        sendingTransport.Send(new TestMessage());
                        tx.Complete();
                    }
                }
                catch
                {
                    wait.Set();
                }
            });

            new Thread(sendMessagesThread).Start();
            wait.WaitOne();
            sut.Stop();
            count.Should().Be(5);
        }

        [TestMethod]
        public void Should_call_registered_handler_for_a_message_without_properties()
        {
            var wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<EmptyTestMessageHandler>(x => new EmptyTestMessageHandler(m => wait.Set()));

            var transport = BuildTransactionalSendingTransport();
            using (var tx = new TransactionScope())
            {
                transport.Send(new EmptyTestMessage());
                tx.Complete();
            }

            var sut = BuildMessageBus(BuildTransactionalReceivingTransport(), builder);
            sut.Start();
            wait.WaitOne(1000).Should().BeTrue("the registered message handler should have been invoked");
            sut.Stop();
            var queue = new MessageQueue(TestTransactionalQueuePath);
            queue.GetAllMessages().Length.Should().Be(0, "the message should have been removed from the queue");
        }

        [TestMethod]
        public void Should_stop_and_not_remove_poison_message_from_ReceivingTransport()
        {
            Exception fatalException = null;
            var wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<EmptyTestMessageHandler>(x => new EmptyTestMessageHandler(m => { throw new ApplicationException("Test exception"); }));

            var transport = BuildTransactionalSendingTransport();
            using (var tx = new TransactionScope())
            {
                transport.Send(new EmptyTestMessage());
                tx.Complete();
            }

            var sut = BuildMessageBus(BuildTransactionalReceivingTransport(), builder);
            sut.Stopped += x =>
               {
                   fatalException = x.Exception;
                   wait.Set();
               };

            sut.Start();
            wait.WaitOneDebuggerFriendly(500).Should().BeTrue("the service should have been stopped due to a fatal exception");
            fatalException.Should().NotBeNull();
            var queue = new MessageQueue(TestTransactionalQueuePath);
            queue.GetAllMessages().Length.Should().Be(1, "the message should stay in the queue");
        }

        [TestMethod]
        public void Should_not_remove_message_from_ReceivingTransport_when_AbortCurrentMessage_requested()
        {
            Exception fatalException = null;
            var wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<EmptyTestMessageHandler>(x => new EmptyTestMessageHandler((m, context) => context.AbortCurrentMessage()));

            var transport = BuildTransactionalSendingTransport();
            using (var tx = new TransactionScope())
            {
                transport.Send(new EmptyTestMessage());
                tx.Complete();
            }

            var sut = BuildMessageBus(BuildTransactionalReceivingTransport(), builder);
            sut.Stopped += x =>
            {
                fatalException = x.Exception;
                wait.Set();
            };

            sut.Start();
            wait.WaitOne(1000).Should().BeFalse("the service should NOT have been stopped");
            sut.Stop();

            fatalException.Should().BeNull();
            var queue = new MessageQueue(TestTransactionalQueuePath);
            queue.GetAllMessages().Length.Should().Be(1, "the message should stay in the queue");
        }

        [TestMethod]
        public void Should_retry_processing_a_message_5_times()
        {
            Exception fatalException = null;
            var wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var builder = new ContainerBuilder();
            int retryCount = 1;
            int messageProcessing = 0;
            int messageProcessed = 0;
            builder.RegisterMessageHandler<EmptyTestMessageHandler>(x => new EmptyTestMessageHandler(m =>
            {
                if (retryCount < 5)
                {
                    retryCount++;
                    throw new ApplicationException("Test exception");
                }
            }));

            var transport = BuildTransactionalSendingTransport();
            using (var tx = new TransactionScope())
            {
                transport.Send(new EmptyTestMessage());
                tx.Complete();
            }

            var sut = BuildMessageBus(BuildTransactionalReceivingTransport(), builder);
            sut.Stopped += x =>
            {
                fatalException = x.Exception;
                wait.Set();
            };
            sut.MessageProcessing += x => messageProcessing++;
            sut.MessageProcessed += x => messageProcessed++;

            sut.Start();
            wait.WaitOne(1000).Should().BeFalse("the service should NOT have been stopped");
            retryCount.Should().Be(5, "the message should have been retried " + 5 + " times");
            messageProcessing.Should().Be(retryCount, "MessageProcessing must be invoked once per retry attempt");
            messageProcessed.Should().Be(1, "MessageProcessed must be invoked only once");
            fatalException.Should().BeNull();
            var queue = new MessageQueue(TestTransactionalQueuePath);
            queue.GetAllMessages().Length.Should().Be(0, "the message should have been removed from the queue");
        }

    }
}
