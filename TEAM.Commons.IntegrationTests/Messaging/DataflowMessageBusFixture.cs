﻿using System;
using FluentAssertions;
using TEAM.Commons.Messaging;
using System.Transactions;
using System.Messaging;
using System.Threading.Tasks;
using NUnit.Framework;
using TEAM.Commons.Tests.Messaging.Infrastructure;

namespace TEAM.Commons.IntegrationTests.Messaging
{
    [TestFixture]
   public class DataflowMessageBusFixture
   {

      class Sut
      {
         Task task;
         bool StopRequested;
         public int Counter;
         public void Start()
         {
            task = Task.Run(async () =>
               {
                  while (!StopRequested)
                  {
                     Counter++;
                     await Task.Delay(100);
                  }
               });
         }
         public Task StopAsync()
         {
            StopRequested = true;
            return task;
         }
      }

      [Test]
      public async Task T()
      {
         var sut = new Sut();
         sut.Start();
         await Task.Delay(2000);
         await sut.StopAsync();
         sut.Counter.Should().BeGreaterThan(1);
      }

      public const string TestNonTransactionalQueuePath = ".\\Private$\\team.commons.messaging.dataflow.integrationtests_NonTransactional";

      [Test]
      [Ignore("Work in progress")]
      public void Peek_on_transaction_Queue()
      {
         const string queuePath = "test-transactional";
         if (MessageQueue.Exists(queuePath))
         {
            MessageQueue.Delete(queuePath);
         }
         MessageQueue.Create(queuePath, true);
         using (var q = new MessageQueue(queuePath))
         {
             using (var tx = new TransactionScope())
             {
                 //q.Send();
             }
         }
      }

      [Test]
      [Ignore("This feature is not fully implemented")]
      public async Task Should_()
      {
         const int numberOfMessages = 1000;
         if (MessageQueue.Exists(TestNonTransactionalQueuePath))
         {
            MessageQueue.Delete(TestNonTransactionalQueuePath);
         }
         MessageQueue.Create(TestNonTransactionalQueuePath, false);

         using (var mq = new MessageQueue(TestNonTransactionalQueuePath))
         {
            for (int i = 0; i < numberOfMessages; i++)
            {
               var message = new Message() { Label = "TEST-" + i, Recoverable = true };

               var serializedMessage = new JsonMessageSerializer().Serialize(CreateTransportMessage(new { Prop1 = "Value1" }));
               message.BodyStream.Write(serializedMessage, 0, serializedMessage.Length);
               mq.Send(message);
            }

            var sut = new DataflowMessageBus(TestNonTransactionalQueuePath, new JsonMessageSerializer());
            sut.Start();
            WaitUntilQueueIsEmpty(mq);
            await sut.StopAsync();
            sut.MessagesProcessed.Should().Be(numberOfMessages);
            Console.WriteLine("Milliseconds per message: " + ((double)sut.MessagesProcessed) / sut.RunningTime.TotalMilliseconds);
         }
      }

      private static void WaitUntilQueueIsEmpty(MessageQueue mq)
      {
         Message peekedMessage = null;
         do
         {
            try
            {
               peekedMessage = mq.Peek(TimeSpan.FromMilliseconds(10));
            }
            catch (MessageQueueException ex)
            {
               if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
               {
                  peekedMessage = null;
               }
            }
         } while (peekedMessage != null);
      }

      private TransportMessage CreateTransportMessage(object message)
      {
         var result = new TransportMessage
         {
            Messages = new object[] { message }
         };
         return result;
      }
   }
}
