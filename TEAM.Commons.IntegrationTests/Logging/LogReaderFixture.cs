﻿using NUnit.Framework;
using FluentAssertions;
using System.Collections.Generic;
using TEAM.Commons.Logging;

namespace TEAM.Commons.IntegrationTests.Logging
{
    [TestFixture]
    public class LogReaderFixture
    {
        [Test]
        public void Should_read_log_file()
        {
            var lines = new List<LogEntry>();
            using (var logReader = new JsonLogReader("Logging\\test.log"))
            {
                LogEntry logEntry;
                do
                {
                    logEntry = logReader.ReadNextLogEntry();
                    if (logEntry != null)
                    {
                        lines.Add(logEntry);
                    }
                } while (logEntry != null);
            }
            lines.Should().HaveCount(1);
        }
    }
}
