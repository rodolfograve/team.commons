﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class RandomNumberGeneratorFixture
    {
        [TestMethod]
        public void Should_generate_random_number_when_min_and_max_are_same()
        {
            var result = Data.GenerateRandomInt(10, 10);
            result.Should().Be(10);
        }
    }
}
