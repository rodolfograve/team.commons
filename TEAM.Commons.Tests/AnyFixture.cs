﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class AnyFixture
    {
        [TestMethod]
        public void Should_generate_random_long_in_the_specified_range()
        {
            const long min = 50;
            const long max = min + 1;
            var result = Any.Long(min, max);
            result.Should().BeInRange(min, max);
        }

        [TestMethod]
        public void Should_generate_random_double_in_the_specified_range()
        {
            const double min = 50;
            const double max = min + 1;
            var result = Any.Double(min, max);
            result.Should().BeInRange(min, max);
        }

        [TestMethod]
        public void Should_generate_random_decimal_in_the_specified_range_with_decimalPlaces()
        {
            const decimal min = 50;
            const decimal max = min + 1;
            const int expectedDecimalPlaces = 2;

            var result = Any.Decimal(min, max, expectedDecimalPlaces);
            int decimalPlaces = BitConverter.GetBytes(decimal.GetBits(result)[3])[2];

            result.Should().BeInRange(min, max);
            decimalPlaces.Should().BeLessOrEqualTo(expectedDecimalPlaces);
        }

        [TestMethod]
        public void Should_generate_random_double_in_the_default_range()
        {
            var result = Any.Double();
            result.Should().BeInRange(double.MinValue, double.MaxValue);
        }

        [TestMethod]
        public void Should_generate_random_decimal_in_the_default_range()
        {
            var result = Any.Decimal();
            result.Should().BeInRange(decimal.MinValue, decimal.MaxValue);
        }
    }
}
