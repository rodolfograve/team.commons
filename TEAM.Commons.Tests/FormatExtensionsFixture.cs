﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class FormatExtensionsFixture
    {

        [TestMethod]
        public void ToStringOrNull_returns_null_for_nullable_null_with_default_nullString()
        {
            int? sut = null;
            var result = sut.ToStringOrNull();
            result.Should().Be("NULL");
        }

        [TestMethod]
        public void ToStringOrNull_returns_null_for_nullable_null()
        {
            int? sut = null;
            var result = sut.ToStringOrNull("TEST");
            result.Should().Be("TEST");
        }

        [TestMethod]
        public void ToStringOrNull_returns_ToString_for_nullable_with_value()
        {
            int? sut = Any.Int();
            var result = sut.ToStringOrNull("TEST");
            result.Should().Be(sut.ToString());
        }

        [TestMethod]
        public void ToStringOrNull_returns_null_for_null_object_with_default_nullString()
        {
            string sut = null;
            var result = sut.ToStringOrNull();
            result.Should().Be("NULL");
        }

        [TestMethod]
        public void ToStringOrNull_returns_null_for_null_object()
        {
            string sut = null;
            var result = sut.ToStringOrNull("TEST");
            result.Should().Be("TEST");
        }

        [TestMethod]
        public void ToStringOrNull_returns_ToString_for_object_with_value()
        {
            var sut = Any.String();
            var result = sut.ToStringOrNull("TEST");
            result.Should().Be(sut.ToString());
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Null()
        {
            SimpleClass dto = null;
            string result = dto.PrettyFormat();

            result.Should().Be("<NULL>");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Instance_of_Class_without_Namespace()
        {
           ClassWithoutNamespace dto = new ClassWithoutNamespace { Str = "test" };
           string result = dto.PrettyFormat();

           result.Should().NotBeNullOrWhiteSpace();
           result.Should().Contain("Str").And.Contain("test");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_NullableInt()
        {
            int? dto = null;
            string result = dto.PrettyFormat();

            result.Should().Be("<NULL>");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_NullableDateTime()
        {
            DateTime? dto = null;
            string result = dto.PrettyFormat();

            result.Should().Be("<NULL>");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_LocalDateTime()
        {
           DateTime dto = new DateTime(2013, 7, 15, 14, 51, 37, DateTimeKind.Local);
           string result = dto.PrettyFormat();

         result.Should().Match("2013-07-15 14:51:37 +0*:00 [DateTimeKind: Local]");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_UtcDateTime()
        {
           DateTime dto = new DateTime(2013, 7, 15, 14, 51, 37, DateTimeKind.Utc);
           string result = dto.PrettyFormat();

           result.Should().Be("2013-07-15 14:51:37Z [DateTimeKind: Utc]");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_UnspecifiedDateTime()
        {
           DateTime dto = new DateTime(2013, 7, 15, 14, 51, 37, DateTimeKind.Unspecified);
           string result = dto.PrettyFormat();

           result.Should().Be("2013-07-15 14:51:37 [DateTimeKind: Unspecified]");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Decimal()
        {
            decimal d = 5;
            string result = d.PrettyFormat();

            result.Should().Be("5");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Int()
        {
            int d = 5;
            string result = d.PrettyFormat();

            result.Should().Be("5");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Double()
        {
            double d = 5;
            string result = d.PrettyFormat();

            result.Should().Be("5");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Float()
        {
            float d = 5;
            string result = d.PrettyFormat();

            result.Should().Be("5");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_String()
        {
            string d = "5";
            string result = d.PrettyFormat();

            result.Should().Be("5");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Enum()
        {
            SimpleEnum dto = SimpleEnum.Second;
            string result = dto.PrettyFormat();

            result.Should().Contain(dto.ToString());
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_SimpleClass()
        {
            SimpleClass dto = new SimpleClass()
            {
                DateProperty = new DateTime(2000, 12, 12),
                IntProperty = Data.GenerateRandomInt(),
                StringProperty = Data.GenerateRandomString(),
                GuidProperty = Data.GenerateRandomGuid()
            };
            string result = dto.PrettyFormat();

            result.Should().NotBeNullOrWhiteSpace();
            result.Should().NotContain(Environment.NewLine);
            result.Should().Be("SimpleClass { StringProperty='" + dto.StringProperty + "', IntProperty='" + dto.IntProperty + "', DateProperty='" + dto.DateProperty.Value.ToString("yyyy-MM-dd HH:mm:ss") + " [DateTimeKind: Unspecified]', GuidProperty='" + dto.GuidProperty + "', }");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_CyclicClass()
        {
            CyclicClass c = new CyclicClass()
            {
                IntProperty = 7,
                SimpleProperty = new SimpleClass()
                {
                    IntProperty = 5,
                    DateProperty = new DateTime(2010, 03, 02),
                    StringProperty = "My string"
                }
            };
            c.CyclicProperty = c;

            string result = c.PrettyFormat();
            result.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void PrettyFormat_Does_NOT_Output_Protected_Properties_Or_Fields()
        {
            SimpleClass dto = new SimpleClass("protectedValue")
            {
                DateProperty = new DateTime(2000, 12, 12),
                IntProperty = 1000,
                StringProperty = "Str"
            };

            var result = dto.PrettyFormat();

            result.Should().NotContainEquivalentOf("protectedValue", "Protected properties should be formatted");
        }

        [TestMethod]
        public void PrettyFormat_Does_NOT_Output_Readonly_Fields()
        {
            SimpleClass dto = new SimpleClass();
            string result = dto.PrettyFormat();

            result.Should().NotContain("ReadonlyField");
        }

        [TestMethod]
        public void PrettyFormat_Does_NOT_Output_Const_Fields()
        {
            SimpleClass dto = new SimpleClass();
            string result = dto.PrettyFormat();

            result.Should().NotContain("ConstField");
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_EmptyArrays()
        {
            string[] s = new string[5];
            string result = s.PrettyFormat();

            result.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_SimpleClass_With_EmptyArray_Property()
        {
            SimpleClassWithArray s = new SimpleClassWithArray();
            s.MyArrayProp = new string[5];
            string result = s.PrettyFormat();

            result.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void PrettyFormat_Can_Format_Enumerable()
        {
            var s = Enumerable.Range(0, 10).Select(x => new SimpleClass() { IntProperty = x, StringProperty = x.ToString("00") });
            string result = s.PrettyFormat();

            result.Should().NotBeNullOrWhiteSpace();
        }
    }
}
