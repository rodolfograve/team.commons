﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class StringExtensionsFixture
    {

        [TestMethod]
        public void Should_return_null_when_target_is_null()
        {
            string sut = null;
            sut.SafeSubstring(0, 10).Should().BeNull();
        }

        [TestMethod]
        public void Should_return_target_when_target_is_empty()
        {
            const string sut = "";
            sut.SafeSubstring(0, 10).Should().BeEmpty();
        }

        [TestMethod]
        public void Should_return_the_full_string_when_startIndex_is_0_and_range_is_longer_than_valid()
        {
            const string sut = "Test string";
            sut.SafeSubstring(0, 100).Should().Be(sut);
        }

        [TestMethod]
        public void Should_return_the_full_string_when_startIndex_is_0_and_range_is_shorter_than_valid()
        {
            const string sut = "Test string";
            sut.SafeSubstring(0, 4).Should().Be("Test");
        }

        [TestMethod]
        public void Should_return_the_right_string_when_startIndex_is_not_0_and_range_is_longer_than_valid()
        {
           const string sut = "Test string";
            sut.SafeSubstring(5, 100).Should().Be("string");
        }

        [TestMethod]
        public void Should_return_the_right_string_when_startIndex_is_not_0_and_range_is_shorter_than_valid()
        {
           const string sut = "Test string";
           sut.SafeSubstring(5, 3).Should().Be("str");
        }

        [TestMethod]
        public void Should_return_empty_when_the_startIndex_is_greater_than_the_length()
        {
           const string sut = "Test string";
           sut.SafeSubstring(sut.Length + 1, 3).Should().BeEmpty();
        }

        [TestMethod]
        public void Should_return_empty_when_the_startIndex_is_equal_to_the_length()
        {
           const string sut = "Test string";
           sut.SafeSubstring(sut.Length, 3).Should().BeEmpty();
        }

    }
}
