﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System.Diagnostics;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class TraceExtensionsFixture
    {

        [TestMethod]
        public void Should_build_TraceMessage()
        {
            var result = TraceExtensions.BuildTraceMessage(TraceEventType.Start, "test message", "DoIt", "c:\\test file path\\file.cs", 12);
            result.Should().Be("[file.cs@12-DoIt] says: test message.");
        }

        [TestMethod]
        public void Should_trace_message_with_caller_info()
        {
            var sut = new TraceSource("test");
            sut.TraceEventWithCallerInfo(TraceEventType.Start, "test message");
        }

    }
}
