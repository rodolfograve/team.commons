﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System.Collections.Generic;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class DataGenerationExtensionsFixture
    {
        [TestMethod]
        public void Should_fail_to_generate_different_from_empty_keeping_the_same_length()
        {
            Action a = () => "".GenerateDifferentWithSameLength();
            a.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void Should_fail_to_generate_different_from_null_keeping_the_same_length()
        {
            Action a = () => ((string)null).GenerateDifferentWithSameLength();
            a.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void Should_generate_string_different_from_s_keeping_same_length()
        {
            const string sut = "s";
            var result = sut.GenerateDifferentWithSameLength();

            result.Should().NotBe(sut);
            result.Length.Should().Be(sut.Length);
        }

        [TestMethod]
        public void Should_generate_string_different_from_X_keeping_same_length()
        {
            const string sut = "X";
            var result = sut.GenerateDifferentWithSameLength();

            result.Should().NotBe(sut);
            result.Length.Should().Be(sut.Length);
        }

        [TestMethod]
        public void Should_generate_string_different_from_long_string_keeping_same_length()
        {
            const string sut = "some long text";
            var result = sut.GenerateDifferentWithSameLength();

            result.Should().NotBe(sut);
            result.Length.Should().Be(sut.Length);
        }

        [TestMethod]
        public void Should_generate_string_different_from_s()
        {
            const string sut = "s";
            var result = sut.GenerateDifferent();

            result.Should().NotBe(sut);
            result.Should().Contain(sut);
        }

        [TestMethod]
        public void Should_generate_string_different_from_long_string()
        {
            const string sut = "some long text";
            var result = sut.GenerateDifferent();

            result.Should().NotBe(sut);
            result.Should().Contain(sut);
        }

        [TestMethod]
        public void Should_generate_Guid_different()
        {
            var sut = Guid.NewGuid();
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_different_int()
        {
            const int sut = 10;
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_different_int_for_max_int()
        {
            const int sut = int.MaxValue;
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_different_int_for_min_int()
        {
            const int sut = int.MinValue;
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_different_long()
        {
            const long sut = (long)int.MaxValue + 1000;
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_different_long_for_max_long()
        {
            const long sut = long.MaxValue;
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_different_long_for_min_long()
        {
            const long sut = long.MinValue;
            sut.GenerateDifferent().Should().NotBe(sut);
        }

        [TestMethod]
        public void Should_generate_random_long_in_the_specified_range()
        {
            var result = Data.GenerateRandomLong(1000, 1000);
            result.Should().Be(1000);
        }

        [TestMethod]
        public void Should_generate_random_double_in_the_specified_range()
        {
            const double min = 50;
            const double max = min + 1;
            var result = Data.GenerateRandomDouble(min, max);
            result.Should().BeInRange(min, max);
        }

        [TestMethod]
        public void Should_generate_random_decimal_in_default_range()
        {
            var result = Data.GenerateRandomDecimal();
            result.Should().BeInRange(decimal.MinValue, decimal.MaxValue);
        }

        [TestMethod]
        public void Should_generate_random_decimal_in_the_specified_range_with_decimalPlaces()
        {
            const decimal min = 50;
            const decimal max = min + 1;
            const int expectedDecimalPlaces = 2;

            var result = Data.GenerateRandomDecimal(min, max, expectedDecimalPlaces);
            int decimalPlaces = BitConverter.GetBytes(decimal.GetBits(result)[3])[2];

            result.Should().BeInRange(min, max);
            decimalPlaces.Should().BeLessOrEqualTo(expectedDecimalPlaces);
        }

        [TestMethod]
        public void Should_generate_random_date_in_the_specified_range()
        {
            var result = Data.GenerateRandomDateTime(new DateTime(2015, 11, 9), new DateTime(2015, 11, 9));
            result.Should().Be(new DateTime(2015, 11, 9));
        }

        [TestMethod]
        public void Should_pick_single_element_from_list()
        {
            var list = new List<string>() { Any.String() };
            var result = list.RandomPick();
            result.Should().Be(list[0]);
        }
    }
}
