﻿using System;
using System.Data.SqlClient;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class DbConnectionExtensionsFixture
    {

        private const string SomeConnectionString = "Data Source=someserver;Initial Catalog=somedatabase;User ID=someuser;Password=somepassword";

        [TestMethod]
        public void Creates_Command_With_Parameters_From_Anonymous_Object()
        {
            using (var sut = new SqlConnection(SomeConnectionString))
            {
                using (var cmd = sut.CreateCommandText(
                    "select * from sometable where Id=@Id AND @Nullable is null",
                    new { Id = 5, MyNullable = (string)null }))
                {
                    cmd.Should().NotBeNull();
                    cmd.Parameters.Should().HaveCount(2);
                    cmd.Parameters[0].ParameterName.Should().Be("@Id");
                    cmd.Parameters[1].Value.Should().Be(DBNull.Value);
                    cmd.Parameters[0].Value.Should().Be(5);
                }
            }
        }

        [TestMethod]
        public void Should_compile()
        {
            using (var sut = new SqlConnection(SomeConnectionString))
            {
                var result = sut.GetAllWithStreaming<SimpleClass>("select * from sometable");
            }
        }
    }
}
