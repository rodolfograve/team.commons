﻿using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace TEAM.Commons.Tests
{
    [TestFixture]
    public class ProcessMethodDelegateCacheFixture
    {
        [Test]
        public void Should_return_null_if_Process_action_not_found()
        {
            var process = ProcessMethodDelegateCache<SampleAction>.ForAction<string>.ProcessAction;
            process.Should().BeNull();
        }

        [Test]
        public void Should_invoke_Process_action()
        {
            var process = ProcessMethodDelegateCache<SampleAction>.ForAction<int>.ProcessAction;
            var receiver = new SampleAction();
            process(receiver, Any.Int());
            receiver.ProcessIntCounter.Should().Be(1);
        }

        [Test]
        public void Should_return_null_if_Process_func_with_one_input_not_found()
        {
            var process = ProcessMethodDelegateCache<SampleFunc>.ForFunc<string, int>.ProcessFunc;
            process.Should().BeNull();
        }

        [Test]
        public void Should_invoke_Process_func_with_one_input()
        {
            var process = ProcessMethodDelegateCache<SampleFunc>.ForFunc<int, string>.ProcessFunc;
            var receiver = new SampleFunc();
            var input = Any.Int();
            var result = process(receiver, input);
            result.Should().Be(input.ToString());
        }

        [Test]
        public void Should_return_null_if_Process_func_with_two_input_not_found()
        {
            var process = ProcessMethodDelegateCache<SampleFunc2>.ForFunc<string, int, object>.ProcessFunc;
            process.Should().BeNull();
        }

        [Test]
        public void Should_invoke_Process_func_with_two_input()
        {
            var process = ProcessMethodDelegateCache<SampleFunc2>.ForFunc<int, string, string>.ProcessFunc;
            var receiver = new SampleFunc2();
            var input1 = Any.Int();
            var input2 = Any.String();
            var result = process(receiver, input1, input2);
            result.Should().Be(input1.ToString() + input2);
        }

        private class SampleAction
        {
            public int ProcessIntCounter { get; private set; }
            private void Process(int i) => ProcessIntCounter++;
        }
        private class SampleFunc
        {
            protected string Process(int i) => i.ToString();
        }

        private class SampleFunc2
        {
            public string Process(int i, string s) => i.ToString() + s;
        }

    }
}
