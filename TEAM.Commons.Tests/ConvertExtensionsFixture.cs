﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEAM.Commons.Tests
{
   [TestClass]
   public class ConvertExtensionsFixture
   {

      [TestMethod]
      public void Should_convert_GUID()
      {
         var input = new Guid("{22539BDB-08A1-4ED2-B1BA-AA1D4ACF71AF}");
         var output = ConvertExtensions.ChangeType(input, typeof (Guid));

         Assert.AreEqual(input, output);
      }

   }
}
