﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Tests
{
    public class SimpleClass
    {
        public SimpleClass()
        {

        }

        public SimpleClass(string protectedStringValue)
        {
            ProtectedString = protectedStringValue;
        }

        public readonly string ReadonlyField = "You shouldn't see this 'readonly field' in PrettyFormat";

        public const string ConstField = "You shouldn't see this 'const' in PrettyFormat";

        public string StringProperty { get; set; }

        public int IntProperty { get; set; }

        public DateTime? DateProperty { get; set; }

        public Guid GuidProperty { get; set; }

        protected string ProtectedString { get; set; }

    }
}
