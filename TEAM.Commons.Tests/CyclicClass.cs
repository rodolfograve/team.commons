﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Tests
{
    public class CyclicClass
    {

        public int IntProperty { get; set; }

        public SimpleClass SimpleProperty { get; set; }

        public CyclicClass CyclicProperty { get; set; }

    }
}
