﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class WorkerThreadFixture : IDisposable
    {

        private AutoResetEvent Wait;
        private int SimpleGlobalResource;
        private WorkerThread Worker;

        [TestInitialize]
        public void Initialize()
        {
            Wait = new AutoResetEvent(false);
        }

        [TestMethod]
        public void DelegateWorkerThread_Can_Execute_SimpleTask()
        {
            Worker = new DelegateWorkerThread("Test", new DelegateWorkerThread.WorkerActionDelegate(SimpleTask));
            Worker.Stopped += (sender, e) => Wait.Set();
            Worker.Start();

            bool workerStopped = Wait.WaitOneDebuggerFriendly(100);

            workerStopped.Should().BeTrue("Worker must have Set the Wait");
            SimpleGlobalResource.Should().BeGreaterThan(0, "The task must've been executed at least once");
        }

        protected void SimpleTask()
        {
            SimpleGlobalResource++;
            Worker.StopAsync();
        }

        [TestMethod]
        public void WorkerThread_Stops_When_Exception_Is_Thrown_And_Passes_The_Exception()
        {
            Worker = new DelegateWorkerThread("Test", ThrowExceptionTask);
            Exception error = null;
            Worker.Stopped += (sender, e) =>
            {
                error = e.Error;
                Wait.Set();
            };
            Worker.Start();

            bool workerStopped = Wait.WaitOneDebuggerFriendly(100);

            workerStopped.Should().BeTrue("Worker must have Set the Wait");
            error.Should().NotBeNull("The WorkerThread should've included the exception in the even.");
            error.Message.Should().Be("My test exception");
        }

        protected void ThrowExceptionTask()
        {
            throw new Exception("My test exception");
        }

        [TestMethod]
        public void WorkerThread_Stops_When_Requested()
        {
            Worker = new DelegateWorkerThread("Test", new DelegateWorkerThread.WorkerActionDelegate(DelayTask));
            Worker.Stopped += (sender, e) => Wait.Set();
            Worker.Start();

            bool workerStopped = Wait.WaitOneDebuggerFriendly(500);

            workerStopped.Should().BeTrue("Worker must have Set the Wait");
            StopsWhenRequestedGlobalResource.Should().BeGreaterThan(0, "The task must've been executed at least once");
            Worker.IsStopped.Should().BeTrue("The Worker should be stopped");
        }

        protected int StopsWhenRequestedGlobalResource;

        protected void DelayTask()
        {
            StopsWhenRequestedGlobalResource++;
            Thread.Sleep(100);
            Worker.StopAsync();
        }

        public void Dispose()
        {
            if (Wait != null)
            {
                Wait.Dispose();
                Wait = null;
            }
        }

    }
}
