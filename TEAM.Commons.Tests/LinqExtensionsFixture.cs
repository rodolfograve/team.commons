﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class LinqExtensionsFixture
    {
        [TestMethod]
        public void Distinct_Can_Use_EqualsFunc_And_HashFunc()
        {
            IList<SimpleClass> source = new List<SimpleClass>();
            var simpleClass1 = new SimpleClass()
            {
                IntProperty = 1,
                StringProperty = "1"
            };
            source.Add(simpleClass1);

            var simpleClass2 = new SimpleClass()
            {
                IntProperty = 2,
                StringProperty = "2"
            };
            source.Add(simpleClass2);

            var simpleClass3 = new SimpleClass()
            {
                IntProperty = 1,
                StringProperty = "2"
            };
            source.Add(simpleClass3);

            IEnumerable<SimpleClass> result = source.Distinct((x, y) => x.IntProperty == y.IntProperty, x => x.IntProperty.GetHashCode());

            result.Should().HaveCount(2);
            result.Should().ContainInOrder(new SimpleClass[] { simpleClass1, simpleClass2 });
        }

        [TestMethod]
        public void Distinct_Can_Use_KeyFunc()
        {
            IList<SimpleClass> source = new List<SimpleClass>();
            var simpleClass1 = new SimpleClass()
            {
                IntProperty = 1,
                StringProperty = "1"
            };
            source.Add(simpleClass1);

            var simpleClass2 = new SimpleClass()
            {
                IntProperty = 2,
                StringProperty = "2"
            };
            source.Add(simpleClass2);

            var simpleClass3 = new SimpleClass()
            {
                IntProperty = 1,
                StringProperty = "2"
            };
            source.Add(simpleClass3);

            IEnumerable<SimpleClass> result = source.Distinct(x => x.IntProperty);

            result.Should().HaveCount(2);
            result.Should().ContainInOrder(new SimpleClass[] { simpleClass1, simpleClass2 });
        }

        [TestMethod]
        public void IsDefaultValue_Works_For_Reference_Types_PositiveCase()
        {
            string sut = null;
            sut.IsDefaultValue().Should().BeTrue();
        }

        [TestMethod]
        public void IsDefaultValue_Works_For_Reference_Types_NegativeCase()
        {
            string sut = "";
            sut.IsDefaultValue().Should().BeFalse();
        }

        [TestMethod]
        public void IsDefaultValue_Works_For_Struct_Types_PositiveCase()
        {
            DateTime sut = default(DateTime);
            sut.IsDefaultValue().Should().BeTrue();
        }

        [TestMethod]
        public void IsDefaultValue_Works_For_Struct_Types_NegativeCase()
        {
            DateTime sut = new DateTime(2011, 3, 31);
            sut.IsDefaultValue().Should().BeFalse();
        }

        [TestMethod]
        public void Group_Groups_SimpleClass_Into_SimpleClassGroup()
        {
            var sut = new SimpleClass[]
            {
                new SimpleClass() { IntProperty = 1, StringProperty = "1" },
                new SimpleClass() { IntProperty = 1, StringProperty = "2"},
                new SimpleClass() { IntProperty = 1, StringProperty = "3"},
                new SimpleClass() { IntProperty = 2, StringProperty = "4"},
                new SimpleClass() { IntProperty = 2, StringProperty = "5"},
                new SimpleClass() { IntProperty = 2, StringProperty = "6"},
                new SimpleClass() { IntProperty = 3, StringProperty = "7"},
            };

            var result =
                sut.GroupConsecutive<SimpleClass, SimpleClassGroup>(
                        (x, y) => x.IntProperty == y.GlobalIntProperty,
                        (x) => new SimpleClassGroup(),
                        (x, y) => y.AddSimpleClass(x)
                    );

            result.Should().HaveCount(3);
            result.ElementAt(0).GlobalIntProperty.Should().Be(1);
            result.ElementAt(1).GlobalIntProperty.Should().Be(2);
            result.ElementAt(2).GlobalIntProperty.Should().Be(3);
        }

        [TestMethod]
        public void SumNullable_should_return_null_if_a_value_is_null()
        {
            var sut = new int?[] { 1, null, 10 };
            var result = sut.SumNullable(x => x);
            result.Should().NotHaveValue();
        }

        [TestMethod]
        public void SumNullable_should_return_sum_if_there_are_no_null_values()
        {
            var sut = new int?[] { 1, 10, 13, 0, 4 };
            var expectedResult = sut.Sum();
            var result = sut.SumNullable(x => x);
            result.Should().Be(expectedResult);
        }

        [TestMethod]
        public void Concat_params_should_return_correct_value()
        {
            var sut = new int[] { Any.Int(), Any.Int() };
            var newValue1 = Any.Int();
            var newValue2 = Any.Int();

            var result = sut.Concat(newValue1, newValue2);

            result.Should().BeEquivalentTo(sut.Concat(new int[] { newValue1, newValue2 }));
        }

        [TestMethod]
        public void Concat_IEnumerable_should_return_correct_value()
        {
            var sut = new int[] { Any.Int(), Any.Int() };
            var newValue1 = Any.Int();
            var newValue2 = Any.Int();

            var newValue3 = Any.Int();
            var newValue4 = Any.Int();

            var result = sut.Concat(new[] { newValue1, newValue2 }, new[] { newValue3, newValue4});

            result.Should().BeEquivalentTo(sut.Concat(new int[] { newValue1, newValue2, newValue3, newValue4 }));
        }
    }
}
