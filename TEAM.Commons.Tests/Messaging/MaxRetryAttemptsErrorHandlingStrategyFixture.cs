﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using Moq;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.Tests.Messaging
{
    [TestClass]
    public class MaxRetryAttemptsErrorHandlingStrategyFixture
    {

        [TestMethod]
        public void CanBeRetried_should_be_false_when_its_been_retried_maxRetryAttempts()
        {
            var processingContextMock = new Mock<IMessageProcessingContext>();
            processingContextMock.Setup(x => x.RetryCount).Returns(1);

            var sut = new MaxRetryAttemptsErrorHandlingStrategy(1, TimeSpan.Zero);

            sut.ProcessFailure(new TestMessage(), new Exception(), processingContextMock.Object);
            sut.CanBeRetried.Should().Be(false);
        }

    }
}
