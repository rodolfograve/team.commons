﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System;
using System.Linq;
using TEAM.Commons.Messaging.InProcess;
using TEAM.Commons.Tests.Messaging.Infrastructure;

namespace TEAM.Commons.Tests.Messaging
{
    [TestClass]
    public class InProcessTransportWithSerializerFixture
    {
        [TestMethod]
        public void Should_send_and_receive_a_message()
        {
            var sut = new InProcessTransportWithSerializer(new JsonMessageSerializer());
            var sentMessage = new TestMessage() { PropInt = 1, PropNullableInt = 5, PropDateTime = DateTime.Now, PropNullableDateTime = null, PropString = "test string" };
            sut.Send(sentMessage);
            var result = sut.TryReceiveCurrentAvailableMessage();
            result.Should().NotBeNull();
            result.Messages.Should().NotBeNull();
            result.Messages.Should().NotBeEmpty();
            result.Messages.Length.Should().Be(1);
            result.Messages[0].Should().BeOfType<TestMessage>();

            var receivedMessaged = sentMessage;
            receivedMessaged.Should().NotBeNull();
            receivedMessaged.PropInt.Should().Be(sentMessage.PropInt);
            receivedMessaged.PropNullableInt.Should().Be(sentMessage.PropNullableInt);
            receivedMessaged.PropDateTime.Should().Be(sentMessage.PropDateTime);
            receivedMessaged.PropNullableDateTime.Should().Be(sentMessage.PropNullableDateTime);
            receivedMessaged.PropString.Should().Be(sentMessage.PropString);
        }
    }
}
