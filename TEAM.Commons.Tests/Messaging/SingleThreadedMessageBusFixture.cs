﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using TEAM.Commons.Messaging;
using TEAM.Commons.Messaging.InProcess;
using Autofac;
using System.Threading;
using System.Transactions;
using Moq;
using System.Diagnostics.Contracts;
using System.Diagnostics;

namespace TEAM.Commons.Tests.Messaging
{
    [TestClass]
    public class SingleThreadedMessageBusFixture
    {

        [TestMethod]
        public void Should_call_registered_handler_for_message_when_handles_only_one_message()
        {
            var count = 0;
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMessageHandler>(x => new TestMessageHandler(m => count++));

            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            count.Should().Be(1);
        }

        [TestMethod]
        public void Should_call_registered_handler_for_message_when_handles_more_than_one_message()
        {
            var count = 0;
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMultiMessageHandler>(x => new TestMultiMessageHandler(m => count++, m => count++));

            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());
            transport.Send(new AnotherTestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.Start();
            Thread.Sleep(500);
            sut.Stop();
            count.Should().Be(2);
        }

        [TestMethod]
        public void Should_process_incoming_messages_when_started()
        {
            var wait = new EventWaitHandle(false, EventResetMode.ManualReset);
            var count = 0;

            Action processMessageAction = () =>
                {
                    count++;
                    if (count >= 5)
                    {
                        wait.Set();
                    }
                };

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMultiMessageHandler>(x => new TestMultiMessageHandler(m => processMessageAction(), m => processMessageAction()));

            var transport = BuildInProcessTransport();

            var sut = BuildMessageBus(transport, builder);
            sut.Start();

            var sendMessagesThread = new ThreadStart(() =>
            {
                try
                {
                    transport.Send(new TestMessage());
                    transport.Send(new AnotherTestMessage());
                    transport.Send(new TestMessage());
                    transport.Send(new AnotherTestMessage());
                    transport.Send(new TestMessage());
                }
                catch
                {
                    wait.Set();
                }
            });

            new Thread(sendMessagesThread).Start();
            wait.WaitOne();
            sut.Stop();
            count.Should().Be(5);
        }

        [TestMethod]
        public void Should_use_the_same_ErrorHandlingStrategy_instance_for_each_retry()
        {
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<EmptyTestMessageHandler>(x => new EmptyTestMessageHandler((m, c) => { throw new Exception(); }));
            var transport = BuildInProcessTransport();

            var factoryCallCount = 0;

            Func<IErrorHandlingStrategy> factory = () =>
               {
                   factoryCallCount++;
                   return new MaxRetryAttemptsErrorHandlingStrategy(3, TimeSpan.Zero);
               };

            var sut = BuildMessageBus(transport, builder, factory);

            var wait = new ManualResetEventSlim(false);
            var failedCount = 0;
            sut.Stopped += x => wait.Set();
            sut.MessageProcessingFailed += x =>
               {
                   failedCount++;
                   if (failedCount == 4)
                       wait.Set();
               };

            sut.Start();

            transport.Send(new TestMessage());

            wait.Wait();

            factoryCallCount.Should().Be(1, "The ErrorHandlingStrategy factory should be called once to create an instance that will be re-used for the retries");

            sut.Stop();
        }

        [TestMethod]
        public void Should_not_retry_if_the_ErrorHandlingStrategy_says_so_but_the_bus_has_been_asked_to_Stop()
        {
            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler(x => new EmptyTestMessageHandler((m, c) => { throw new Exception(); }));
            var transport = BuildInProcessTransport();

            var retryCount = 0;
            var errorHandlingStrategyMock = new Mock<IErrorHandlingStrategy>();
            errorHandlingStrategyMock.Setup(x => x.CanBeRetried).Returns(() => retryCount <= 1).Verifiable();

            errorHandlingStrategyMock.Setup(x => x.ProcessFailure(It.IsAny<object>(), It.IsAny<Exception>(), It.IsAny<IMessageProcessingContext>()))
                                     .Callback(() => retryCount++).Verifiable();

            var wait = new ManualResetEventSlim();
            var requestStopWait = new ManualResetEventSlim();
            var sut = BuildMessageBus(transport, builder, () => errorHandlingStrategyMock.Object);
            sut.Stopped += x => wait.Set();
            sut.MessageProcessing += _ => requestStopWait.Set();

            sut.Start();
            transport.Send(new EmptyTestMessage());

            requestStopWait.WaitDebuggerFriendly().Should().BeTrue();
            sut.Stop();
            wait.WaitDebuggerFriendly().Should().BeTrue();

            errorHandlingStrategyMock.Verify();
            retryCount.Should().Be(1);
        }

        [TestMethod]
        public void Should_increase_the_MessageProcessingContext_RetryCount_when_a_message_is_retried()
        {
            var builder = new ContainerBuilder();
            var wait = new ManualResetEventSlim();
            var requestStopWait = new ManualResetEventSlim();

            bool processingContextRetryCountIncreased = false;
            builder.RegisterMessageHandler(x => new EmptyTestMessageHandler((m, c) =>
            {
                if (!processingContextRetryCountIncreased && c.RetryCount == 0)
                {
                    throw new Exception("Expected exception to trigger a retry");
                }
                else
                {
                   requestStopWait.Set();
                   processingContextRetryCountIncreased = true;
                }
            }));
            var transportMock = new Mock<IReceivingTransport>();
            transportMock.Setup(x => x.BlockUntilMessageIsAvailable(It.IsAny<TimeSpan>())).Returns(true);
            transportMock.Setup(x => x.TryReceiveCurrentAvailableMessage()).Returns(new TransportMessage { Messages = new[] { new EmptyTestMessage() } });

            var errorHandlingStrategyMock = new Mock<IErrorHandlingStrategy>();
            errorHandlingStrategyMock.Setup(x => x.CanBeRetried).Returns(() => !processingContextRetryCountIncreased);


            var sut = BuildMessageBus(transportMock.Object, builder, () => errorHandlingStrategyMock.Object);
            sut.Stopped += x => wait.Set();

            sut.Start();

            requestStopWait.WaitDebuggerFriendly().Should().BeTrue();
            sut.Stop();

            wait.WaitDebuggerFriendly().Should().BeTrue();

            errorHandlingStrategyMock.Verify(x => x.ProcessFailure(It.IsAny<object>(), It.IsAny<Exception>(), It.IsAny<IMessageProcessingContext>()), Times.Once());
            processingContextRetryCountIncreased.Should().BeTrue();
        }

        [TestMethod]
        public void Should_call_events_when_message_is_processed_OK()
        {
            var messageProcessingCalled = false;
            var messageProcessedCalled = false;
            var messageProcessingFailedCalled = false;

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMultiMessageHandler>(x => new TestMultiMessageHandler(m => { }, m => { }));
            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.MessageProcessing += e => messageProcessingCalled = true;
            sut.MessageProcessed += e => messageProcessedCalled = true;
            sut.MessageProcessingFailed += e => messageProcessedCalled = true;

            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            messageProcessingCalled.Should().BeTrue();
            messageProcessedCalled.Should().BeTrue();
            messageProcessingFailedCalled.Should().BeFalse();
        }

        [TestMethod]
        public void Should_call_events_when_message_processing_fails()
        {
            var messageProcessingCalled = false;
            var messageProcessedCalled = false;
            var messageProcessingFailedCalled = false;

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMultiMessageHandler>(x => new TestMultiMessageHandler(m => { throw new Exception(); }, m => { }));
            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.MessageProcessing += e => messageProcessingCalled = true;
            sut.MessageProcessed += e => messageProcessedCalled = true;
            sut.MessageProcessingFailed += e => messageProcessingFailedCalled = true;

            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            messageProcessingCalled.Should().BeTrue();
            messageProcessedCalled.Should().BeFalse();
            messageProcessingFailedCalled.Should().BeTrue();
        }

        [TestMethod]
        public void Should_call_events_when_the_ambient_transaction_is_aborted_while_processing_a_message()
        {
            var messageProcessingCalled = false;
            var messageProcessedCalled = false;
            var messageProcessingFailedCalled = false;

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMultiMessageHandler>(x => new TestMultiMessageHandler(m =>
                {
                    using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
                    {
                        throw new Exception();
                    }
                }, m => { }));

            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.MessageProcessing += e => messageProcessingCalled = true;
            sut.MessageProcessed += e => messageProcessedCalled = true;
            sut.MessageProcessingFailed += e => messageProcessingFailedCalled = true;

            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            messageProcessingCalled.Should().BeTrue();
            messageProcessedCalled.Should().BeFalse();
            messageProcessingFailedCalled.Should().BeTrue();
        }

        [TestMethod]
        public void Should_rollback_transaction_and_continue_normally_if_AbortMessage_is_requested_in_the_message_handler()
        {
            var messageProcessingCalled = false;
            var messageProcessedCalled = false;
            var messageProcessingFailedCalled = false;

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMessageHandler>(x => new TestMessageHandler((m, context) =>
            {
                context.AbortCurrentMessage();
            }));

            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.MessageProcessing += e => messageProcessingCalled = true;
            sut.MessageProcessed += e => messageProcessedCalled = true;
            sut.MessageProcessingFailed += e => messageProcessingFailedCalled = true;

            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            messageProcessingCalled.Should().BeTrue();
            messageProcessedCalled.Should().BeFalse();
            messageProcessingFailedCalled.Should().BeFalse();
        }

        [TestMethod]
        public void Should_stop_if_there_is_an_exception_while_processing_the_MessageProcessingFailed_event()
        {
            var messageProcessingCalled = false;
            var messageProcessedCalled = false;
            var messageProcessingFailedCalled = false;
            Exception exception = null;

            var builder = new ContainerBuilder();
            builder.RegisterMessageHandler<TestMultiMessageHandler>(x => new TestMultiMessageHandler(m => { throw new Exception(); }, m => { }));
            var transport = BuildInProcessTransport();

            transport.Send(new TestMessage());

            var sut = BuildMessageBus(transport, builder);
            sut.MessageProcessing += e => messageProcessingCalled = true;
            sut.MessageProcessed += e => messageProcessedCalled = true;
            sut.MessageProcessingFailed += e => { messageProcessingFailedCalled = true; throw new Exception(); };
            sut.Stopped += e => exception = e.Exception;

            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            messageProcessingCalled.Should().BeTrue();
            messageProcessedCalled.Should().BeFalse();
            messageProcessingFailedCalled.Should().BeTrue();
            exception.Should().NotBeNull("a CriticalMessageBusException must be thrown");
        }

        [TestMethod]
        public void Should_call_events_when_message_is_sent_OK()
        {
            var messageSendingCalled = false;
            var messageSentCalled = false;

            var sendingTransport = BuildInProcessTransport();
            var messageMap = new DictionaryMessageMap();
            messageMap.AddEndpoint("TestEndpoint", new MessageRoutingInfo() { SendingTransport = sendingTransport });
            messageMap.AddMessageMap<TestMessage>("TestEndpoint");

            var sut = BuildMessageBusWithMessageMap(messageMap);

            sut.MessageSending += e => messageSendingCalled = true;
            sut.MessageSent += e => messageSentCalled = true;

            sut.Publish(new TestMessage());

            var result = sendingTransport.WaitForMessage(TimeSpan.FromMilliseconds(100));

            messageSendingCalled.Should().BeTrue();
            messageSentCalled.Should().BeTrue();

            result.Should().NotBeNull();
            result.Messages.Should().HaveCount(1);
            result.Messages[0].Should().BeOfType<TestMessage>();
        }

        [TestMethod]
        public void Should_fail_if_there_is_an_exception_when_sending_a_message()
        {
            var messageSendingCalled = false;
            var messageSentCalled = false;

            var sendingTransport = BuildInProcessTransport();
            var messageMap = new DictionaryMessageMap();
            messageMap.AddEndpoint("TestEndpoint", new MessageRoutingInfo { SendingTransport = sendingTransport });
            messageMap.AddMessageMap<TestMessage>("TestEndpoint");

            var sut = BuildMessageBusWithMessageMap(messageMap);

            sut.MessageSending += e => { messageSendingCalled = true; throw new Exception(); };
            sut.MessageSent += e => messageSentCalled = true;

            Action a = () => sut.Publish(new TestMessage());
            a.ShouldThrow<Exception>();

            messageSendingCalled.Should().BeTrue();
            messageSentCalled.Should().BeFalse();

            var result = sendingTransport.WaitForMessage(TimeSpan.FromMilliseconds(100));
            result.Should().BeNull();
        }

        [TestMethod]
        [Ignore]
        public void Should_raise_stopped_event_without_error_when_stopped_with_a_call_to_Stop()
        {
            Exception exception = null;
            var completedWaitHandle = new ManualResetEvent(false);
            var transport = BuildInProcessTransport();
            var builder = new ContainerBuilder();

            var sut = BuildMessageBus(transport, builder);
            sut.Stopped += x =>
                {
                    exception = x.Exception;
                    completedWaitHandle.Set();
                };

            sut.Start();
            sut.Stop();

            completedWaitHandle.WaitOneDebuggerFriendly(100).Should().BeTrue("the Stopped event should have been invoked");
            exception.Should().BeNull("no error should be raised since the MessageBus was correctly stopped through a call to .Stop()");
        }

        [TestMethod]
        public void Should_raise_stopped_event_with_exception_when_a_critical_failure_occurs()
        {
            Exception exception = null;
            var waitHandle = new ManualResetEvent(false);
            var transport = BuildInProcessTransport();

            var builder = new ContainerBuilder(); // No MessageHandlers registered

            var sut = BuildMessageBus(transport, builder);
            sut.Stopped += x =>
            {
                exception = x.Exception;
                waitHandle.Set();
            };

            sut.Start();
            transport.Send(new TestMessage());

            waitHandle.WaitOneDebuggerFriendly(1000).Should().BeTrue("the Stopped event should have been invoked");
            exception.Should().NotBeNull("a MessageBusConfigurationErrorsException should be thrown because no MessageHandler was registered for the message");
            exception.Should().BeOfType<MessageBusConfigurationErrorsException>();
        }

        [TestMethod]
        public void Should_stop_on_request_even_when_no_message_arrives()
        {
            Exception exception = null;
            var waitHandle = new ManualResetEvent(false);
            var transport = BuildInProcessTransport();

            var builder = new ContainerBuilder(); // No MessageHandlers registered

            var sut = BuildMessageBus(transport, builder);
            sut.Stopped += x =>
            {
                exception = x.Exception;
                waitHandle.Set();
            };

            sut.Start();
            Thread.Sleep(500);
            sut.Stop();

            waitHandle.WaitOne(2500).Should().BeTrue("the Stopped event should have been invoked");
            exception.Should().BeNull("the service should stop cleanly without exceptions");
        }

        [TestMethod]
        public void Should_not_process_any_messages_that_arrive_after_stop_was_called()
        {
            Exception exception = null;
            var waitHandle = new ManualResetEvent(false);
            var transport = BuildInProcessTransport();

            var builder = new ContainerBuilder(); // No MessageHandlers registered

            var sut = BuildMessageBus(transport, builder);
            sut.Stopped += x =>
            {
                exception = x.Exception;
                waitHandle.Set();
            };

            sut.Start();
            Thread.Sleep(10);
            sut.Stop();

            transport.Send(new TestMessage());

            waitHandle.WaitOne(250000).Should().BeTrue("the Stopped event should have been invoked");
            exception.Should().BeNull("the service should stop cleanly without exceptions");
        }

        [TestMethod]
        public void Should_send_message_to_named_endpoint()
        {
            var transport = BuildInProcessTransport();

            var messageMap = new DictionaryMessageMap();
            messageMap.AddEndpoint("TestEndpoint", new MessageRoutingInfo() { SendingTransport = transport });

            var sut = BuildMessageBusWithMessageMap(messageMap);

            sut.Send(new TestMessage(), "TestEndpoint");

            transport.PendingMessagesCount.Should().Be(1, "the message should have been sent to this transport");
        }

        [TestMethod]
        public void Should_fail_to_send_message_to_an_unregistered_endpointId()
        {
            var transport = BuildInProcessTransport();

            var messageMap = new DictionaryMessageMap();
            messageMap.AddEndpoint("TestEndpoint", new MessageRoutingInfo() { SendingTransport = transport });

            var sut = BuildMessageBusWithMessageMap(messageMap);

            Action a = () => sut.Send(new TestMessage(), "TestEndpoint WRONG");
            a.ShouldThrow<ApplicationException>();
        }

        protected SingleThreadMessageBus BuildMessageBus(IReceivingTransport transport, ContainerBuilder builder, Func<IErrorHandlingStrategy> errorHandlingStrategyFactory = null)
        {
            Contract.Requires(transport != null, "transport is null.");
            Contract.Requires(builder != null, "builder is null.");

            var messageHandlersContainer = new AutofacMessageHandlersContainerAdapter(builder.Build());
            var map = new DictionaryMessageMap();
            var transactionOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };

            var result = new SingleThreadMessageBus("Test", transport, messageHandlersContainer, map, transactionOptions, errorHandlingStrategyFactory);
            return result;
        }

        protected SingleThreadMessageBus BuildMessageBusWithMessageMap(IMessageMap messageMap)
        {
            var transport = BuildInProcessTransport();
            var messageHandlersContainer = new AutofacMessageHandlersContainerAdapter(new ContainerBuilder().Build());
            var result = new SingleThreadMessageBus("Test", transport, messageHandlersContainer, messageMap, new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            });
            return result;
        }

        protected InProcessTransport BuildInProcessTransport()
        {
            var result = new InProcessTransport();
            return result;
        }

    }
}
