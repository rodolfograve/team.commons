﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.Tests.Messaging
{
    public class TestMessageHandler : IMessageHandler<TestMessage>
    {

        public TestMessageHandler(Action<TestMessage, IMessageProcessingContext> delegatedAction)
        {
            DelegatedAction = delegatedAction;
        }

        public TestMessageHandler(Action<TestMessage> delegatedAction)
        {
            DelegatedAction = (m, _) => delegatedAction(m);
        }

        protected readonly Action<TestMessage, IMessageProcessingContext> DelegatedAction;

        public void Handle(TestMessage message, IMessageProcessingContext processingContext)
        {
            DelegatedAction(message, processingContext);
        }
    }
}
