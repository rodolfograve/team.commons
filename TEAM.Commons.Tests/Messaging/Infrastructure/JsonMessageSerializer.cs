﻿using System.IO;
using System.Text;
using Newtonsoft.Json;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.Tests.Messaging.Infrastructure
{
    public class JsonMessageSerializer : IMessageSerializer
    {
        private static readonly JsonSerializer Serializer;
        static JsonMessageSerializer()
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            };
            Serializer = JsonSerializer.Create(settings);
        }
        public TransportMessage Deserialize(byte[] message)
        {
            using (var jsonReader = new JsonTextReader(new StringReader(Encoding.UTF8.GetString(message))))
            {
                return Serializer.Deserialize<TransportMessage>(jsonReader);
            }
        }

        public byte[] Serialize(TransportMessage message)
        {
            using (var result = new MemoryStream())
            {
                using (var jsonWriter = new JsonTextWriter(new StreamWriter(result)))
                {
                    Serializer.Serialize(jsonWriter, message);
                }
                return result.GetBuffer();
            }
        }
    }
}
