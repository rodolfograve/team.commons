﻿using System;
using System.Linq;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.Tests.Messaging
{
    public class TestMultiMessageHandler : TestMessageHandler, IMessageHandler<AnotherTestMessage>
    {

        public TestMultiMessageHandler(Action<TestMessage> delegatedActionForTestMessage, Action<AnotherTestMessage> delegatedActionForAnotherTestMessage)
            : base ((message, _) => delegatedActionForTestMessage(message))
        {
            DelegatedActionForAnotherTestMessage = delegatedActionForAnotherTestMessage;
        }

        protected readonly Action<AnotherTestMessage> DelegatedActionForAnotherTestMessage;

        public void Handle(AnotherTestMessage message, IMessageProcessingContext processingContext)
        {
            DelegatedActionForAnotherTestMessage(message);
        }
    }
}
