﻿using System;
using System.Collections.Generic;
using System.Linq;
using TEAM.Commons.Messaging;

namespace TEAM.Commons.Tests.Messaging
{
   public class EmptyTestMessageHandler : IMessageHandler<EmptyTestMessage>
   {

      public EmptyTestMessageHandler(Action<EmptyTestMessage, IMessageProcessingContext> action)
      {
         Action = action;
      }

      protected readonly Action<EmptyTestMessage, IMessageProcessingContext> Action;

      public void Handle(EmptyTestMessage message, IMessageProcessingContext processingContext)
      {
         Action(message, processingContext);
      }
   }
}
