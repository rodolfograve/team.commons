﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Tests.Messaging
{
   public class ComplexTestMessage
   {
      public Guid BtsId { get; set; }
      public long TradeId { get; set; }
      public Guid WorkflowInstanceId { get; set; }
      public int WorkflowStageOrder { get; set; }
   }
}
