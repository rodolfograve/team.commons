﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace TEAM.Commons.Tests
{
    public class SimpleClassGroup
    {

        public SimpleClassGroup()
        {
            SimpleClasses_Internal = new List<SimpleClass>();
        }

        public int GlobalIntProperty { get; protected set; }

        protected readonly IList<SimpleClass> SimpleClasses_Internal;

        public IEnumerable<SimpleClass> SimpleClasses
        {
            get
            {
                return SimpleClasses_Internal;
            }
        }

        public void AddSimpleClass(SimpleClass newItem)
        {
            Contract.Requires(newItem != null, "newItem is null.");
            if (SimpleClasses_Internal.Any())
            {
                if (newItem.IntProperty != GlobalIntProperty)
                {
                    throw new Exception("Can not add a SimpleClass with different IntProperty.");
                }
            }
            else
            {
                GlobalIntProperty = newItem.IntProperty;
            }
            SimpleClasses_Internal.Add(newItem);
        }

    }
}
