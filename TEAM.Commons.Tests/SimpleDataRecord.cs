﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TEAM.Commons.Tests
{
    public class SimpleDataRecord : IDataRecord
    {

        public SimpleDataRecord()
        {
            Values = new Dictionary<string, object>();
        }

        protected readonly IDictionary<string, object> Values;

        public void Add(string name, object value)
        {
            Values.Add(name, value);
        }

        public int FieldCount
        {
            get { return Values.Count; }
        }

        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public int GetInt32(int i)
        {
            throw new NotImplementedException();
        }

        public long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public string GetName(int i)
        {
            return Values.Keys.ElementAt(i);
        }

        public int GetOrdinal(string name)
        {
            int index = 0;
            foreach (var key in Values.Keys)
            {
                if (key == name)
                {
                    return index;
                }
                index++;
            }
            throw new IndexOutOfRangeException();
        }

        public string GetString(int i)
        {
            throw new NotImplementedException();
        }

        public object GetValue(int i)
        {
            int index = 0;
            foreach (var key in Values.Keys)
            {
                if (index == i)
                {
                    return Values[key] ?? DBNull.Value;
                }
                index++;
            }
            throw new IndexOutOfRangeException();
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public bool IsDBNull(int i)
        {
            return GetValue(i) == DBNull.Value;
        }

        public object this[string name]
        {
            get { throw new NotImplementedException(); }
        }

        public object this[int i]
        {
            get 
            {
                return GetValue(i);
            }
        }
    }
}
