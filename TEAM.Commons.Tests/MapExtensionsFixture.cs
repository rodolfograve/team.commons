﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class MapExtensionsFixture
    {

        protected SimpleDataRecord CreateDataRecord(string strValue, int intValue, DateTime? dateValue, Guid guidValue)
        {
            SimpleDataRecord result = new SimpleDataRecord();

            result.Add("StringProperty", strValue);
            result.Add("IntProperty", intValue);
            result.Add("DateProperty", dateValue);
            result.Add("GuidProperty", guidValue);
            return result;
        }

        [TestMethod]
        public void SimpleDataRecord_Can_Keep_And_Retrieve_Properties()
        {
            DateTime t = new DateTime(2009, 12, 18);
            string expectedStrValue = "Test";
            int expectedIntValue = 18;
            DateTime expectedDateValue = new DateTime(2009, 5, 23);
            Guid expectedGuidValue = Guid.NewGuid();

            SimpleDataRecord record = CreateDataRecord(expectedStrValue, expectedIntValue, expectedDateValue, expectedGuidValue);

            int i = record.GetOrdinal("StringProperty");
            object value = record.GetValue(i);
            value.Should().Be(expectedStrValue);

            i = record.GetOrdinal("IntProperty");
            value = record.GetValue(i);
            value.Should().Be(expectedIntValue);

            i = record.GetOrdinal("DateProperty");
            value = record.GetValue(i);
            value.Should().Be(expectedDateValue);

            i = record.GetOrdinal("GuidProperty");
            value = record.GetValue(i);
            value.Should().Be(expectedGuidValue);
        }

        [TestMethod]
        public void Convert_Can_Map_Enums()
        {
            SimpleEnum e = ConvertExtensions.ChangeType<SimpleEnum>(SimpleEnum.Second.ToString());
            e.Should().Be(SimpleEnum.Second);
        }

        [TestMethod]
        public void Convert_Can_Map_NullableEnums_WithValue()
        {
            SimpleEnum? e = ConvertExtensions.ChangeType<SimpleEnum?>(SimpleEnum.Second.ToString());
            e.Should().Be(SimpleEnum.Second);
        }

        [TestMethod]
        public void Convert_Can_Map_NullableEnums_Nulled()
        {
            SimpleEnum? e = ConvertExtensions.ChangeType<SimpleEnum?>(null);
            e.Should().BeNull();
        }

        [TestMethod]
        public void MapFromDataRecord_Can_Map_Public_Properties()
        {
            string expectedStrValue = "Test";
            int expectedIntValue = 18;
            DateTime? expectedDateValue = null; //new DateTime(2009, 5, 23);
            Guid expectedGuidValue = Guid.NewGuid();

            SimpleDataRecord record = CreateDataRecord(expectedStrValue, expectedIntValue, expectedDateValue, expectedGuidValue);
            SimpleClass simpleClass = new SimpleClass();
            simpleClass.MapFromDataRecord(record);

            simpleClass.StringProperty.Should().Be(expectedStrValue);
            simpleClass.IntProperty.Should().Be(expectedIntValue);
            simpleClass.DateProperty.Should().Be(expectedDateValue);
            simpleClass.GuidProperty.Should().Be(expectedGuidValue);
        }

        public class SimpleProtectedClass
        {
            public string PublicProperty { get; set; }

            public string ProtectedProperty { get; protected set; }

            public string PrivateProperty { get; private set; }

        }

        [TestMethod]
        public void MapFromDataRecord_Maps_Properties_With_Protected_And_Private_Sets()
        {
            string expectedPublicValue = "Public";
            string expectedProtectedValue = "Protected";
            string expectedPrivateValue = "Private";

            var record = new SimpleDataRecord();
            record.Add("PublicProperty", expectedPublicValue);
            record.Add("ProtectedProperty", expectedProtectedValue);
            record.Add("PrivateProperty", expectedPrivateValue);

            var simpleProtectedClass = new SimpleProtectedClass();
            simpleProtectedClass.MapFromDataRecord(record);

            simpleProtectedClass.PublicProperty.Should().Be(expectedPublicValue);
            simpleProtectedClass.ProtectedProperty.Should().Be(expectedProtectedValue);
            simpleProtectedClass.PrivateProperty.Should().Be(expectedPrivateValue);
        }

    }
}
