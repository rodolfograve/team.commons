﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
   [TestFixture]
   public class LogicalOperatorsFixture
   {
      /// <summary>
      /// See http://en.wikipedia.org/wiki/Truth_table#Logical_implication
      /// </summary>
      /// <param name="leftSide"></param>
      /// <param name="rightSide"></param>
      /// <param name="expectedResult"></param>
      [TestCase(true, true, true)]
      [TestCase(true, false, false)]
      [TestCase(false, true, true)]
      [TestCase(false, false, true)]
      public void Implication_truth_table(bool leftSide, bool rightSide, bool expectedResult)
      {
         leftSide.Implies(rightSide).Should().Be(expectedResult);
      }
   }
}
