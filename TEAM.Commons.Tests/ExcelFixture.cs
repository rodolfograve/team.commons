﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons.Excel;
using System.IO;

namespace TEAM.Commons.Tests
{
    /// <summary>
    /// Descripción resumida de Excel
    /// </summary>
    [TestClass]
    public class ExcelFixture
    {

        [TestMethod]
        public void Can_Generate_Excel_As_PropietaryXMLFormat()
        {
            ExcelWorkbook sut = ExcelWorkbook.WithWorksheets
            (
                ExcelWorksheet.WithRows("Testing",
                    ExcelRow.WithCells(
                        ExcelCell.String("This is first row, first column"),
                        ExcelCell.String("This is first row, second column")
                    ),
                    null,
                    null,
                    ExcelRow.WithCells(
                        ExcelCell.String("This is second row, first column"),
                        ExcelCell.Empty(),
                        null,
                        ExcelCell.String("This is second row, fourth column")
                    )
                )
            );
            string outputFilePath = Path.GetFullPath(".\\Test.xml");
            if (System.IO.File.Exists(outputFilePath))
            {
                System.IO.File.Delete(outputFilePath);
            }
            sut.GenerateAsMsPropietaryXml(File.OpenWrite(outputFilePath), null);
        }

        [TestMethod]
        public void Can_Generate_Excel_As_OpenXML()
        {
            //ExcelWorkbook sut = ExcelWorkbook.WithWorksheets
            //(
            //    ExcelWorksheet.WithRows("Testing",
            //        ExcelRow.WithCells(
            //            ExcelCell.String("This is first row, first column"),
            //            ExcelCell.String("This is first row, second column")
            //        ),
            //        null,
            //        null,
            //        ExcelRow.WithCells(
            //            ExcelCell.String("This is second row, first column"),
            //            ExcelCell.Empty(),
            //            null,
            //            ExcelCell.String("This is second row, fourth column")
            //        )
            //    )
            //);
            //string outputFilePath = Path.GetFullPath(".\\Test.xlsx");
            //using (var outputStream = File.Open(outputFilePath, FileMode.Create, FileAccess.ReadWrite))
            //{
            //    sut.GenerateAsOpenXML(outputStream, null);
            //}
        }

    }
}
