﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class DictionaryExtensionsFixture
    {

        [TestMethod]
        public void Should_add_nonExisting_value()
        {
            var sut = new Dictionary<string, string>();
            var key = Any.String();
            var value = Any.String();
            var result = sut.GetOrAdd(key, _ => value);

            result.Should().Be(value);
            sut[key].Should().Be(value);
        }

        [TestMethod]
        public void Should_get_existing_value()
        {
            var sut = new Dictionary<string, string>();
            var key = Any.String();
            var value = Any.String();
            sut.Add(key, value);

            var result = sut.GetOrAdd(key, _ => value.GenerateDifferent());

            result.Should().Be(value);
            sut[key].Should().Be(value);
        }

        [TestMethod]
        public void Should_return_null_when_key_does_not_exist_for_nullable_value()
        {
            var sut = new Dictionary<string, string>();
            var result = sut.GetOrDefault(Any.String());

            result.Should().BeNull();
        }

        [TestMethod]
        public void Should_return_existing_value()
        {
            var sut = new Dictionary<string, string>();
            var key = Any.String();
            var value = Any.String();
            sut.Add(key, value);

            var result = sut.GetOrDefault(key);

            result.Should().Be(value);
        }

        [TestMethod]
        public void Should_return_default_value_when_key_does_not_exist()
        {
            var sut = new Dictionary<int, int>();
            var result = sut.GetOrDefault(Any.Int());

            result.Should().Be(default(int));
        }

        [TestMethod]
        public void Should_combine_hash_codes_for_ints()
        {
            var h1 = 1.GetHashCode();
            var h2 = 2.GetHashCode();

            var result = 1.CombineHashCodeWith(2);

            result.Should().Be((((h1 << 5) + h1) ^ h2));
        }

        [TestMethod]
        public void Should_combine_hash_codes_for_different_classes()
        {
            var h1 = "test1".GetHashCode();
            var h2 = 2.GetHashCode();

            var result = "test1".CombineHashCodeWith(2);

            result.Should().Be((((h1 << 5) + h1) ^ h2));
        }

        [TestMethod]
        public void Should_combine_hash_codes_for_structs()
        {
            var s = new TestStruct();
            var h1 = s.GetHashCode();
            var h2 = 2.GetHashCode();

            var result = s.CombineHashCodeWith(2);

            result.Should().Be((((h1 << 5) + h1) ^ h2));
        }

        private struct TestStruct { }

    }
}
