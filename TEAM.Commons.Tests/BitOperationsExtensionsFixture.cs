﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class BitOperationsExtensionsFixture
    {
        [TestMethod]
        public void Should_calculate_right_value_for_int()
        {
            0.GetHammingWeight().Should().Be(0);
            1.GetHammingWeight().Should().Be(1);
            2.GetHammingWeight().Should().Be(1);
            3.GetHammingWeight().Should().Be(2);
            32.GetHammingWeight().Should().Be(1);
            129.GetHammingWeight().Should().Be(2);
            130.GetHammingWeight().Should().Be(2);
            131.GetHammingWeight().Should().Be(3);

            int.MaxValue.GetHammingWeight().Should().Be(31);

            (-1).GetHammingWeight().Should().Be(32);
            int.MinValue.GetHammingWeight().Should().Be(1);
        }

        [TestMethod]
        public void Should_calculate_right_value_for_uint()
        {
            ((uint)0).GetHammingWeight().Should().Be(0);
            ((uint)1).GetHammingWeight().Should().Be(1);
            ((uint)2).GetHammingWeight().Should().Be(1);
            ((uint)3).GetHammingWeight().Should().Be(2);
            ((uint)32).GetHammingWeight().Should().Be(1);
            ((uint)129).GetHammingWeight().Should().Be(2);
            ((uint)130).GetHammingWeight().Should().Be(2);
            ((uint)131).GetHammingWeight().Should().Be(3);

            uint.MaxValue.GetHammingWeight().Should().Be(32);
        }

        [TestMethod]
        [Ignore()] // The long implementation is not working
        public void Should_calculate_right_value_for_long()
        {
            //((long)0).GetHammingWeight().Should().Be(0);
            //((long)1).GetHammingWeight().Should().Be(1);
            //((long)2).GetHammingWeight().Should().Be(1);
            //((long)3).GetHammingWeight().Should().Be(2);
            //((long)32).GetHammingWeight().Should().Be(1);
            //((long)129).GetHammingWeight().Should().Be(2);
            //((long)130).GetHammingWeight().Should().Be(2);
            //((long)131).GetHammingWeight().Should().Be(3);

            //(((long)int.MaxValue) + 1).GetHammingWeight().Should().Be(33);

            //long.MaxValue.GetHammingWeight().Should().Be(63);
        }
    }
}
