﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using FluentAssertions;

namespace TEAM.Commons.Tests
{

    [TestClass]
    public class LinqToXMLExtensionsFixture
    {
        private const string Xml =
@"<?xml version=""1.0""?>
<root>
    <intTag attr=""10"">5</intTag>
    <stringTag attr=""11"">6</stringTag>
    <decimalTag attr=""15.35"">5.35</decimalTag>
    <invariantDateTag attr=""04/20/2011"">03/20/2011</invariantDateTag>
    <spanishDateTag attr=""21/03/2011"">20/03/2011</spanishDateTag>
</root>
";

        [TestMethod]
        public void Can_Convert_Values()
        {
            var sut = XElement.Load(new StringReader(Xml));

            var intTagValue = sut.Element("intTag").GetValue<int>();
            var intAttrValue = sut.Element("intTag").Attribute("attr").GetValue<int>();

            var stringTagValue = sut.Element("stringTag").GetValue<string>();
            var stringAttrValue = sut.Element("stringTag").Attribute("attr").GetValue<string>();

            var decimalTagValue = sut.Element("decimalTag").GetValue<decimal>();
            var decimalAttrValue = sut.Element("decimalTag").Attribute("attr").GetValue<decimal>();

            var invariantDateTagValue = sut.Element("invariantDateTag").GetValue<DateTime>(CultureInfo.InvariantCulture);
            var invariantDateAttrValue = sut.Element("invariantDateTag").Attribute("attr").GetValue<DateTime>(CultureInfo.InvariantCulture);

            var spanishDateTagValue = sut.Element("spanishDateTag").GetValue<DateTime>(CultureInfo.CreateSpecificCulture("es-ES"));
            var spanishDateAttrValue = sut.Element("spanishDateTag").Attribute("attr").GetValue<DateTime>(CultureInfo.CreateSpecificCulture("es-ES"));

            var notExistingStringAttributeValue = sut.Element("stringTag").GetAttributeValueOrDefault<string>("notExisting");
            var notExistingDecimalAttributeValue = sut.Element("decimalTag").GetAttributeValueOrDefault<decimal?>("notExisting");

            intTagValue.Should().Be(5);
            intAttrValue.Should().Be(10);
            stringTagValue.Should().Be("6");
            stringAttrValue.Should().Be("11");

            decimalTagValue.Should().Be(5.35m);
            decimalAttrValue.Should().Be(15.35m);

            invariantDateTagValue.Should().Be(new DateTime(2011, 3, 20));
            invariantDateAttrValue.Should().Be(new DateTime(2011, 4, 20));

            spanishDateTagValue.Should().Be(new DateTime(2011, 3, 20));
            spanishDateAttrValue.Should().Be(new DateTime(2011, 3, 21));

            notExistingStringAttributeValue.Should().BeNull();
            notExistingDecimalAttributeValue.Should().NotHaveValue();
        }

        [TestMethod]
        public void Should_get_default_value_for_type_when_attribute_is_missing_and_no_default_value_is_provided()
        {
           var sut = XElement.Load(new StringReader(Xml));
           var result = sut.GetAttributeValueOrDefault<string>("MissingAttrName");
           result.Should().Be(default(string));
        }

        [TestMethod]
        public void Should_get_specified_default_value_when_attribute_is_missing_and_a_default_value_is_provided()
        {
           var sut = XElement.Load(new StringReader(Xml));
           var theProvidedDefaultValue = "the provided default value";
           var result = sut.GetAttributeValueOrDefault<string>("MissingAttrName", theProvidedDefaultValue);
           result.Should().Be(theProvidedDefaultValue);
        }

        [TestMethod]
        public void Should_fail_if_required_attribute_is_missing()
        {
           var sut = XElement.Load(new StringReader(Xml));
           Action a = () => sut.GetRequiredAttributeValue<string>("MissingAttrName");
           a.ShouldThrow<ArgumentException>();
        }

    }
}
