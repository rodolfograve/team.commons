﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TEAM.Commons.Web.Tests
{
   public class TestControllerWithGetAndPost : Controller
   {

      [HttpGet]
      public ActionResult Execute()
      {
         return null;
      }

      [HttpPost]
      public ActionResult Execute(string p)
      {
         return null;
      }

   }
}
