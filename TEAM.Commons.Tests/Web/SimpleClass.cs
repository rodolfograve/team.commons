﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Web.Tests
{
    class SimpleClass
    {
        public string StringProp { get; set; }

        public int IntProp { get; set; }

        public DateTime DateTimeProp { get; set; }
    }
}
