﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Web.Routing;

namespace TEAM.Commons.Web.Tests.MVC.TestExtensions
{
    [TestClass]
    public class RedirectToRouteResultTestExtensionsFixture
    {

        [TestMethod]
        public void Should_fail_if_RouteName_doesnt_match()
        {
            var routeName = "Route Name";
            var sut = new RedirectToRouteResult(routeName, new RouteValueDictionary());

            sut.WithRouteName(routeName);
        }

        [TestMethod]
        public void Should_validate_ContainsNull()
        {
           var sut = new RedirectToRouteResult("test", new RouteValueDictionary(new { NullKey = (object)null }));

           sut.ContainsNull("NullKey");
        }

        [TestMethod]
        public void Should_not_fail_when_Contains_compares_to_Null()
        {
           var sut = new RedirectToRouteResult("test", new RouteValueDictionary(new { NullKey = (object)null }));

           sut.Contains("NullKey", (object)null);
        }
    }
}
