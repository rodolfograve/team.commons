﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web;
using System.Collections.Specialized;

namespace TEAM.Commons.Web.Tests.MVC.TestExtensions
{
    [TestClass]
    public class ControllerTestsExtensionsFixture
    {
        [TestMethod]
        public void Should_bind_HttpPostedFileBase_when_the_file_is_null_and_not_required()
        {
            var values = new NameValueCollection();
            var sut = new TestNonSingleActionController();
            var result = sut.ExerciseWithRawValues<HttpPostedFileBase>(sut.FileCanBeNull, values);
            result.AssertIsView().WithName("NULL");
        }

        [TestMethod]
        [Ignore] // Need to get the File binding working!
        public void Should_get_invalid_ModelState_when_file_is_null_and_required()
        {
            var values = new NameValueCollection();
            var sut = new TestNonSingleActionController();
            var result = sut.ExerciseWithRawValues<HttpPostedFileBase>(sut.FileIsRequired, values);
            result.AssertIsView().WithName("FAIL");
        }

        [TestMethod]
        [Ignore] // Need to get the File binding working!
        public void Should_bind_HttpPostedFileBase_when_file_is_NOT_null()
        {
            var values = new NameValueCollection();
            var files = new NameValueCollection()
             {
                { "file", "something"}
             };
            var sut = new TestNonSingleActionController();
            var result = sut.ExerciseWithRawValues<HttpPostedFileBase>(sut.FileCanBeNull, values, files);
            result.AssertIsView().WithName("OK");
        }

        [TestMethod]
        public void Should_bind_a_SimpleClass_when_ExerciseWithRawValues()
        {
            var values = new NameValueCollection()
             {
                { "StringProp", "something"},
                { "IntProp", "123"},
                { "DateTimeProp", "something"},
             };
            var sut = new TestNonSingleActionController();
            var result = sut.ExerciseWithRawValues<HttpPostedFileBase>(sut.FileIsRequired, values);
        }
    }
}
