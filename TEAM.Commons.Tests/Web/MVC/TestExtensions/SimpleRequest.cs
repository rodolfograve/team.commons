﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace TEAM.Commons.Web.Tests.MVC.TestExtensions
{
    public class SimpleRequest
    {
        public string StringProp { get; set; }

        public int IntProp { get; set; }

        public DateTime DateTimeProp { get; set; }

        [Required]
        public string StringPropRequired { get; set; }

        [Required]
        public int? IntPropRequired { get; set; }

        [Required]
        public DateTime? DateTimePropRequired { get; set; }
    }
}
