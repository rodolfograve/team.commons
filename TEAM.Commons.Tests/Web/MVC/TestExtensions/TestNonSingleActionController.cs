﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TEAM.Commons.Web.Tests.MVC.TestExtensions
{
    public class TestNonSingleActionController : Controller
    {

        public ActionResult FileCanBeNull(HttpPostedFileBase file)
        {
            return DoSomethingWithAnHttpPostedFileBase(file);
        }

        public ActionResult FileIsRequired([Required]HttpPostedFileBase file)
        {
            return DoSomethingWithAnHttpPostedFileBase(file);
        }

        protected ActionResult DoSomethingWithAnHttpPostedFileBase(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    return View("NULL");
                }
                else
                {
                    return View("OK");
                }
            }
            else
            {
                return View("FAIL");
            }
        }

        /// <summary>
        /// Not SingleActionController because it's not public and doesn't return ActionResult.
        /// </summary>
        private void Execute()
        {
        }

        /// <summary>
        /// Not SingleActionController because it's not public.
        /// </summary>
        protected ActionResult Execute(int p1)
        {
           return null;
        }

        /// <summary>
        /// Not SingleActionController because it doesn't return ActionResult.
        /// </summary>
        public void Execute(string p1)
        {
        }

    }
}
