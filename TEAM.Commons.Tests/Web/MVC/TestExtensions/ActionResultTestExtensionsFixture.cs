﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FluentAssertions;

namespace TEAM.Commons.Web.Tests.MVC.TestExtensions
{
   [TestClass]
   public class ActionResultTestExtensionsFixture
   {

      [TestMethod]
      public void Should_not_fail_when_reason_message_contains_curly_brackets_and_there_are_no_reasonArgs()
      {
         ViewResult sut = new ViewResult();
         sut.AssertIsView("Some {curly} brackets");
      }

      [TestMethod]
      public void Should_not_fail_when_reason_message_contains_even_open_curly_brackets_and_there_are_no_reasonArgs()
      {
         ViewResult sut = new ViewResult();
         sut.AssertIsView("Some {curly brackets");
      }

      [TestMethod]
      public void Should_not_fail_when_reason_message_contains_even_closed_curly_brackets_and_there_are_no_reasonArgs()
      {
         ViewResult sut = new ViewResult();
         sut.AssertIsView("Some curly} brackets");
      }

      [TestMethod]
      public void Should_not_fail_when_reason_message_contains_curly_brackets_and_there_are_reasonArgs()
      {
         ViewResult sut = new ViewResult();
         sut.AssertIsView("Some {curly} brackets", "some value");
      }

      [TestMethod]
      public void Should_not_fail_when_reason_message_contains_formattable_curly_brackets_and_there_are_reasonArgs()
      {
         ViewResult sut = new ViewResult();
         sut.AssertIsView("Some {0} brackets", "some value");
      }

   }
}
