﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons.Web.Tests.Controllers;
using FluentAssertions;

namespace TEAM.Commons.Web.Tests.MVC.SingleActionControllers
{
   [TestClass]
   public class SingleActionControllerExtensionsFixture
   {

      [TestMethod]
      public void RedirectToAction_Should_return_a_RedirectToRoute_with_the_especified_action_and_controller_values()
      {
         var sut = new TestSingleActionController();

         var result = sut.RedirectToAction(typeof(TestSingleActionController));

         var controller = result.RouteValues["controller"];
         var action = result.RouteValues["action"];

         controller.Should().NotBeNull();
         controller.Should().Be("Controllers");
         action.Should().NotBeNull();
         action.Should().Be("TestSingleActionController");
      }

      [TestMethod]
      public void RedirectToAction_Should_return_a_RedirectToRoute_with_the_especified_action_and_controller_values_plus_the_provided_route_values()
      {
         var sut = new TestSingleActionController();

         var result = sut.RedirectToAction(typeof(TestSingleActionController), new { Prop1 = "Prop1Value" });

         var controller = result.RouteValues["controller"];
         var action = result.RouteValues["action"];
         var prop1 = result.RouteValues["Prop1"];

         controller.Should().NotBeNull();
         controller.Should().Be("Controllers");
         action.Should().NotBeNull();
         action.Should().Be("TestSingleActionController");
         prop1.Should().NotBeNull();
         prop1.Should().Be("Prop1Value");
      }

   }
}
