﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons.Web.Tests.MVC.TestExtensions;
using FluentAssertions;
using System.Web.Mvc;
using Autofac.Core.Registration;

namespace TEAM.Commons.Web.Tests.MVC.SingleActionControllers
{
    [TestClass]
    public class AutofacRegistrationExtensionsFixture
    {

        [TestMethod]
        public void Should_register_only_the_SingleActionControllers()
        {
            var sut = new ContainerBuilder();
            sut.RegisterTypesForSingleActionControllers(typeof(TestSingleActionController).Assembly, "TEAM.Commons.Web.Tests.MVC.TestExtensions");
            var result = sut.Build();
            result.Resolve<TestSingleActionController>().Should().NotBeNull();
            result.Resolve<TestSingleActionControllerWithParameters>().Should().NotBeNull();

            Action a = () => result.Resolve<TestNonSingleActionController>();
            a.ShouldThrow<ComponentNotRegisteredException>();
        }


    }
}
