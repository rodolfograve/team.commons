﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons.Web.MVC.SingleActionControllers;
using FluentAssertions;

namespace TEAM.Commons.Web.MVC.SingleActionControllers.Tests
{
    [TestClass]
    public class SingleActionControllerFactoryFixture
    {
        [TestMethod]
        public void GetSingleActionControllerTypeNameFromControllerName()
        {
            const string controllersAssembly = "TEAM.Commons.Web";
            const string controllersBaseNamespace = "TEAM.Commons.Web.MVC.SingleActionControllers.Tests.Controllers";
            const string controllerName = "Project";
            const string actionName = "Index";

            var sut = new SingleActionControllerFactory(controllersAssembly, controllersBaseNamespace);

            var expected = string.Format("{0}.{1}.{2}, {3}", controllersBaseNamespace, controllerName, actionName, controllersAssembly);
            
            var actual = sut.GetSingleActionControllerTypeNameFrom(controllerName, actionName);
            actual.Should().Be(expected);
        }

        [TestMethod]
        public void Should_get_controllerName_from_Type()
        {
           var result = SingleActionControllerFactory.GetController(typeof(TEAM.Commons.Web.Tests.Controllers.TestSingleActionController));
           result.Should().Be("Controllers");
        }

        [TestMethod]
        public void Should_get_actionName_from_Type()
        {
           var result = SingleActionControllerFactory.GetAction(typeof(TEAM.Commons.Web.Tests.Controllers.TestSingleActionController));
           result.Should().Be("TestSingleActionController");
        }
    }
}
