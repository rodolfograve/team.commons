﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace TEAM.Commons.Web.Tests
{
   [TestClass]
    public class JsonExtensionsFixture
    {
        [TestMethod]
        public void ToJson()
        {
            var sut = new SimpleClass()
            {
                StringProp = "Hello",
                IntProp = 23,
                DateTimeProp = new DateTime(2011, 3, 26)
            };

            string result = sut.ToJson();
            result.Should().NotBeNullOrWhiteSpace();
        }
    }
}
