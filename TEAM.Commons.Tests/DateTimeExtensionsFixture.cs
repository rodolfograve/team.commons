﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class DateTimeExtensionsFixture
    {

        [TestMethod]
        public void FirstDayOfMonth_should_return_correct_values()
        {
            var date = Any.DateTime();
            var lastDayOfMonth = date.FirstDayOfMonth();
            lastDayOfMonth.Year.Should().Be(date.Year);
            lastDayOfMonth.Month.Should().Be(date.Month);
            lastDayOfMonth.Day.Should().Be(1);
        }

        [TestMethod]
        public void LastDayOfMonth_should_return_correct_values()
        {
            var date = Any.DateTime();
            var lastDayOfMonth = date.LastDayOfMonth();
            lastDayOfMonth.Year.Should().Be(date.Year);
            lastDayOfMonth.Month.Should().Be(date.Month);
            lastDayOfMonth.Day.Should().Be(DateTime.DaysInMonth(date.Year, date.Month));
        }

        [TestClass]
        public class GenerateSequenceFutureDaysUntil
        {

            [TestMethod]
            public void Should_return_an_empty_sequence_when_from_and_to_are_the_same_NOT_including_from()
            {
                var dateTime = Any.DateTime();
                var result = dateTime.GenerateSequenceFutureDaysUntil(dateTime, false).ToList();
                result.Should().BeEmpty();
            }

            [TestMethod]
            public void Should_return_a_one_element_sequence_when_from_and_to_are_the_same_including_from()
            {
                var dateTime = Any.DateTime();
                var result = dateTime.GenerateSequenceFutureDaysUntil(dateTime).ToList();
                result.Should().HaveCount(1);
                result.Single().Should().Be(dateTime);
            }

            [TestMethod]
            public void Should_return_a_two_element_sequence_when_from_and_to_are_consecutive_including_from()
            {
                var from = Any.DateTime();
                var to = from.AddDays(1);
                var result = from.GenerateSequenceFutureDaysUntil(to).ToList();
                result.Should().HaveCount(2);
                result[0].Should().Be(from);
                result[1].Should().Be(to);
            }

            [TestMethod]
            public void Should_return_a_sequence_with_from_when_from_and_to_are_consecutive_NOT_including_from()
            {
                var from = Any.DateTime();
                var to = from.AddDays(1);
                var result = from.GenerateSequenceFutureDaysUntil(to, false).ToList();
                result.Should().HaveCount(1);
                result[0].Should().Be(to);
            }
        }
        [TestClass]
        public class GenerateSequencePastDaysUntil
        {

            [TestMethod]
            public void Should_return_an_empty_sequence_when_from_and_to_are_the_same_NOT_including_from()
            {
                var dateTime = Any.DateTime();
                var result = dateTime.GenerateSequencePastDaysUntil(dateTime, false).ToList();
                result.Should().BeEmpty();
            }

            [TestMethod]
            public void Should_return_a_one_element_sequence_when_from_and_to_are_the_same_including_from()
            {
                var dateTime = Any.DateTime();
                var result = dateTime.GenerateSequencePastDaysUntil(dateTime).ToList();
                result.Should().HaveCount(1);
                result.Single().Should().Be(dateTime);
            }

            [TestMethod]
            public void Should_return_a_two_element_sequence_when_from_and_to_are_consecutive_including_from()
            {
                var from = Any.DateTime();
                var to = from.AddDays(-1);
                var result = from.GenerateSequencePastDaysUntil(to).ToList();
                result.Should().HaveCount(2);
                result[0].Should().Be(from);
                result[1].Should().Be(to);
            }

            [TestMethod]
            public void Should_return_a_sequence_with_from_when_from_and_to_are_consecutive_NOT_including_from()
            {
                var from = Any.DateTime();
                var to = from.AddDays(-1);
                var result = from.GenerateSequencePastDaysUntil(to, false).ToList();
                result.Should().HaveCount(1);
                result[0].Should().Be(to);
            }
        }

        [TestClass]
        public class GenerateSequenceFutureDays
        {
            [TestMethod]
            public void Should_start_from_from_when_including_from()
            {
                var from = Any.DateTime();
                var result = from.GenerateSequenceFutureDays();
                result.First().Should().Be(from);
            }

            [TestMethod]
            public void Should_start_from_after_from_when_NOT_including_from()
            {
                var from = Any.DateTime();
                var result = from.GenerateSequenceFutureDays(false);
                result.First().Should().Be(from.AddDays(1));
            }
        }

        [TestClass]
        public class GenerateSequencePastDays
        {
            [TestMethod]
            public void Should_start_from_from_when_including_from()
            {
                var from = Any.DateTime();
                var result = from.GenerateSequencePastDays();
                result.First().Should().Be(from);
            }

            [TestMethod]
            public void Should_start_from_after_from_when_NOT_including_from()
            {
                var from = Any.DateTime();
                var result = from.GenerateSequencePastDays(false);
                result.First().Should().Be(from.AddDays(-1));
            }
        }

    }
}
