﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class LinqAsyncExtensionsFixture
    {
        [TestMethod]
        public async Task SelectAsync_should_return_all_values_when_select_returns_immediately()
        {
            var input = Enumerable.Range(1, 10).ToArray();
            var result = await input.SelectAsync(x => Task.FromResult(x));
            result.Should().HaveCount(input.Length);
            result.Should().BeEquivalentTo(input);
        }

        [TestMethod]
        public async Task SelectAsync_should_return_all_values_when_select_is_really_async()
        {
            var input = Enumerable.Range(1, 10).ToArray();
            var result = await input.SelectAsync(async x =>
            {
                await Task.Delay(1);
                return x;
            });
            result.Should().HaveCount(input.Length);
            result.Should().BeEquivalentTo(input);
        }

        [TestMethod]
        public async Task WhereAsync_should_return_all_values_when_predicate_is_true()
        {
            var input = Enumerable.Range(1, 10).ToArray();
            var result = await input.WhereAsync(async x =>
            {
                await Task.Delay(1);
                return true;
            });
            result.Should().HaveCount(input.Length);
            result.Should().BeEquivalentTo(input);
        }

        [TestMethod]
        public async Task WhereAsync_should_return_NO_values_when_predicate_is_true()
        {
            var input = Enumerable.Range(1, 10).ToArray();
            var result = await input.WhereAsync(async x =>
            {
                await Task.Delay(1);
                return false;
            });
            result.Should().BeEmpty();
        }
    }
}
