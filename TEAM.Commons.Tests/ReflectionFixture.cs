﻿using System;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace TEAM.Commons.Tests
{
    public class ReflectionFixture
    {

        [TestFixture]
        public class GetMethod
        {
            [TestCase("PrivateAction", typeof(void))]
            [TestCase("ProtectedAction", typeof(void))]
            [TestCase("PublicAction", typeof(void))]
            [TestCase("PrivateFunc", typeof(int))]
            [TestCase("ProtectedFunc", typeof(string))]
            [TestCase("PublicFunc", typeof(object))]
            public void Should_find_parameterless(string methodName, Type returnType)
            {
                var result = Reflection<Sample>.GetMethodOrNull(methodName, returnType);
                result.Should().NotBeNull();
                result.Name.Should().Be(methodName);
                result.ReturnType.Should().Be(returnType);

                var parameters = result.GetParameters();
                parameters.Should().BeEmpty();
            }

            [TestCase("PrivateAction", typeof(void), typeof(int))]
            [TestCase("ProtectedAction", typeof(void), typeof(string))]
            [TestCase("PublicAction", typeof(void), typeof(object))]
            [TestCase("PrivateFunc", typeof(int), typeof(object))]
            [TestCase("ProtectedFunc", typeof(string), typeof(int))]
            [TestCase("PublicFunc", typeof(object), typeof(string))]
            public void Should_find_with_one_parameter(string methodName, Type returnType, Type input1)
            {
                var result = Reflection<Sample1>.GetMethodOrNull(methodName, returnType, input1);
                result.Should().NotBeNull();
                result.Name.Should().Be(methodName);
                result.ReturnType.Should().Be(returnType);

                var parameters = result.GetParameters();
                parameters.Should().HaveCount(1);
                parameters[0].ParameterType.Should().Be(input1);
            }

            [TestCase("PrivateAction1", typeof(void), typeof(int))]
            [TestCase("ProtectedAction", typeof(int), typeof(string))]
            [TestCase("PublicAction2", typeof(void), typeof(int))]
            [TestCase("PrivateFunc1", typeof(int), typeof(object))]
            [TestCase("ProtectedFunc", typeof(object), typeof(int))]
            [TestCase("PublicFunc", typeof(object), typeof(int))]
            public void Should_return_null_when_not_found(string methodName, Type returnType, Type input1)
            {
                var result = Reflection<Sample1>.GetMethodOrNull(methodName, returnType, input1);
                result.Should().BeNull();
            }
        }

        [TestFixture]
        public class GetAction
        {
            [TestCase("PrivateAction")]
            [TestCase("ProtectedAction")]
            [TestCase("PublicAction")]
            public void Should_find_parameterless(string methodName)
            {
                var result = Reflection<Sample>.GetActionOrNull(methodName);
                result.Should().NotBeNull();
                result.Name.Should().Be(methodName);
                result.ReturnType.Should().Be(typeof(void));

                var parameters = result.GetParameters();
                parameters.Should().BeEmpty();
            }

            [TestCase("PrivateAction", typeof(int))]
            [TestCase("ProtectedAction", typeof(string))]
            [TestCase("PublicAction", typeof(object))]
            public void Should_find_with_one_parameter(string methodName, Type input1)
            {
                var result = Reflection<Sample1>.GetActionOrNull(methodName, input1);
                result.Should().NotBeNull();
                result.Name.Should().Be(methodName);
                result.ReturnType.Should().Be(typeof(void));

                var parameters = result.GetParameters();
                parameters.Should().HaveCount(1);
                parameters[0].ParameterType.Should().Be(input1);
            }

            [TestCase("PrivateAction1", typeof(int))]
            [TestCase("ProtectedAction", typeof(object))]
            [TestCase("PublicAction2", typeof(int))]
            [TestCase("PrivateFunc1", typeof(object))]
            [TestCase("ProtectedFunc", typeof(int))]
            [TestCase("PublicFunc", typeof(string))]
            public void Should_return_null_when_not_found(string methodName, Type input1)
            {
                var result = Reflection<Sample1>.GetActionOrNull(methodName, input1);
                result.Should().BeNull();
            }
        }

        [TestFixture]
        public class GetFunction
        {
            [TestCase("PrivateAction", typeof(void))]
            [TestCase("ProtectedAction", typeof(void))]
            [TestCase("PublicAction", typeof(void))]
            [TestCase("PrivateFunc", typeof(int))]
            [TestCase("ProtectedFunc", typeof(string))]
            [TestCase("PublicFunc", typeof(object))]
            public void Should_find_parameterless(string methodName, Type returnType)
            {
                var result = Reflection<Sample>.GetFuncOrNull(methodName, returnType);
                result.Should().NotBeNull();
                result.Name.Should().Be(methodName);
                result.ReturnType.Should().Be(returnType);

                var parameters = result.GetParameters();
                parameters.Should().BeEmpty();
            }

            [TestCase("PrivateAction", typeof(void), typeof(int))]
            [TestCase("ProtectedAction", typeof(void), typeof(string))]
            [TestCase("PublicAction", typeof(void), typeof(object))]
            [TestCase("PrivateFunc", typeof(int), typeof(object))]
            [TestCase("ProtectedFunc", typeof(string), typeof(int))]
            [TestCase("PublicFunc", typeof(object), typeof(string))]
            public void Should_find_with_one_parameter(string methodName, Type returnType, Type input1)
            {
                var result = Reflection<Sample1>.GetFuncOrNull(methodName, returnType, input1);
                result.Should().NotBeNull();
                result.Name.Should().Be(methodName);
                result.ReturnType.Should().Be(returnType);

                var parameters = result.GetParameters();
                parameters.Should().HaveCount(1);
                parameters[0].ParameterType.Should().Be(input1);
            }

            [TestCase("PrivateAction1", typeof(void), typeof(int))]
            [TestCase("ProtectedAction", typeof(int), typeof(string))]
            [TestCase("PublicAction2", typeof(void), typeof(int))]
            [TestCase("PrivateFunc1", typeof(int), typeof(object))]
            [TestCase("ProtectedFunc", typeof(object), typeof(int))]
            [TestCase("PublicFunc", typeof(object), typeof(int))]
            public void Should_return_null_when_not_found(string methodName, Type returnType, Type input1)
            {
                var result = Reflection<Sample1>.GetFuncOrNull(methodName, returnType, input1);
                result.Should().BeNull();
            }
        }

        private class Sample
        {
            private void PrivateAction() { }
            protected void ProtectedAction() { }
            public void PublicAction() { }

            private int PrivateFunc() => 0;
            protected string ProtectedFunc() => "";
            public object PublicFunc() => new object();

        }

        private class Sample1
        {
            private void PrivateAction(int i) { }
            protected void ProtectedAction(string s) { }
            public void PublicAction(object o) { }

            private int PrivateFunc(object o) => 0;
            protected string ProtectedFunc(int i) => "";
            public object PublicFunc(string s) => new object();

        }
    }
}
