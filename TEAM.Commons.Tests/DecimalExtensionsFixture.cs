﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class DecimalExtensionsFixture
    {

        [TestMethod]
        public void Should_return_0_decimal_places_for_0()
        {
            (0m).GetNumberOfDecimalPlaces().Should().Be(0);
        }

        [TestMethod]
        public void Should_return_0_decimal_places_when_there_are_no_decimal_places()
        {
            (3m).GetNumberOfDecimalPlaces().Should().Be(0);
        }

        [TestMethod]
        public void Should_return_right_number_of_decimal_places_1()
        {
            (3.1m).GetNumberOfDecimalPlaces().Should().Be(1);
        }

        [TestMethod]
        public void Should_return_right_number_of_decimal_places_2()
        {
            (3.123m).GetNumberOfDecimalPlaces().Should().Be(3);
        }

        [TestMethod]
        public void Should_return_right_number_of_decimal_places_when_there_are_heading_zeroes()
        {
            (3.003m).GetNumberOfDecimalPlaces().Should().Be(3);
        }

        [TestMethod]
        public void Should_return_right_number_of_decimal_places_for_trailing_zeroes()
        {
            (3.000m).GetNumberOfDecimalPlaces().Should().Be(3);
        }

        [TestMethod]
        public void Should_return_1_decimal_place_for_one_trailing_zero()
        {
            (3.0m).GetNumberOfDecimalPlaces().Should().Be(1);
        }




    }
}
