﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using System.Data;
using FluentAssertions;

namespace TEAM.Commons.Tests
{
    [TestClass]
    public class DbCommandExtensionsFixture
    {
        [TestMethod]
        public void Can_Add_Parameters_To_SqlCommand_From_Anonymous_Object()
        {
            var sut = new SqlCommand();
            sut.AddParameters(new { Tmp = "tmpValue" });

            sut.Parameters.Should().HaveCount(1);
            sut.Parameters[0].ParameterName.Should().Be("@Tmp");
            sut.Parameters["@Tmp"].ParameterName.Should().Be("@Tmp");
            sut.Parameters[0].Value.Should().Be("tmpValue");
        }

        [TestMethod]
        public void Can_Add_Existing_Parameters_To_SqlCommand_From_Anonymous_Object()
        {
            var sut = new SqlCommand();
            sut.Parameters.Add("@Tmp", SqlDbType.VarChar);

            sut.AddParameters(new { Tmp = "tmpValue" });

            sut.Parameters.Should().HaveCount(1);
            sut.Parameters[0].ParameterName.Should().Be("@Tmp");
            sut.Parameters["@Tmp"].ParameterName.Should().Be("@Tmp");
            sut.Parameters[0].Value.Should().Be("tmpValue");
        }

        [TestMethod]
        public void Can_Format_Complete_SqlCommand()
        {
            SqlConnection conn = new SqlConnection("Data Source=someserver;Initial Catalog=somedatabase;User ID=someuser;Password=somepassword");
            SqlCommand sut = conn.CreateCommand();
            sut.CommandText = "SELECT * FROM SOMETABLE WHERE SomeColumn=@SomeColumn";
            sut.Parameters.AddWithValue("@SomeColumn", "somevalue");
            string result = sut.Format();

            result.Should().NotBeNullOrWhiteSpace();
        }

    }
}
