﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEAM.Commons.Logging;

namespace TEAM.Commons.Tests.Logging
{
    [TestClass]
    public class SynchronizationLogDecoratorSpec
    {
        [TestMethod]
        public async Task Should_log_all_entries_from_multiple_threads_synchronously()
        {
            using (var log = new CounterLog())
            {
                using (var sut = new SynchronizationLogDecorator(log))
                {
                    var tasks = Enumerable.Range(1, 10).Select(async i =>
                    {
                        for (int j = 0; j < 1000; j++)
                        {
                            // Using log.Log instead of sut.Log in the line below makes the test fail, as expected
                            await Task.Run(() => sut.Log(i)).ConfigureAwait(false);
                        }
                    });
                    await Task.WhenAll(tasks).ConfigureAwait(false);
                    log.LogEntries.Should().Be(10 * 1000);
                }
            }
        }

        [TestMethod]
        public void Should_dispose_StreamWriter_when_dispose_if_DisposeToDecorate()
        {
            var testLog = new TestLog();
            using (ILog sut = new SynchronizationLogDecorator(testLog, true))
            {
            }
            testLog.HasBeenDisposed.Should().BeTrue();
        }

        [TestMethod]
        public void Should_NOT_dispose_StreamWriter_when_dispose_if_NOT_DisposeToDecorate()
        {
            var testLog = new TestLog();
            using (ILog sut = new SynchronizationLogDecorator(testLog, false))
            {
            }
            testLog.HasBeenDisposed.Should().BeFalse();
        }

        private class TestLog : ILog
        {
            public bool HasBeenDisposed { get; private set; }
            public void Dispose()
            {
                HasBeenDisposed = true;
            }

            public void Log(object content, Exception exception = null, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = 0, params string[] tags)
            {
            }

            public Task LogAsync(object content, Exception exception = null, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = 0, params string[] tags) =>
                Task.FromResult(1);
        }
    }
}
