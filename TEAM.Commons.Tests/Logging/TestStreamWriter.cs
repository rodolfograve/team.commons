﻿using System.IO;

namespace TEAM.Commons.Tests.Logging
{
    public class TestStreamWriter : MemoryStream
    {
        public bool HasBeenDisposed { get; private set; }
        protected override void Dispose(bool disposing)
        {
            HasBeenDisposed = true;
            base.Dispose(disposing);
        }
    }
}
