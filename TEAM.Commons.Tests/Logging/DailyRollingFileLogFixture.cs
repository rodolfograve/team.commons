﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Text;
using TEAM.Commons.Logging;
using FluentAssertions;

namespace TEAM.Commons.Tests.Logging
{
    [TestClass]
    public class FileLogFixture
    {
        [TestMethod]
        public void Should_log_a_string()
        {
            using (ILog sut = new DailyRollingFileLog(Path.GetTempPath(), "test", "log", Mock.Of<ILogEntrySerializer>(), new SystemDailyRollingFileLogDateTime()))
            {
                sut.Log("Testing the logging...", tags: new string[] { "blas", "blas" });
            }
        }

        [TestMethod]
        public void Should_log_a_string_with_several_lines()
        {
            using (ILog sut = new DailyRollingFileLog(Path.GetTempPath(), "test", "log", Mock.Of<ILogEntrySerializer>(), new SystemDailyRollingFileLogDateTime()))
            {
                sut.Log(new StringBuilder().AppendLine("Testing the logging...").AppendLine("Another line...").AppendLine("Yet another line...").ToString());
            }
        }

        [TestMethod]
        public void Should_roll_file_when_date_changes()
        {
            var dateTimeMock = new Mock<IDailyRollingFileLogDateTime>();
            var today = DateTime.Today;
            var tomorrow = today.AddDays(1);

            dateTimeMock.Setup(x => x.Today).Returns(today);

            using (ILog sut = new DailyRollingFileLog(Path.GetTempPath(), "test", "log", Mock.Of<ILogEntrySerializer>(), dateTimeMock.Object))
            {
                sut.Log("Line written today");

                dateTimeMock.Setup(x => x.Today).Returns(tomorrow);
                sut.Log("Line written tomorrow");
            }
        }

        [TestMethod]
        public void Should_dispose_StreamWriter_when_dispose()
        {
            var writer = new TestStreamWriter();
            var streamWriterFactoryMock = new Mock<IStreamWriterFactory>();
            streamWriterFactoryMock.Setup(x => x.Create(It.IsAny<string>())).Returns(new StreamWriter(writer));
            using (ILog sut = new DailyRollingFileLog(Path.GetTempPath(), "test", "log", Mock.Of<ILogEntrySerializer>(), new SystemDailyRollingFileLogDateTime(), streamWriterFactoryMock.Object))
            {
                sut.Log("Testing the logging...", tags: new string[] { "blas", "blas" });
            }
            writer.HasBeenDisposed.Should().BeTrue();
        }
    }
}
