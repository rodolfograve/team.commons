﻿using Moq;
using NUnit.Framework;
using System;
using TEAM.Commons.Logging;
using System.Runtime.CompilerServices;

namespace TEAM.Commons.Tests.Logging
{
    [TestFixture]
    public class SectionLoggerFixture
    {
        private const string ExpectedFilePath = @"TEAM.Commons.Tests\Logging\SectionLoggerFixture.cs"; // This fixture's file path

        [Test]
        public void Should_log_enter_and_exit_method_with_default_options()
        {
            var sut = new Mock<ILog>();

            using (sut.Object.LogMethod())
            {
            }

            const int logMethodCallLineNumber = 19;
            VerifyInvokedEnteringSection(sut, logMethodCallLineNumber);
            VerifyInvokedExitingSectionWithoutElapsedTime(sut, logMethodCallLineNumber);
        }

        [Test]
        public void Should_log_enter_and_exit_method_including_ElapsedTime()
        {
            var sut = new Mock<ILog>();

            using (sut.Object.LogMethod(true))
            {
            }

            const int logMethodCallLineNumber = 33;
            VerifyInvokedEnteringSection(sut, logMethodCallLineNumber);
            VerifyInvokedExitingSectionWithElapsedTime(sut, logMethodCallLineNumber);
        }

        [Test]
        public void Should_log_enter_and_exit_method_even_when_there_is_an_exception()
        {
            var sut = new Mock<ILog>();

            try
            {
                using (sut.Object.LogMethod())
                {
                    throw new Exception("Intentional exception simulating a throwing method.");
                }
            }
            catch
            {
            }

            const int logMethodCallLineNumber = 49;
            VerifyInvokedEnteringSection(sut, logMethodCallLineNumber);
            VerifyInvokedExitingSectionWithoutElapsedTime(sut, logMethodCallLineNumber);
        }

        private void VerifyInvokedEnteringSection(Mock<ILog> sut, int callerLineNumber, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "") =>
            sut.Verify(x => x.Log(
                    It.Is<string>(m => IsEnteringSection(m, callerMemberName)),
                    null,
                    callerMemberName,
                    callerFilePath,
                    callerLineNumber
                ),
                Times.Once);

        private void VerifyInvokedExitingSectionWithoutElapsedTime(Mock<ILog> sut, int callerLineNumber, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "") =>
            sut.Verify(x => x.Log(
                    It.Is<string>(m => IsExitingSectionWithoutElapsedTime(m, callerMemberName)),
                    null,
                    callerMemberName,
                    callerFilePath,
                    callerLineNumber
                ),
                Times.Once);

        private void VerifyInvokedExitingSectionWithElapsedTime(Mock<ILog> sut, int callerLineNumber, [CallerMemberName]string callerMemberName = "", [CallerFilePath]string callerFilePath = "") =>
            sut.Verify(x => x.Log(
                    It.Is<string>(m => IsExitingSectionWithElapsedTime(m, callerMemberName)),
                    null,
                    callerMemberName,
                    callerFilePath,
                    callerLineNumber
                ),
                Times.Once);

        private bool IsEnteringSection(string message, string sectionName) =>
            message.StartsWith("Entering section") && message.Contains(sectionName);

        private bool IsExitingSection(string message, string sectionName) =>
            message.StartsWith("Exiting section") && message.Contains(sectionName);

        private bool IsExitingSectionWithoutElapsedTime(string message, string sectionName) =>
            IsExitingSection(message, sectionName) && !message.Contains("Took");

        private bool IsExitingSectionWithElapsedTime(string message, string sectionName) =>
            IsExitingSection(message, sectionName) && message.Contains("Took");
    }
}
