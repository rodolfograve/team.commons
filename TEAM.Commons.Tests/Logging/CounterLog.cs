﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using TEAM.Commons.Logging;

namespace TEAM.Commons.Tests.Logging
{
    public class CounterLog : ILog
    {
        public CounterLog()
        {
        }

        private bool IsDisposing;

        public int LogEntries { get; private set; }

        public void Log(object content, Exception exception = null, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int callerLineNumber = 0, params string[] tags)
        {
            CheckDispose();
            LogEntries++;
        }

        private void CheckDispose()
        {
            if (IsDisposing)
            {
                throw new ObjectDisposedException(nameof(CounterLog));
            }
        }

        public void Dispose() => IsDisposing = true;
    }
}
