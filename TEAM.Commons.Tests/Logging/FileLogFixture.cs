﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;
using System.Text;
using TEAM.Commons.Logging;

namespace TEAM.Commons.Tests.Logging
{
    [TestClass]
    public class DailyRollingFileLogFixture
    {
        [TestMethod]
        public void Should_log_a_string()
        {
            using (ILog sut = new FileLog(Path.GetTempFileName(), Mock.Of<ILogEntrySerializer>()))
            {
                sut.Log("Testing the logging...");
            }
        }

        [TestMethod]
        public void Should_log_a_string_with_several_lines()
        {
            using (ILog sut = new FileLog(Path.GetTempFileName(), Mock.Of<ILogEntrySerializer>()))
            {
                sut.Log(new StringBuilder().AppendLine("Testing the logging...").AppendLine("Another line...").AppendLine("Yet another line...").ToString());
            }
        }

        [TestMethod]
        public void Should_log_with_an_Exception()
        {
            using (ILog sut = new FileLog(Path.GetTempFileName(), Mock.Of<ILogEntrySerializer>()))
            {
                sut.Log("Testing the logging...", new Exception("And this is an Exception"));
            }
        }
    }
}
