﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Rodolfo Grave - http://rodolfograve.blogspot.com/team.commons")]
[assembly: AssemblyProduct("TEAM.Commons")]
[assembly: AssemblyCopyright("Copyright © Rodolfo Grave - http://rodolfograve.blogspot.com 2014")]
[assembly: AssemblyTrademark("http://rodolfograve.blogspot.com")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("2.2.0")]
[assembly: AssemblyFileVersion("2.2.0")]