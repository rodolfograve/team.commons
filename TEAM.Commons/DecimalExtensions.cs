﻿using System;

// Keep namespace-less so that the extension methods are available as soon as you add a reference to TEAM.Commons.
public static class DecimalExtensions
{

    /// <summary>
    /// Returns the number of decimal places of a decimal number, as stored in memory.
    /// Notice this is not about relevant places but in-memory representation.
    /// </summary>
    /// <param name="target">The decimal number.</param>
    /// <returns>The number of decimal places as represented in-memory.</returns>
    public static int GetNumberOfDecimalPlaces(this decimal target)
    {
        return BitConverter.GetBytes(decimal.GetBits(target)[3])[2];
    }
}
