﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using TEAM.Commons;

public static class Data
{

    private static IRandomNumberGenerator RandomNumberGenerator = new RandomNumberGenerator();
    private static readonly char[] defaultCharsForRandomString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 ".ToCharArray();

    public static void SetRandomNumberGenerator(IRandomNumberGenerator randomNumberGenerator)
    {
        Contract.Requires(randomNumberGenerator != null);
        Contract.Ensures(RandomNumberGenerator == randomNumberGenerator);

        RandomNumberGenerator = randomNumberGenerator;
    }

    public static Guid GenerateRandomGuid() => Guid.NewGuid();

    public static int GenerateRandomInt(int min = int.MinValue, int max = int.MaxValue)
    {
        Contract.Requires(min <= max);
        Contract.Ensures(Contract.Result<int>() >= min);
        Contract.Ensures(Contract.Result<int>() <= max);
        return RandomNumberGenerator.Next(min, max);
    }

    public static long GenerateRandomLong(long min = long.MinValue, long max = long.MaxValue)
    {
        Contract.Requires(min <= max);
        Contract.Ensures(Contract.Result<long>() >= min);
        Contract.Ensures(Contract.Result<long>() <= max);
        return RandomNumberGenerator.NextLong(min, max);
    }

    public static double GenerateRandomDouble(double min = double.MinValue, double max = double.MaxValue, int? decimalPlaces = null)
    {
        Contract.Requires(min <= max);
        Contract.Ensures(Contract.Result<double>() >= min);
        Contract.Ensures(Contract.Result<double>() <= max);

        var result = Math.Abs(RandomNumberGenerator.NextDouble());
        var normalizedResult = min + result * (max - min);
        if (decimalPlaces.HasValue)
        {
            normalizedResult = Math.Round(normalizedResult, decimalPlaces.Value);
        }
        return normalizedResult;
    }

    public static decimal GenerateRandomDecimal(decimal min = decimal.MinValue, decimal max = decimal.MaxValue, int? decimalPlaces = null)
    {
        Contract.Requires(min <= max);
        Contract.Ensures(Contract.Result<decimal>() >= min);
        Contract.Ensures(Contract.Result<decimal>() <= max);
        var result = Convert.ToDecimal(RandomNumberGenerator.NextDouble());
        var normalizedResult = min + result * SafeSubtractOrMax(max, min);
        if (decimalPlaces.HasValue)
        {
            normalizedResult = Math.Round(normalizedResult, decimalPlaces.Value);
        }
        return normalizedResult;
    }

    private static decimal SafeSubtractOrMax(decimal s1, decimal s2)
    {
        try
        {
            return s1 - s2;
        }
        catch (OverflowException)
        {
            return decimal.MaxValue;
        }
    }

    public static string GenerateRandomString(int minLength = 1, int maxLength = 15, params char[] validChars)
    {
        Contract.Requires(minLength <= maxLength);
        Contract.Ensures(Contract.Result<string>() != null);
        Contract.Ensures(Contract.Result<string>().Length >= minLength);
        Contract.Ensures(Contract.Result<string>().Length <= maxLength);

        if (validChars == null || !validChars.Any())
        {
            validChars = defaultCharsForRandomString;
        }
        var randomLength = GenerateRandomInt(minLength, maxLength);
        var result = new StringBuilder(randomLength);
        for (int i = 0; i < randomLength; i++)
        {
            var newChar = validChars[GenerateRandomInt(0, validChars.Length)];
            result.Append(newChar);
        }
        return result.ToString();
    }

    public static DateTime GenerateRandomDateTime(DateTime? min = null, DateTime? max = null)
    {
        Contract.Requires(min == null || max == null || min <= max);
        Contract.Ensures(Contract.Result<DateTime>() <= max);
        var validMin = min ?? new DateTime(2000, 1, 1);
        var validMax = max ?? new DateTime(2100, 12, 31);
        return new DateTime(GenerateRandomLong(validMin.Ticks, validMax.Ticks));
    }

    public static Guid GenerateDifferentFrom(params Guid[] guids)
    {
        Contract.Requires(guids != null && guids.Length != 0, "guids is null or empty.");

        while (true)
        {
            var result = Guid.NewGuid();
            if (!guids.Any(x => x == result)) return result;
        }
    }

    public static T RandomPick<T>(IList<T> elements)
    {
        Contract.Requires(elements != null, "elements is null.");
        Contract.Requires(elements.Count > 0);
        if (elements.Count == 0) throw new ArgumentException("The are no elements to pick one from. Do not pass an empty list.", nameof(elements));
        try
        {
            int index = GenerateRandomInt(0, elements.Count - 1);
            return elements[index];
        }
        catch (IndexOutOfRangeException ex)
        {
            throw new IndexOutOfRangeException("Failed to pick random element from list with " + elements.Count + " elements", ex);
        }
    }

}
