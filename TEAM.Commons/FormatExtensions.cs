﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;

public static class FormatExtensions
{
    /// <summary>
    /// Returns "NULL" if the object is null, otherwise .ToString()
    /// </summary>
    /// <typeparam name="T">A value object.</typeparam>
    /// <param name="target">The object to format.</param>
    /// <returns>"NULL" if the object is null, otherwise .ToString()</returns>
    public static string ToStringOrNull<T>(this Nullable<T> target, string nullString = "NULL") where T : struct
    {
        return target.HasValue ? target.Value.ToString() : nullString;
    }

    /// <summary>
    /// Returns "NULL" if the object is null, otherwise .ToString()
    /// </summary>
    /// <typeparam name="T">A value object.</typeparam>
    /// <param name="target">The object to format.</param>
    /// <returns>"NULL" if the object is null, otherwise .ToString()</returns>
    public static string ToStringOrNull<T>(this T target, string nullString = "NULL") where T : class
    {
        return target == null ? nullString : target.ToString();
    }

    /// <summary>
    /// Returns a string containing the values of all the properties and fields of the <paramref name="target"/> object.
    /// Very useful to output logs.
    /// </summary>
    /// <param name="target">The object to format.</param>
    /// <returns>The string with the value of the properties and fields.</returns>
    public static string PrettyFormat(this object target, int maxDeepLevel = 1)
    {
        return PrettyFormat_Internal(target, 0, maxDeepLevel, null, null);
    }

    /// <summary>
    /// Returns a string containing the values of all the properties and fields of the <paramref name="obj"/> object,
    /// excluding properties and fields specified in <paramref name="excludeFields"/>.
    /// Very useful to output logs.
    /// </summary>
    /// <param name="obj">The object to format.</param>
    /// <param name="excludeFields"></param>
    /// <returns>The string with the value of the properties and fields.</returns>
    public static string PrettyFormatExcluding(this object obj, int maxDeepLevel = 1, params string[] excludeFields)
    {
        return PrettyFormat_Internal(obj, 0, maxDeepLevel, x => excludeFields == null || !excludeFields.Contains(x.Name), x => excludeFields == null || !excludeFields.Contains(x.Name));
    }

    /// <summary>
    /// Returns a string containing the values of the properties and fields of the <paramref name="obj"/> object,
    /// specified in <paramref name="fields"/>.
    /// Very useful to output logs.
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="excludeFields"></param>
    /// <returns></returns>
    public static string PrettyFormatIncluding(this object obj, int maxDeepLevel = 1, params string[] fields)
    {
        return PrettyFormat_Internal(obj, 0, maxDeepLevel, x => fields == null || fields.Contains(x.Name), x => fields == null || fields.Contains(x.Name));
    }

    /// <summary>
    /// Returns a string containing the values of the properties and fields of the <paramref name="obj"/> object,
    /// matched by the <paramref name="propertiesSelector"/> and <paramref name="fieldsSelector"/> functions.
    /// Very useful to output logs.
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="excludeFields"></param>
    /// <returns></returns>
    public static string PrettyFormat(this object obj, Func<PropertyInfo, bool> propertiesSelector, Func<FieldInfo, bool> fieldsSelector, int maxDeepLevel = 1)
    {
        return PrettyFormat_Internal(obj, 0, maxDeepLevel, propertiesSelector, fieldsSelector);
    }
    
    /// <summary>
    /// Determines if a type is IEnumerable
    /// </summary>
    /// <param name="target">The type to check for IEnumera-bility</param>
    /// <returns>True if the type is IEnumerable. False otherwise.</returns>
    public static bool IsEnumerable(this Type target)
    {
        return typeof(IEnumerable).IsAssignableFrom(target);
    }

    private static string PrettyFormat_Internal(this object obj, int currentDeepLevel, int maxDeepLevel, Func<PropertyInfo, bool> propertiesSelector, Func<FieldInfo, bool> fieldsSelector)
    {
        var resultBuilder = new StringBuilder(128);
        if (obj == null)
        {
            resultBuilder.Append("<NULL>");
        }
        else
        {
            Type t = obj.GetType();
            if (t.IsEnum)
            {
                resultBuilder.Append(t.Name + "." + obj + "->(" + ((int)obj) + ")");
            }
            else if (t == typeof(DateTime))
            {
                resultBuilder.Append(FormatDateTime((DateTime)obj));
            }
            else if (t.IsPrimitive || t == typeof(string) || t == typeof(decimal))
            {
                resultBuilder.Append(obj.ToString());
            }
            else if (t.IsEnumerable())
            {
                PrettyFormatEnumerable(ref resultBuilder, (IEnumerable)obj, currentDeepLevel, maxDeepLevel);
            }
            else
            {
                resultBuilder.Append(t.Name + " { ");

                if (currentDeepLevel <= maxDeepLevel)
                {
                    foreach (var prop in t.GetProperties().Where(x => x.GetGetMethod() != null && x.GetGetMethod().IsPublic))
                    {
                        if (propertiesSelector == null || propertiesSelector(prop))
                        {
                            if (prop.GetIndexParameters().Length == 0)
                            {
                                object value = GetProperyValue(obj, prop);
                                PrettyFormatMember(ref resultBuilder, prop.Name, prop.PropertyType, value, currentDeepLevel + 1, maxDeepLevel);
                            }
                            else
                            {
                                resultBuilder.Append(prop.Name + "-- INDEXED PROPERTY --");
                            }
                        }
                    }
                    foreach (var prop in t.GetFields().Where(x => !x.IsInitOnly && !x.IsLiteral))
                    {
                        if (fieldsSelector == null || fieldsSelector(prop))
                        {
                            object value = GetFieldValue(obj, prop);
                            PrettyFormatMember(ref resultBuilder, prop.Name, prop.FieldType, value, currentDeepLevel + 1, maxDeepLevel);
                        }
                    }
                }
                else
                {
                    resultBuilder.Append("-- EXCLUDED (Too deep) --");
                }
                resultBuilder.Append("}");
            }
        }
        return resultBuilder.ToString();
    }

    private static string FormatDateTime(DateTime? datetime)
    {
        if (datetime == null) return "<NULL>";
        switch (datetime.Value.Kind)
        {
            case DateTimeKind.Utc:
                {
                    return datetime.Value.ToString("yyyy-MM-dd HH:mm:ss\"Z\"") + " [DateTimeKind: " + datetime.Value.Kind + "]";
                }
            case DateTimeKind.Local:
                {
                    return datetime.Value.ToString("yyyy-MM-dd HH:mm:ss zzz") + " [DateTimeKind: " + datetime.Value.Kind + "]";
                }
            case DateTimeKind.Unspecified:
                {
                    return datetime.Value.ToString("yyyy-MM-dd HH:mm:ss") + " [DateTimeKind: " + datetime.Value.Kind + "]";
                }
            default: return datetime.Value.ToString();
        }
    }

    private static object GetProperyValue(object source, PropertyInfo propertyInfo)
    {
        Contract.Requires(source != null, "source is null.");
        Contract.Requires(propertyInfo != null, "propertyInfo is null.");
        
        object result = null;
        try
        {
            result = propertyInfo.GetValue(source, null);
        }
        catch (Exception ex)
        {
            result = "Exception: '" + ex.Message;
        }
        return result;
    }

    private static object GetFieldValue(object source, FieldInfo fieldInfo)
    {
        Contract.Requires(source != null, "source is null.");
        Contract.Requires(fieldInfo != null, "fieldInfo is null.");
        
        object result = null;
        try
        {
            result = fieldInfo.GetValue(source);
        }
        catch (Exception ex)
        {
            result = "Exception: '" + ex;
        }
        return result;
    }

    private static void PrettyFormatEnumerable(ref StringBuilder output, IEnumerable target, int currentDeepLevel, int maxDeepLevel)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(output != null);
        Contract.Ensures(output != null);

        int count = 1;
        foreach (var item in target)
        {
            output.Append(
                count.ToString("00") + "->"
                + (item == null ? "<NULL>" : PrettyFormat_Internal(item, currentDeepLevel, maxDeepLevel, null, null))
                + ",");
            count++;
        }
    }

    private static void PrettyFormatMember(ref StringBuilder output, string memberName, Type memberType, object memberValue, int currentDeepLevel, int maxDeepLevel)
    {
        Contract.Requires(output != null);
        Contract.Requires(!String.IsNullOrEmpty(memberName), "memberName is null or empty.");
        Contract.Requires(memberType != null, "memberType is null.");
        Contract.Requires(memberValue != null, "memberValue is null.");
        Contract.Ensures(output == Contract.OldValue<StringBuilder>(output));
        
        if (memberValue == null)
        {
            output.Append(memberName + "=<NULL>, ");
        }
        else if (typeof(Exception).IsAssignableFrom(memberType))
        {
            output.Append(memberName + "=" + memberValue.ToString());
        }
        else if (memberType == typeof(byte[]))
        {
            output.Append(memberName + "=BINARY CONTENT (byte[]), ");
        }
        else if (memberType.IsEnum)
        {
            output.Append(memberName + "='" + memberType.Name + "." + memberValue.ToString() + "', ");
        }
        else if (memberType == typeof(SqlConnection))
        {
            output.Append(memberName + "=" + ((SqlConnection)memberValue).ConnectionStringWithoutPassword());
        }
        else if (typeof(DateTime?).IsAssignableFrom(memberType))
        {
            output.Append(memberName + "='" + FormatDateTime((DateTime?)memberValue) + "', ");
        }
        else if (memberType.IsEnumerable() && !typeof(string).IsAssignableFrom(memberType))
        {
            output.Append(memberName + "= { ");
            PrettyFormatEnumerable(ref output, (IEnumerable)memberValue, currentDeepLevel, maxDeepLevel);
            output.Append(" } ");
        }
        else if (memberType == typeof(string))
        {
            var memberValueStr = (string)memberValue;
            if (memberValueStr.Length > 128)
            {
                output.Append(memberName + "='" + memberValueStr.Substring(0, 128) + "...', ");
            }
            else
            {
                output.Append(memberName + "='" + memberValueStr + "', ");
            }
        }
        else
        {
            Type baseType = memberType;
            if (memberType.IsGenericType && memberType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            { // Is Nullable
                var genericArguments = memberType.GetGenericArguments();
                Contract.Assert(genericArguments.Length > 0);
                Contract.Assert(genericArguments[0] != null);
                baseType = genericArguments[0];
            }

            if (baseType.IsPrimitive || (baseType.Namespace != null && (baseType.Namespace.StartsWith("System") || baseType.Namespace.StartsWith("Microsoft"))))
            {
                output.Append(memberName + "='" + (memberValue == null ? "<NULL>" : memberValue.ToString()) + "', ");
            }
            else
            {
                output.Append(memberName + "='" + (memberValue == null ? "<NULL>" : memberValue.PrettyFormat_Internal(currentDeepLevel, maxDeepLevel, null, null)) + "', ");
            }
        }
    }

}
