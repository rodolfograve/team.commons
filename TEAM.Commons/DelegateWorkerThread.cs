﻿using System;
using System.Diagnostics.Contracts;

namespace TEAM.Commons
{
    public class DelegateWorkerThread : WorkerThread
    {

        /// <summary>
        /// Creates an instance of DelegateWorkerThread, ready to be Started.
        /// </summary>
        /// <param name="id">Id of this WorkerThread. Useful for logging and debugging.</param>
        /// <param name="work">Action to run in a cycle until Stop is requested.</param>
        public DelegateWorkerThread(string id, WorkerActionDelegate work)
            : base(id)
        {
            Contract.Requires(!String.IsNullOrEmpty(id), "id is null or empty.");
            Contract.Requires(work != null, "work is null.");
            
            DelegatedWork = work;
        }

        public delegate void WorkerActionDelegate();
        protected readonly WorkerActionDelegate DelegatedWork;

        protected override void DoWork()
        {
            DelegatedWork();
        }

    }
}
