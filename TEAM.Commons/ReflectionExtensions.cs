﻿namespace System.Reflection
{

    /// <summary>
    /// Convenient Reflection for type T.
    /// </summary>
    /// <typeparam name="T">The type to which reflection is to be applied.</typeparam>
    public static class Reflection<T>
    {
        /// <summary>
        /// Returns the MethodInfo if a Public or NonPublic method is found in type T with the specified name, return type, and parameters.
        /// </summary>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="returnType">The type returned by the method. Use typeof(void) for void.</param>
        /// <param name="parameterTypes">The types of the parameters.</param>
        /// <returns>MethodInfo if a method is found, null otherwise.</returns>
        public static MethodInfo GetMethodOrNull(string methodName, Type returnType, params Type[] parameterTypes) =>
            typeof(T).GetMethodOrNull(methodName, returnType, parameterTypes);

        /// <summary>
        /// Returns the MethodInfo if a void method (Action) is found in type T with the specified name and parameters.
        /// </summary>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="parameterTypes">The types of the parameters.</param>
        /// <returns>MethodInfo if a void method (Action) is found, null otherwise.</returns>
        public static MethodInfo GetActionOrNull(string methodName, params Type[] parameterTypes) =>
            typeof(T).GetActionOrNull(methodName, parameterTypes);

        /// <summary>
        /// Returns the MethodInfo if a function method (Func) is found with the specified name, return type and parameters.
        /// </summary>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="parameterTypes">The types of the parameters.</param>
        /// <returns>MethodInfo if a void method (Action) is found, null otherwise.</returns>
        public static MethodInfo GetFuncOrNull(string methodName, Type output, params Type[] parameterTypes) =>
            typeof(T).GetMethodOrNull(methodName, output, parameterTypes);

    }

    public static class ReflectionExtensions
    {

        /// <summary>
        /// Returns the MethodInfo if a Public or NonPublic method is found in type target with the specified name, return type, and parameters.
        /// </summary>
        /// <param name="target">The type that contains the method.</param>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="returnType">The type returned by the method. Use typeof(void) for void.</param>
        /// <param name="parameterTypes">The types of the parameters.</param>
        /// <returns>MethodInfo if a method is found, null otherwise.</returns>
        public static MethodInfo GetMethodOrNull(this Type target, string methodName, Type returnType, params Type[] parameterTypes)
        {
            var result = target.GetMethod(methodName,
                             BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic,
                             null,
                             parameterTypes,
                             null);
            return returnType.Equals(result?.ReturnType) ? result : null;
        }


        /// <summary>
        /// Returns the MethodInfo if a void method (Action) is found with the specified name and parameters.
        /// </summary>
        /// <param name="target">The type that contains the method.</param>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="parameterTypes">The types of the parameters.</param>
        /// <returns>MethodInfo if a void method (Action) is found, null otherwise.</returns>
        public static MethodInfo GetActionOrNull(this Type target, string methodName, params Type[] parameterTypes) =>
            target.GetMethodOrNull(methodName, typeof(void), parameterTypes);

        /// <summary>
        /// Returns the MethodInfo if a function method (Func) is found with the specified name, return type and parameters.
        /// </summary>
        /// <param name="target">The type that contains the method.</param>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="parameterTypes">The types of the parameters.</param>
        /// <returns>MethodInfo if a void method (Action) is found, null otherwise.</returns>
        public static MethodInfo GetFuncOrNull(this Type target, string methodName, Type output, params Type[] parameterTypes) =>
            target.GetMethodOrNull(methodName, output, parameterTypes);

        public static TDelegate ConvertToDelegate<TDelegate>(this MethodInfo target) where TDelegate : class =>
            target == null ? null : Delegate.CreateDelegate(typeof(TDelegate), target) as TDelegate;
        public static Action<TInput1, TInput2> ConvertToAction<TInput1, TInput2>(this MethodInfo target) =>
            target == null ? null : (Action<TInput1, TInput2>)Delegate.CreateDelegate(typeof(Action<TInput1, TInput2>), target);
        public static Action<TInput1, TInput2, TInput3> ConvertToAction<TInput1, TInput2, TInput3>(this MethodInfo target) =>
            target == null ? null : (Action<TInput1, TInput2, TInput3>)Delegate.CreateDelegate(typeof(Action<TInput1, TInput2, TInput3>), target);
        public static Func<TInput, TOutput> ConvertToFunc<TInput, TOutput>(this MethodInfo target) =>
            target == null ? null : (Func<TInput, TOutput>)Delegate.CreateDelegate(typeof(Func<TInput, TOutput>), target);
        public static Func<TInput1, TInput2, TOutput> ConvertToFunc<TInput1, TInput2, TOutput>(this MethodInfo target) =>
            target == null ? null : (Func<TInput1, TInput2, TOutput>)Delegate.CreateDelegate(typeof(Func<TInput1, TInput2, TOutput>), target);
        public static Func<TInput1, TInput2, TInput3, TOutput> ConvertToFunc<TInput1, TInput2, TInput3, TOutput>(this MethodInfo target) =>
            target == null ? null : (Func<TInput1, TInput2, TInput3, TOutput>)Delegate.CreateDelegate(typeof(Func<TInput1, TInput2, TInput3, TOutput>), target);

    }
}
