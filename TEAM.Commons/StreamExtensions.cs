﻿using System.Diagnostics.Contracts;
using System.Collections.Generic;

namespace System.IO
{
    /// <summary>
    /// Extensions to Stream and StreamReader
    /// </summary>
    public static class StreamExtensions
    {

        /// <summary>
        /// Lazily iterates over ReadLine
        /// </summary>
        /// <param name="target">The StreamReader to iterative over.</param>
        /// <returns>A lazily evaluated IEnumerable that contains all the lines in the StreamReader</returns>
        public static IEnumerable<string> StreamLines(this StreamReader target)
        {
            while (!target.EndOfStream)
            {
                yield return target.ReadLine();
            }
        }

        /// <summary>
        /// Lazily iterates over ReadLine
        /// </summary>
        /// <param name="target">The StreamReader to iterative over.</param>
        /// <returns>A lazily evaluated IEnumerable that contains all the lines in the StreamReader</returns>
        public static IEnumerable<string> StreamLines(this Stream target)
        {
            using (var streamReader = new StreamReader(target))
            {
                foreach (var line in streamReader.StreamLines())
                {
                    yield return line;
                }
            }
        }

        public static void CopyTo(this Stream target, Stream to)
        {
            Contract.Requires(target != null, "target is null.");
            Contract.Requires(to != null, "to is null.");

            var reader = new StreamReader(target);
            var writer = new StreamWriter(to);
            writer.Write(reader.ReadToEnd());
            writer.Flush();
        }
    }
}