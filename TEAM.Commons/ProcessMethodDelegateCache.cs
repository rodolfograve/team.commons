﻿namespace System.Reflection
{
    public static class ProcessMethodDelegateCache<ReceiverType>
    {
        public const string MethodName = "Process";

        public static bool IsValidProcessMethod(MethodInfo method) => method != null && !method.IsGenericMethod && !method.IsAbstract;

        public static MethodInfo GetProcessActionOrNull(params Type[] parameterTypes)
        {
            var result = Reflection<ReceiverType>.GetActionOrNull(MethodName, parameterTypes);
            return IsValidProcessMethod(result) ? result : null;
        }

        public static MethodInfo GetProcessFuncOrNull<TOutput>(params Type[] parameterTypes)
        {
            var result = Reflection<ReceiverType>.GetFuncOrNull(MethodName, typeof(TOutput), parameterTypes);
            return IsValidProcessMethod(result) ? result : null;
        }

        public static class ForAction<TInput>
        {
            public static readonly Action<ReceiverType, TInput> ProcessAction;

            static ForAction()
            {
                ProcessAction = GetProcessActionOrNull(typeof(TInput))?.ConvertToAction<ReceiverType, TInput>();
            }
        }

        public static class ForAction<TInput1, TInput2>
        {
            public static readonly Action<ReceiverType, TInput1, TInput2> ProcessAction;

            static ForAction()
            {
                ProcessAction = GetProcessActionOrNull(typeof(TInput1), typeof(TInput2))?.ConvertToAction<ReceiverType, TInput1, TInput2>();
            }
        }

        public static class ForFunc<TInput, TOutput>
        {
            public static readonly Func<ReceiverType, TInput, TOutput> ProcessFunc;

            static ForFunc()
            {
                ProcessFunc = GetProcessFuncOrNull<TOutput>(typeof(TInput))?.ConvertToFunc<ReceiverType, TInput, TOutput>();
            }
        }
        public static class ForFunc<TInput1, TInput2, TOutput>
        {
            public static readonly Func<ReceiverType, TInput1, TInput2, TOutput> ProcessFunc;

            static ForFunc()
            {
                ProcessFunc = GetProcessFuncOrNull<TOutput>(typeof(TInput1), typeof(TInput2))?.ConvertToFunc<ReceiverType, TInput1, TInput2, TOutput>();
            }
        }
    }
}
