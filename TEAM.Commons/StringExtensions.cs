﻿using System;
using System.Security;
using System.Diagnostics.Contracts;

public static class StringExtensions
{

    /// <summary>
    /// Retrieves a substring from this instance. The substring starts at a specified character position and has a specified max length.
    /// </summary>
    /// <param name="target">The string that this method extends.</param>
    /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
    /// <param name="maxLength">The maximum number of characters in the substring.</param>
    /// <returns></returns>
    [Pure]
    public static string SafeSubstring(this string target, int startIndex, int maxLength)
    {
        Contract.Ensures(Contract.Result<string>() != null);
        Contract.Ensures(Contract.Result<string>().Length <= maxLength);
      
        if (target == null)
        {
            return target;
        }
        else
        {
            if (startIndex >= target.Length)
            {
                return string.Empty;
            }
            else
            {
                int safeLength = Math.Min(target.Length - startIndex, maxLength);
                return target.Substring(startIndex, safeLength);
            }
        }
    }

    /// <summary>
    /// Creates a SecureString that represents the given string.
    /// </summary>
    /// <param name="target">The string that this method extends.</param>
    /// <returns>A SecureString instance which represents the specified string.</returns>
    [Pure]
    public static SecureString ToSecureString(this string target)
    {
        Contract.Requires(target != null);
        Contract.Ensures(Contract.Result<SecureString>() != null);
        var result = new SecureString();
        foreach (var c in target)
        {
           result.AppendChar(c);
        }
        result.MakeReadOnly();
        return result;
    }

    /// <summary>
    /// Returns null if the value is null or empty. Useful to minimize handling of multiple cases.
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    [Pure]
    public static string NormalizeEmptyToNull(this string target) => string.IsNullOrWhiteSpace(target) ? null : target;

    /// <summary>
    /// Returns string.Empty if the value is null or empty. Useful to minimize handling of multiple cases.
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    [Pure]
    public static string NormalizeNullToEmpty(this string target) => string.IsNullOrWhiteSpace(target) ? "" : target;
}
