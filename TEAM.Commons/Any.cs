﻿using System;
using System.Collections.Generic;

public static partial class Any
{
    public static Guid Guid() => Data.GenerateRandomGuid();
    public static int Int(int min = int.MinValue, int max = int.MaxValue) => Data.GenerateRandomInt(min, max);
    public static long Long(long min = long.MinValue, long max = long.MaxValue) => Data.GenerateRandomLong(min, max);
    public static double Double(double min = double.MinValue, double max = double.MaxValue, int? decimalPlaces = null) => Data.GenerateRandomDouble(min, max, decimalPlaces);
    public static decimal Decimal(decimal min = decimal.MinValue, decimal max = decimal.MaxValue, int? decimalPlaces = null) => Data.GenerateRandomDecimal(min, max, decimalPlaces);
    public static string String(int minLength = 1, int maxLength = 15, params char[] validChars) => Data.GenerateRandomString(minLength, maxLength, validChars);
    public static T Pick<T>(IList<T> elements) => Data.RandomPick(elements);
    public static DateTime DateTime(DateTime? min = null, DateTime? max = null) => Data.GenerateRandomDateTime(min, max);
}
