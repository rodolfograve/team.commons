﻿using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.Sockets
{
    /// <summary>
    /// Wraps a <seealso cref="System.Net.Sockets.SocketAsyncEventArgs"/> in order to provide awaitable operations,
    /// by implementing the C# compiler's pattern based support for awaiting
    /// Taken from https://blogs.msdn.microsoft.com/pfxteam/2011/12/15/awaiting-socket-operations/
    /// </summary>
    public class SocketAwaitable : INotifyCompletion, IDisposable
    {
        private readonly static Action SentinelAction = () => { };
        internal bool WasCompleted;
        internal Action Continuation;
        internal SocketAsyncEventArgs SocketAsyncEventArgs;

        public SocketAwaitable(int bufferSize = 0x1000)
        {
            if (bufferSize < 0) throw new ArgumentOutOfRangeException(nameof(bufferSize), $"Must be positive but found {bufferSize}");

            SocketAsyncEventArgs = new SocketAsyncEventArgs();
            SocketAsyncEventArgs.SetBuffer(new byte[bufferSize], 0, bufferSize);

            SocketAsyncEventArgs.Completed += OnSocketAsyncEventArgsCompleted;
        }

        public SocketAwaitable(SocketAsyncEventArgs socketAsyncEventArgs)
        {
            if (socketAsyncEventArgs == null) throw new ArgumentNullException(nameof(socketAsyncEventArgs));
            SocketAsyncEventArgs = socketAsyncEventArgs;

            SocketAsyncEventArgs.Completed += OnSocketAsyncEventArgsCompleted;
        }

        public Socket AcceptSocket
        {
            get
            {
                return SocketAsyncEventArgs.AcceptSocket;
            }
            set
            {
                SocketAsyncEventArgs.AcceptSocket = value;
            }
        }

        private void OnSocketAsyncEventArgsCompleted(object sender, SocketAsyncEventArgs args)
        {
            Action currentContinuation = Continuation ?? Interlocked.CompareExchange(ref Continuation, SentinelAction, null);
            currentContinuation?.Invoke();
        }

        internal void Reset()
        {
            WasCompleted = false;
            Continuation = null;
        }

        public SocketAwaitable GetAwaiter() => this;
        public bool IsCompleted => WasCompleted;

        public void OnCompleted(Action continuation)
        {
            if (Continuation == SentinelAction || Interlocked.CompareExchange(ref Continuation, continuation, null) == SentinelAction)
            {
                Task.Run(continuation);
            }
        }

        public void GetResult()
        {
            if (SocketAsyncEventArgs.SocketError != SocketError.Success)
                throw new SocketException((int)SocketAsyncEventArgs.SocketError);
        }

        public void Dispose()
        {
            if (SocketAsyncEventArgs != null)
            {
                SocketAsyncEventArgs.Completed -= OnSocketAsyncEventArgsCompleted;
            }
        }
    }
}
