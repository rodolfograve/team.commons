﻿using System.Diagnostics.Contracts;

namespace System.Net.Sockets
{
    public static class SocketExtensions
    {
        /// <summary>
        /// Returns an awaitable for the async Send operation. Use the overload that receives a <seealso cref="SocketAwaitable"/>
        /// to avoid the allocation of a new SocketAwaitable instance
        /// </summary>
        /// <param name="socket">The socket on which to send the data</param>
        /// <param name="socketAsyncEventArgs">The <seealso cref="SocketAsyncEventArgs"/> that contains the data to send</param>
        /// <returns>An awaitable for the Receive operation</returns>
        public static SocketAwaitable ReceiveAsync(this Socket socket, SocketAsyncEventArgs socketAsyncEventArgs)
        {
            Contract.Requires(socket != null);
            Contract.Requires(socketAsyncEventArgs != null);

            return socket.ReceiveAsync(new SocketAwaitable(socketAsyncEventArgs));
        }

        /// <summary>
        /// Returns the passed in <param name="awaitable" /> for the async Send operation. Reusing <seealso cref="SocketAwaitable"/> is key
        /// on very high performance scenarios in order to avoid pressure on the garbage collector
        /// </summary>
        /// <param name="socket">The socket on which to send the data</param>
        /// <param name="awaitable">The <seealso cref="SocketAwaitable"/> with the <seealso cref="SocketAsyncEventArgs"/> that contains the data to send</param>
        /// <returns>An awaitable for the Receive operation</returns>
        public static SocketAwaitable ReceiveAsync(this Socket socket, SocketAwaitable awaitable)
        {
            Contract.Requires(socket != null);
            Contract.Requires(awaitable != null);

            awaitable.Reset();
            if (!socket.ReceiveAsync(awaitable.SocketAsyncEventArgs))
                awaitable.WasCompleted = true;
            return awaitable;
        }

        /// <summary>
        /// Returns an awaitable for the async Send operation. Use the overload that receives a <seealso cref="SocketAwaitable"/>
        /// to avoid the allocation of a new SocketAwaitable instance
        /// </summary>
        /// <param name="socket">The socket on which to send the data</param>
        /// <param name="socketAsyncEventArgs">The <seealso cref="SocketAsyncEventArgs"/> that contains the data to send</param>
        /// <returns>An awaitable for the Send operation</returns>
        public static SocketAwaitable SendAsync(this Socket socket, SocketAsyncEventArgs socketAsyncEventArgs)
        {
            Contract.Requires(socket != null);
            Contract.Requires(socketAsyncEventArgs != null);

            return socket.SendAsync(new SocketAwaitable(socketAsyncEventArgs));
        }

        /// <summary>
        /// Returns the passed in <param name="awaitable" /> for the async Send operation. Reusing <seealso cref="SocketAwaitable"/> is key
        /// on very high performance scenarios in order to avoid pressure on the garbage collector
        /// </summary>
        /// <param name="socket">The socket on which to send the data</param>
        /// <param name="awaitable">The <seealso cref="SocketAwaitable"/> with the <seealso cref="SocketAsyncEventArgs"/> that contains the data to send</param>
        /// <returns>An awaitable for the Send operation</returns>
        public static SocketAwaitable SendAsync(this Socket socket, SocketAwaitable awaitable)
        {
            Contract.Requires(socket != null);
            Contract.Requires(awaitable != null);

            awaitable.Reset();
            if (!socket.SendAsync(awaitable.SocketAsyncEventArgs))
                awaitable.WasCompleted = true;
            return awaitable;
        }
    }
}
