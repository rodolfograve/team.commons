﻿using System;
using System.Diagnostics.Contracts;

public static class ConvertExtensions
{
    /// <summary>
    /// Converts <paramref name="value"/> to type T if possible. Extends the framework's method to allow conversion of nullable types.
    /// </summary>
    /// <typeparam name="T">A System.Type to convert.<paramref name="value"/> to.</typeparam>
    /// <param name="value">The object to convert to the specified type <typeparamref name="T"/>.</param>
    /// <param name="formatProvider">The FormatProvider to use for the conversion.</param>
    /// <returns>The converted value.</returns>
    public static T ChangeType<T>(object value, IFormatProvider formatProvider = null)
    {
        if (value == null)
        {
            return default(T);
        }
        else if (typeof(T).IsEnum)
        {
            return (T)Enum.Parse(typeof(T), value.ToString());
        }
        else
        {
            var conversionType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            return (T)ConvertExtensions.ChangeType(value, conversionType, formatProvider);
        }
    }

    /// <summary>
    /// Converts <paramref name="value"/> to type T if possible, using the CultureInfo.CurrentCulture format provider. Extends the framework's method to allow conversion of nullable types.
    /// </summary>
    /// <param name="conversionType">A System.Type to convert to.</param>
    /// <param name="value">The object to convert to the specified type.</param>
    /// <param name="formatProvider">The FormatProvider to use for the conversion.</param>
    /// <returns>The converted value.</returns>
    public static object ChangeType(object value, Type conversionType, IFormatProvider formatProvider = null)
    {
        Contract.Requires(conversionType != null, "conversionType is null.");

        if (value == null)
        {
            return null;
        }
        else if (conversionType.IsEnum)
        {
            return Enum.Parse(conversionType, value.ToString());
        }
        else
        {
            var realConversionType = Nullable.GetUnderlyingType(conversionType) ?? conversionType;
            try
            {
                return Convert.ChangeType(value, realConversionType, formatProvider);
            }
            catch (FormatException ex)
            {
                throw new FormatException("Error converting value '" + value + "' of type '" + value.GetType().FullName + "' into type '" + conversionType.FullName + "'", ex);
            }
        }
    }

    public static T As<T>(this object target, IFormatProvider formatProvider = null)
    {
        return ChangeType<T>(target, formatProvider);
    }
}
