﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Diagnostics.Contracts;

public static class DbCommandExtensions
{
    public static void AddParameters(this DbCommand command, object parameters, string paramsPrefix = "@")
    {
        Contract.Requires(command != null, "command is null.");
        Contract.Requires(parameters != null, "parameters is null.");
        Contract.Requires(!String.IsNullOrEmpty(paramsPrefix), "paramsPrefix is null or empty.");
        
        var existingParameters = command.Parameters.Cast<DbParameter>().ToArray();
        foreach (var property in parameters.GetType().GetProperties())
        {
            string paramName = paramsPrefix + property.Name;
            var dbParam = existingParameters.SingleOrDefault(x => x.ParameterName.Equals(paramName));
            if (dbParam == null)
            {
                dbParam = command.CreateParameter();
                dbParam.ParameterName = paramName;
                command.Parameters.Add(dbParam);
            }
            dbParam.Value = GetSafeDBValueFor(property.GetValue(parameters, null));
        }
    }

    public static string Format(this DbCommand command)
    {
        Contract.Requires(command != null, "command is null.");
        
        var result = new StringBuilder(command.CommandText);
        result.AppendLine();
        if (command.Parameters.Count > 0)
        {
            result.AppendLine("PARAMS");
            result.AppendLine("(");
            foreach (DbParameter p in command.Parameters)
            {
                result.AppendLine(p.ParameterName + "=" + p.Value.PrettyFormat() + ", ");
            }
            result.AppendLine(")");
        }
        result.AppendLine("CommandTimeout='" + command.CommandTimeout.ToString() + " seconds'");
        if (command.Connection == null)
        {
            result.AppendLine("Connection is <NULL>");
        }
        else
        {
            result.AppendLine("Connection[" + DbConnectionExtensions.ConnectionStringWithoutPassword(command.Connection) + "]");
        }
        return result.ToString();
    }

    public static void SetSafeDBValue(this DbCommand target, string paramName, object value)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(paramName), "paramName is null or empty.");
        
        target.Parameters[paramName].Value = GetSafeDBValueFor(value);
    }

    public static object GetSafeDBValueFor(object value)
    {
        return value ?? DBNull.Value;
    }

}
