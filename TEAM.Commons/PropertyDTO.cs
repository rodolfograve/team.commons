﻿using System;
using Fasterflect;

namespace TEAM.Commons
{
    public class PropertyDTO
    {

        public MemberGetter Getter { get; set; }

        public MemberSetter Setter { get; set; }

        public Type Type { get; set; }

        public string Name { get; set; }

        public Type OwnerType { get; set; }

    }
}
