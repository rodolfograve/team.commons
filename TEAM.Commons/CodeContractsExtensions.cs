﻿using TEAM.Commons;

// Use the System.Diagnostics.Contracts namespace to make sure these extension methods are available whenever
// this namespace is loaded
namespace System.Diagnostics.Contracts
{
   public static class CodeContractExtensions
   {

      [Pure]
      public static bool IsValidGuid(this string value)
      {
         Contract.Ensures(Contract.Result<bool>().Implies(value != null));
         Guid g;
         return Guid.TryParse(value, out g);
      }

      [Pure]
      public static bool IsValidLong(this string value)
      {
         Contract.Ensures(Contract.Result<bool>().Implies(value != null));
         long g;
         return long.TryParse(value, out g);
      }

      [Pure]
      public static bool IsValidInt(this string value)
      {
         Contract.Ensures(Contract.Result<bool>().Implies(value != null));
         int g;
         return int.TryParse(value, out g);
      }


   }
}
