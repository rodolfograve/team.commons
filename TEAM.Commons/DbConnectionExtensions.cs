﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Dynamic;
using System.Diagnostics;
using TEAM.Commons;
using System.Diagnostics.Contracts;

public static class DbConnectionExtensions
{

    private static readonly TraceSource Logger = new TraceSource(Tracing.TraceSourceName);

    public static IEnumerable<T> GetAllWithStreaming<T>(this DbConnection connection, string commandText, Func<T> customConstructor = null, object parameters = null, Func<IDataReader, T> customMapper = null) where T : new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");
        Contract.Ensures(Contract.Result<IEnumerable<T>>() != null);

        var cmd = connection.CreateCommandText(commandText, parameters);
        return GetAllWithStreaming<T>(connection, cmd, customConstructor ?? (() => new T()), parameters, customMapper);
    }

    public static IEnumerable<T> GetAllWithStreaming<T>(this DbConnection connection, DbCommand command, Func<T> constructor, object parameters = null, Func<IDataReader, T> customMapper = null)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(command != null, "command is null.");
        Contract.Requires(constructor != null, "customConstructor is null.");
        Contract.Ensures(Contract.Result<IEnumerable<T>>() != null);

        bool rowRead = false;
        using (var reader = ExecuteReader(connection, command))
        {
            do
            {
                T result = default(T);
                try
                {
                    rowRead = reader.Read();
                    if (rowRead)
                    {
                        result = constructor();
                        if (customMapper == null)
                        {
                            result.MapFromDataRecord(reader);
                        }
                        else
                        {
                            customMapper(reader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error executing QUERY command '" + command.Format() + "'", ex);
                }
                if (rowRead)
                {
                    yield return result;
                }
                else
                {
                    yield break;
                }
            }
            while (rowRead);
        }
        if (Logger.Listeners.Count > 0)
        {
            Logger.TraceEvent(TraceEventType.Verbose, 1, "Completed execution of QUERY command '" + command.Format());
        }
    }

    public static IEnumerable<T> GetAllFromTableWithStreaming<T>(this DbConnection connection, string tableName, Func<T> customConstructor, Func<IDataRecord, T> customMapper) where T : new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(tableName), "tableName is null or empty.");
        Contract.Ensures(Contract.Result<IEnumerable<T>>() != null);
        
        return GetAllWithStreaming<T>(connection, "select * from " + tableName, customConstructor, customMapper);
    }

    public static T GetSingleOrDefault<T>(this DbConnection connection, string commandText) where T : class, new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");

        var cmd = connection.CreateCommand();
        cmd.CommandText = commandText;
        return GetSingleOrDefault<T>(connection, cmd);
    }

    public static T GetSingleOrDefault<T>(this DbConnection connection, string commandText, object parameters) where T : class, new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");

        var cmd = connection.CreateCommandText(commandText, parameters);
        return GetSingleOrDefault<T>(connection, cmd);
    }

    public static T GetSingleOrDefault<T>(this DbConnection connection, DbCommand command) where T : class, new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(command != null, "command is null.");

        T result = null;
        using (var reader = ExecuteReader(connection, command))
        {
            if (reader.Read())
            {
                result = new T();
                result.MapFromDataRecord(reader);
            }
            if (reader.Read())
            {
                throw new InvalidOperationException("Sequence contains more than one matching element.");
            }
        }
        if (Logger.Listeners.Count > 0)
        {
            Logger.TraceEvent(TraceEventType.Verbose, 1, "Completed execution of QUERY command '" + command.Format());
        }
        return result;
    }

    public static T GetFirstOrDefault<T>(this DbConnection connection, string commandText) where T : class, new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");

        var cmd = connection.CreateCommand();
        cmd.CommandText = commandText;
        return GetFirstOrDefault<T>(connection, cmd);
    }

    public static T GetFirstOrDefault<T>(this DbConnection connection, string commandText, object parameters) where T : class, new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");

        var cmd = connection.CreateCommandText(commandText, parameters);
        return GetFirstOrDefault<T>(connection, cmd);
    }

    public static T GetFirstOrDefault<T>(this DbConnection connection, DbCommand command) where T : class, new()
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(command != null, "command is null.");

        T result = null;
        command.Connection = connection;
        using (var reader = ExecuteReader(connection, command))
        {
            if (reader.Read())
            {
                result = new T();
                result.MapFromDataRecord(reader);
            }
        }
        if (Logger.Listeners.Count > 0)
        {
            Logger.TraceEvent(TraceEventType.Stop, 1, "Completed execution of QUERY command '" + command.Format());
        }
        return result;
    }

    public static int ExecuteNonQuery(this DbConnection connection, string commandText)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");

        var command = connection.CreateCommand();
        command.CommandText = commandText;
        return ExecuteNonQuery(connection, command);
    }

    public static int ExecuteNonQuery(this DbConnection connection, string commandText, object parameters)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");

        var command = connection.CreateCommandText(commandText, parameters);
        return ExecuteNonQuery(connection, command);
    }

    public static int ExecuteNonQuery(this DbConnection connection, DbCommand command, object parameters = null)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(command != null, "command is null.");
        
        try
        {
            command.Connection = connection;
            if (Logger.Listeners.Count > 0)
            {
                Logger.TraceEvent(TraceEventType.Start, 4, "Executing NON-QUERY command '" + command.Format() + "'");
            }
            if (parameters != null)
            {
                command.AddParameters(parameters);
            }
            int result = command.ExecuteNonQuery();
            if (Logger.Listeners.Count > 0)
            {
                Logger.TraceEvent(TraceEventType.Stop, 4, "Completed execution of NON-QUERY command '" + command.Format());
            }
            return result;
        }
        catch (Exception ex)
        {
            throw new Exception("Error executing NON-QUERY command '" + command.Format() + "'", ex);
        }
    }

    public static T ExecuteScalar<T>(this DbConnection connection, string commandText)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");

        var command = connection.CreateCommand();
        command.CommandText = commandText;
        return ExecuteScalar<T>(connection, command);
    }

    public static T ExecuteScalar<T>(this DbConnection connection, string commandText, object parameters)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");
        
        var command = connection.CreateCommandText(commandText, parameters);
        return ExecuteScalar<T>(connection, command);
    }

    public static T ExecuteScalar<T>(this DbConnection connection, DbCommand command)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(command != null, "command is null.");
        
        try
        {
            command.Connection = connection;
            if (Logger.Listeners.Count > 0)
            {
                Logger.TraceEvent(TraceEventType.Start, 2, "Executing SCALAR command '" + command.Format() + "'");
            }
            object resultObj = command.ExecuteScalar();
            if (Logger.Listeners.Count > 0)
            {
                Logger.TraceEvent(TraceEventType.Stop, 2, "Completed execution of SCALAR command '" + command.Format());
            }
            T result = default(T);
            if (resultObj != null && resultObj != DBNull.Value)
            {
                result = ConvertExtensions.ChangeType<T>(resultObj);
            }
            return result;
        }
        catch (Exception ex)
        {
            throw new Exception("Error executing SCALAR command '" + command.Format() + "'", ex);
        }
    }

    public static T ExecuteSqlServerScalar<T>(string connectionString, string commandText, object commandParameters)
    {
        Contract.Requires(!String.IsNullOrEmpty(connectionString), "connectionString is null or empty.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(commandParameters != null, "commandParameters is null.");
        
        using (var sqlConnection = new SqlConnection(connectionString))
        {
            using (var command = sqlConnection.CreateCommand())
            {
                sqlConnection.Open();
                return sqlConnection.ExecuteScalar<T>(command);
            }
        }
    }

    public static DbDataReader ExecuteReader(this DbConnection connection, string commandText)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Ensures(Contract.Result<DbDataReader>() != null);
        
        var command = connection.CreateCommand();
        command.CommandText = commandText;
        return ExecuteReader(connection, command);
    }

    public static DbDataReader ExecuteReader(this DbConnection connection, DbCommand command)
    {
        Contract.Requires(connection != null, "connection is null.");
        Contract.Requires(command != null, "command is null.");
        Contract.Ensures(Contract.Result<DbDataReader>() != null);
        
        command.Connection = connection;
        try
        {
            if (Logger.Listeners.Count > 0)
            {
                Logger.TraceEvent(TraceEventType.Start, 3, "Executing READER for command '" + command.Format() + "'");
            }
            var result = command.ExecuteReader();
            return result;
        }
        catch (Exception ex)
        {
            throw new Exception("Error executing READER command '" + command.Format() + "'", ex);
        }
    }

    public static DbCommand CreateCommandText(this DbConnection target, string commandText, object parameters)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");
        Contract.Ensures(Contract.Result<DbCommand>() != null);
        
        return CreateCommand(target, commandText, CommandType.Text, parameters);
    }

    public static DbCommand CreateCommand(this DbConnection target, string commandText, CommandType commandType, object parameters)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(commandText), "commandText is null or empty.");
        Contract.Requires(parameters != null, "parameters is null.");
        Contract.Ensures(Contract.Result<DbCommand>() != null);
        
        var result = target.CreateCommand();
        result.CommandText = commandText;
        result.CommandType = commandType;
        if (parameters != null)
        {
            result.AddParameters(parameters);
        }
        return result;
    }

    public static string ConnectionStringWithoutPassword(this DbConnection target)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Ensures(Contract.Result<string>() != null);
        
        var result = new StringBuilder();
        result.Append("Server='" + target.DataSource + "',Database='" + target.Database + "',ConnectionTimeout='" + target.ConnectionTimeout.ToString() + " seconds'");
        if (target is SqlConnection)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(target.ConnectionString);
            result.Append(",User='" + builder.UserID + "'");
        }
        return result.ToString();
    }

    public static dynamic BuildDynamicFromDataRecord(IDataRecord record)
    {
        Contract.Requires(record != null, "record is null.");
        
        dynamic result = new ExpandoObject();
        var d = result as IDictionary<string, object>;
        for (int i = 0; i < record.FieldCount; i++)
            d.Add(record.GetName(i), record[i]);
        return result;
    }

}
