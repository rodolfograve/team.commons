﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TEAM.Commons
{
    /// <summary>
    /// Very simple StreamReader for CSV files. It won't handle scaping, etc.
    /// </summary>
    public class CsvStreamReader : StreamReader
    {

        /// <summary>
        /// Initializes a new instance of the TEAM.Commons.CsvStreamReader class for the specified stream and delimiter.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <param name="delimiter">The CSV delimiter.</param>
        public CsvStreamReader(Stream stream, char delimiter = ',')
            : base(stream)
        {
            Delimiter = delimiter;
        }

        /// <summary>
        /// Initializes a new instance of the TEAM.Commons.CsvStreamReader class for the specified stream, encoding and delimiter.
        /// </summary>
        /// <param name="stream">The stream to read from.</param>
        /// <param name="encoding">The encoding of the stream to read from.</param>
        /// <param name="delimiter">The CSV delimiter.</param>
        public CsvStreamReader(Stream stream, Encoding encoding, char delimiter = ',')
            : base(stream, encoding)
        {
            Delimiter = delimiter;
        }

        /// <summary>
        /// CSV delimiter.
        /// </summary>
        protected readonly char Delimiter;

        /// <summary>
        /// Reads the next line and parses it as a CSV
        /// </summary>
        public string[] ReadCsvLine() => ParseLine(ReadLine());

        /// <summary>
        /// Returns a lazily evaluated enumeration of all the lines in the stream, parsed as CSV.
        /// </summary>
        public IEnumerable<string[]> StreamCsvLines() => this.StreamLines().Select(ParseLine);

        /// <summary>
        /// Parses a single line. By default, it splits the string using the delimiter.
        /// </summary>
        /// <param name="line">The line to parse.</param>
        protected virtual string[] ParseLine(string line) => line.Split(Delimiter);

    }
}
