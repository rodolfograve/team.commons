﻿using System;
using System.Threading;

namespace TEAM.Commons.WindowsService
{
    public class ConsoleInteractiveSession : IInteractiveSession
    {
        private bool Signalled;

        public string Title
        {
            get { return Console.Title; }
            set { Console.Title = value; }
        }

        public void StopSignal() => Signalled = true;

        public void WaitForStopSignal()
        {
            while (!Console.KeyAvailable && !Signalled)
            {
                Thread.Sleep(500);
            }
        }

        public void WriteLine(string text) => Console.WriteLine(text);
    }
}
