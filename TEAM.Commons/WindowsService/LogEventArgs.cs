﻿using System;

namespace TEAM.Commons.WindowsService
{
    public class LogEventArgs : EventArgs
    {

        public LogEventArgs(string message, Exception exception = null)
        {
            Message = message;
            Exception = exception;
        }

        public readonly string Message;
        public readonly Exception Exception;

    }
}
