﻿using System;
using System.Security.Principal;

namespace TEAM.Commons.WindowsService
{
    public class InteractiveServiceController : IServiceController
    {
        public InteractiveServiceController(IService service, IInteractiveSession interactiveSession = null)
        {
            Service = service;
            InteractiveSession = interactiveSession ?? new ConsoleInteractiveSession();
        }

        private readonly IService Service;
        private readonly IInteractiveSession InteractiveSession;
        private bool IsDisposed;

        public event Action<LogEventArgs> Log;

        public void Start(string[] args = null)
        {
            try
            {
                RaiseLog("Starting in interactive mode. Changing the Principal for the AppDomain.CurrentDomain to PrincipalPolicy.WindowsPrincipal");
                AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                Service.Start(args);

                RaiseLog("Service is now running in interactive mode. Press any key to stop & exit");
                InteractiveSession.WaitForStopSignal();
                Service.Stop();
            }
            catch (Exception ex)
            {
                RaiseLog("Exception starting the service as a Console Application", ex);
                InteractiveSession.WriteLine("Press any key to close.");
                throw;
            }
        }

        public void Stop() => InteractiveSession.StopSignal();

        private void RaiseLog(string message, Exception ex = null) => Log?.Invoke(new LogEventArgs(message, ex));

        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                Service.Dispose();
            }
        }
    }
}
