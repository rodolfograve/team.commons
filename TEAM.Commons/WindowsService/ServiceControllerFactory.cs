﻿using System;

namespace TEAM.Commons.WindowsService
{
    public static class ServiceControllerFactory
    {

        /// <summary>
        /// Detects if the Host is running in a Console Application or a Windows Service,
        /// and returns an IServiceController that can start/stop the service in the appropriate mode.
        /// </summary>
        public static IServiceController For(Action<string[]> onStart, Action onStop, Action onPause = null, Action onContinue  = null, IInteractiveSession interactiveSession = null)
        {
            return For(new DelegateService(onStart, onStop, onPause, onContinue));
        }

        /// <summary>
        /// Detects if the Host is running in a Console Application or a Windows Service,
        /// and returns an IServiceController that can start/stop the service in the appropriate mode.
        /// </summary>
        public static IServiceController For(IService service, IInteractiveSession interactiveSession = null)
        {
            return IsInteractive
                    ? (IServiceController)new InteractiveServiceController(service, interactiveSession ?? new ConsoleInteractiveSession())
                    : new WindowsServiceController(service);
        }

        public static bool IsInteractive => Environment.UserInteractive;

    }
}
