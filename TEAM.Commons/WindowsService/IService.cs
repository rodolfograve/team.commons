﻿using System;

namespace TEAM.Commons.WindowsService
{
    public interface IService : IDisposable
    {
        void Start(string[] args = null);
        void Stop();
        void Pause();
        void Continue();
    }
}
