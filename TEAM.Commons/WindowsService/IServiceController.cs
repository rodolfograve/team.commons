﻿using System;

namespace TEAM.Commons.WindowsService
{
    public interface IServiceController : IDisposable
    {

        /// <summary>
        /// Blocking call to start the service. The service will be stopped as soon as this method exits.
        /// </summary>
        /// <param name="args">Args to pass in to the service Start</param>
        void Start(string[] args = null);

        /// <summary>
        /// Blocking call to stop the service. It must return once the service has been stopped.
        /// </summary>
        void Stop();

        /// <summary>
        /// An event to receive Log entries from this instance.
        /// </summary>
        event Action<LogEventArgs> Log;

    }
}
