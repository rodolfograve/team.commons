﻿using System;
using System.Security.Principal;
using System.ServiceProcess;

namespace TEAM.Commons.WindowsService
{
    public class WindowsServiceController : IServiceController
    {
        public WindowsServiceController(IService service)
        {
            WindowsServiceAdapter = new WindowsServiceAdapter(service);
            WindowsServiceAdapter.Log += RaiseLog;
        }

        private readonly WindowsServiceAdapter WindowsServiceAdapter;

        private bool IsDisposed;

        public event Action<LogEventArgs> Log;

        public void Start(string[] args = null)
        {
            try
            {
                RaiseLog("Starting as a Windows Service. Changing the Principal for the AppDomain.CurrentDomain to PrincipalPolicy.WindowsPrincipal");
                AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                ServiceBase.Run(WindowsServiceAdapter);
            }
            catch (Exception ex)
            {
                RaiseLog("Exception starting the service as a Windows Service", ex);
                throw;
            }
        }

        public void Stop()
        {
            RaiseLog($"Entering {nameof(WindowsServiceController)}.{nameof(Stop)}");
            WindowsServiceAdapter.Stop();
            RaiseLog($"Exiting {nameof(WindowsServiceController)}.{nameof(Stop)}");
        }

        private void RaiseLog(string message, Exception ex = null) => RaiseLog(new LogEventArgs(message, ex));
        private void RaiseLog(LogEventArgs e) => Log?.Invoke(e);

        public void Dispose()
        {
            if (!IsDisposed)
            {
                RaiseLog($"Entering {nameof(WindowsServiceController)}.{nameof(Dispose)}");
                IsDisposed = true;
                WindowsServiceAdapter.Dispose();
                RaiseLog($"Exiting {nameof(WindowsServiceController)}.{nameof(Dispose)}");
            }
        }
    }
}
