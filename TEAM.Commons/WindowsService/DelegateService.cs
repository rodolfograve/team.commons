﻿using System;

namespace TEAM.Commons.WindowsService
{
    public class DelegateService : IService
    {
        public DelegateService(Action<string[]> onStart, Action onStop, Action onPause = null, Action onContinue  = null)
        {
            _onStart = onStart;
            _onStop = onStop;
            _onPause = onPause;
            _onContinue = onContinue;
        }

        private readonly Action<string[]> _onStart;
        private readonly Action _onStop;
        private readonly Action _onPause;
        private readonly Action _onContinue;
        private bool IsStopping;

        public event Action ForceStopped;

        public void Start(string[] args = null) => _onStart(args);

        public void Stop()
        {
            IsStopping = true;
            _onStop();
        }

        public void ForceStop()
        {
            IsStopping = true;
            Stop();
            ForceStopped?.Invoke();
        }

        public void Pause() => _onPause?.Invoke();

        public void Continue() => _onContinue?.Invoke();

        public void Dispose()
        {
            if (!IsStopping)
            {
                Stop();
            }
        }
    }
}
