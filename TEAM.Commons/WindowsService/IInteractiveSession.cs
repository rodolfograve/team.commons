﻿namespace TEAM.Commons.WindowsService
{
    public interface IInteractiveSession
    {
        void WaitForStopSignal();
        void WriteLine(string text);
        void StopSignal();
        string Title { get; set; }
    }
}
