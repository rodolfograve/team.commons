﻿using System;
using System.Diagnostics.Contracts;
using System.ServiceProcess;
using System.Threading;

namespace TEAM.Commons.WindowsService
{
    public class WindowsServiceAdapter : ServiceBase
    {

        public WindowsServiceAdapter(IService service, bool autoLog = true)
        {
            Contract.Requires(service != null);
            Service = service;
            AutoLog = autoLog;
        }

        private readonly IService Service;
        private bool IsDisposed;

        public event Action<LogEventArgs> Log;

        protected override void OnStart(string[] args)
        {
            RaiseLog($"Entering {nameof(WindowsServiceAdapter)}.{nameof(OnStart)}");
            Service.Start(args);
            base.OnStart(args);
            RaiseLog($"Exiting {nameof(WindowsServiceAdapter)}.{nameof(OnStart)}");
        }

        protected override void OnStop()
        {
            RaiseLog($"Entering {nameof(WindowsServiceAdapter)}.{nameof(OnStop)}");
            Service.Stop();
            base.OnStop();
            Thread.Sleep(10000);
            RaiseLog($"Exiting {nameof(WindowsServiceAdapter)}.{nameof(OnStop)}");
        }

        protected override void OnPause()
        {
            Service.Pause();
            base.OnPause();
        }

        protected override void OnContinue()
        {
            Service.Continue();
            base.OnContinue();
        }

        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                if (disposing)
                {
                    Service.Dispose();
                }
                base.Dispose(disposing);
            }
        }

        private void RaiseLog(string message, Exception ex = null) => Log?.Invoke(new LogEventArgs(message, ex));
    }
}
