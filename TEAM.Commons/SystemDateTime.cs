﻿using System;

namespace TEAM.Commons
{
    /// <summary>
    /// Uses System.DateTime to implement <see cref="IDateTime"/>
    /// </summary>
    public class SystemDateTime : IDateTime
    {
        public DateTime Now => DateTime.Now;

        public DateTime UtcNow => DateTime.UtcNow;

        public DateTime Today => DateTime.Today;

        public DateTime SpecifyTime(DateTime value, DateTimeKind kind) => DateTime.SpecifyKind(value, kind);
    }
}
