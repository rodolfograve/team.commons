﻿using System;
using System.Collections.Generic;

public static class DictionaryExtensions
{
    public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> target, TKey key, Func<TKey, TValue> valueFactory)
    {
        TValue existing;
        if (!target.TryGetValue(key, out existing))
        {
            existing = valueFactory(key);
            target.Add(key, existing);
        }
        return existing;
    }

    public static TValue GetOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> target, TKey key)
    {
        TValue result;
        target.TryGetValue(key, out result);
        return result;
    }

    /// <summary>
    /// Combines the hash codes of two objects
    /// </summary>
    public static int CombineHashCodeWith<T, U>(this T target, U otherObject, int defaultOtherHashCode = 0)
    {
        unchecked
        {
            var h1 = target?.GetHashCode() ?? 0;
            var h2 = otherObject?.GetHashCode() ?? defaultOtherHashCode;
            return (((h1 << 5) + h1) ^ h2);
        }
    }
}
