﻿using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class DateTimeExtensions
    {

        public static bool IsWeekend(this DateTime target) => target.DayOfWeek == DayOfWeek.Saturday || target.DayOfWeek == DayOfWeek.Sunday;
        public static bool IsWeekday(this DateTime target) => !target.IsWeekend();
        public static bool IsFirstDayOfMonth(this DateTime date) => date.Date.Equals(date.FirstDayOfMonth());
        public static bool IsLastDayOfMonth(this DateTime date) => date.Date.Equals(date.LastDayOfMonth());

        public static DateTime FirstDayOfMonth(this DateTime dayInMonth) => new DateTime(dayInMonth.Year, dayInMonth.Month, 1);
        public static DateTime LastDayOfMonth(this DateTime dayInMonth) => new DateTime(dayInMonth.Year, dayInMonth.Month, DateTime.DaysInMonth(dayInMonth.Year, dayInMonth.Month));
        public static DateTime Next(this DateTime from) => from.AddDays(1);
        public static DateTime Previous(this DateTime from) => from.AddDays(-1);

        public static DateTime Previous(this DateTime from, DayOfWeek dayOfWeek, bool includingFrom = true) => from.GenerateSequencePastDays(includingFrom).First(x => x.DayOfWeek == dayOfWeek);
        public static DateTime PreviousSaturday(this DateTime from, bool includingFrom = true) => from.Previous(DayOfWeek.Saturday);
        public static DateTime PreviousSunday(this DateTime from, bool includingFrom = true) => from.Previous(DayOfWeek.Sunday);
        public static DateTime PreviousMonday(this DateTime from, bool includingFrom = true) => from.Previous(DayOfWeek.Monday);
        public static DateTime PreviousWeekDay(this DateTime from, bool includingFrom = true) => from.GenerateSequencePastDays().First(IsWeekday);
        public static DateTime PreviousWeekend(this DateTime from, bool includingFrom = true) => from.GenerateSequencePastDays().First(IsWeekend);

        public static DateTime Next(this DateTime from, DayOfWeek dayOfWeek, bool includingFrom = true) => from.GenerateSequenceFutureDays(includingFrom).First(x => x.DayOfWeek == dayOfWeek);
        public static DateTime NextSaturday(this DateTime from, bool includingFrom = true) => from.Next(DayOfWeek.Saturday);
        public static DateTime NextSunday(this DateTime from, bool includingFrom = true) => from.Next(DayOfWeek.Sunday);
        public static DateTime NextMonday(this DateTime from, bool includingFrom = true) => from.Next(DayOfWeek.Monday);
        public static DateTime NextWeekDay(this DateTime from, bool includingFrom = true) => from.GenerateSequenceFutureDays().First(IsWeekday);
        public static DateTime NextWeekend(this DateTime from, bool includingFrom = true) => from.GenerateSequenceFutureDays().First(IsWeekend);

        public static IEnumerable<DateTime> GenerateSequenceFutureDays(this DateTime from, bool includingFrom = true) =>
            Enumerable.Range(includingFrom ? 0 : 1, Convert.ToInt32(DateTime.MaxValue.Date.Subtract(from.Date).TotalDays)).Select(i => from.AddDays(i));

        public static IEnumerable<DateTime> GenerateSequencePastDays(this DateTime from, bool includingFrom = true) =>
            Enumerable.Range(includingFrom ? 0 : 1, Convert.ToInt32(DateTime.MaxValue.Date.Subtract(from.Date).TotalDays)).Select(i => from.AddDays(-i));

        public static IEnumerable<DateTime> GenerateSequenceFutureDaysUntil(this DateTime from, DateTime to, bool includingFrom = true)
        {
            //if (from > to) throw new ArgumentException($"{nameof(from)}={from} must be earlier than or equals to {nameof(to)}={to} in order to be able to generate the sequence of FUTURE days from '{nameof(from)}' to '{nameof(to)}'");
            //if (from == to && !includingFrom) throw new ArgumentException($"If {nameof(includingFrom)} is true then {nameof(from)}={from} must be strictly earlier than {nameof(to)}={to} in order to be able to generate the sequence of FUTURE days from '{nameof(from)}' to '{nameof(to)}' without including '{nameof(from)}'");
            var fromIndex = includingFrom ? 0 : 1;
            var totalDays = Convert.ToInt32(to.Date.Subtract(from.Date).TotalDays);
            for (int i = fromIndex; i <= totalDays; i++)
            {
                yield return from.AddDays(i);
            }
        }

        public static IEnumerable<DateTime> GenerateSequencePastDaysUntil(this DateTime from, DateTime to, bool includingFrom = true)
        {
            //if (from < to) throw new ArgumentException($"{nameof(from)}={from} must be earlier than or equals to {nameof(to)}={to} in order to be able to generate the sequence of PAST days from '{nameof(from)}' to '{nameof(to)}'");
            //if (from == to && !includingFrom) throw new ArgumentException($"If {nameof(includingFrom)} is true then {nameof(from)}={from} must be strictly earlier than {nameof(to)}={to} in order to be able to generate the sequence of PAST days from '{nameof(from)}' to '{nameof(to)}' without including '{nameof(from)}'");
            var fromIndex = includingFrom ? 0 : 1;
            var totalDays = Convert.ToInt32(from.Date.Subtract(to.Date).TotalDays);
            for (int i = fromIndex; i <= totalDays; i++)
            {
                yield return from.AddDays(-i);
            }
        }

        /// <summary>
        /// Creates a new <see cref="DateTime"/> which is a tick before <paramref name="target"/>.
        /// </summary>
        /// <param name="target">The <see cref="DateTime"/>.</param>
        /// <returns></returns>
        public static DateTime RightBefore(this DateTime target) => target.AddTicks(-1);

        /// <summary>
        /// Creates a new <see cref="DateTime"/> which is a tick after <paramref name="target"/>.
        /// </summary>
        /// <param name="target">The <see cref="DateTime"/>.</param>
        /// <returns></returns>
        public static DateTime RightAfter(this DateTime target) => target.AddTicks(1);

        /// <summary>
        /// Returns a <see cref="DateTime"/> which is a tick before <paramref name="target"/> and another one a tick after.
        /// </summary>
        /// <param name="target">The <see cref="DateTime"/>.</param>
        /// <returns></returns>
        public static IEnumerable<DateTime> GenerateRightBeforeAndAfter(this DateTime target)
        {
            yield return target.RightBefore();
            yield return target.RightAfter();
        }
    }
}
