﻿using System;

namespace TEAM.Commons
{
    /// <summary>
    /// Interface to avoid static dependencies on DateTime
    /// </summary>
    public interface IDateTime
    {
        DateTime UtcNow { get; }

        DateTime Now { get; }

        DateTime Today { get; }

        DateTime SpecifyTime(DateTime value, DateTimeKind kind);
    }
}
