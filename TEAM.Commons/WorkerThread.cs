﻿using System;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace TEAM.Commons
{
    /// <summary>
    /// Encapsulates the complexity of a thread that performs an action until it is requested to stop.
    /// </summary>
    public abstract class WorkerThread : IDisposable
    {

        /// <summary>
        /// Creates an instance of WorkerThread, ready to be Started.
        /// </summary>
        /// <param name="id">Id of this WorkerThread. Useful for logging and debugging.</param>
        /// <param name="isBackground">Is this a background thread?</param>
        public WorkerThread(string id, bool isBackground = false)
        {
            if (id == null) throw new ArgumentNullException("id");
            TraceSource = new TraceSource("TEAM.Commons.Messaging.MessageBus", SourceLevels.Information)
            {
                Switch = new SourceSwitch("TEAM.Commons.Messaging.MessageBus", "Verbose")
            };
            Id = id;
            IsBackground = isBackground;
        }

        public readonly string Id;
        public bool IsBackground { get; private set; }

        protected Thread Worker;
        protected readonly TraceSource TraceSource;
        private ManualResetEventSlim _stopWait;

        /// <summary>
        /// Has the Stop method been called to request an Stop?
        /// </summary>
        public bool StopRequested { get; protected set; }

        /// <summary>
        /// The exception thrown during the async execution.
        /// </summary>
        public Exception Error { get; protected set; }

        /// <summary>
        /// Raised when the Worker has been stopped. Contains information about any possible exception.
        /// </summary>
        public event EventHandler<RunWorkerCompletedEventArgs> Stopped;

        /// <summary>
        /// Blocks until a new thread running 'Work' in a loop is started.
        /// The thread will run until Stop is called.
        /// </summary>
        public void Start()
        {
            Worker = new Thread(new ThreadStart(RunActionInLoop)) { Name = Id, IsBackground = IsBackground };
            int managedThreadId = Worker.ManagedThreadId;
            Worker.Start();
            TraceSource.TraceStartWithCallerInfo("Starting WorkingThread '" + Id + "' with thread Id='" + managedThreadId + "'");
        }

        protected void RunActionInLoop()
        {
            try
            {
                while (!StopRequested)
                {
                    DoWork();
                }
                IsStopped = true;
                RaiseStopped(null);
            }
            catch (Exception ex)
            {
                IsStopped = true;
                RaiseStopped(ex);
            }
            finally
            {
                if (_stopWait != null)
                {
                    _stopWait.Set();
                }
            }
        }

        protected abstract void DoWork();

        protected void RaiseStopped(Exception ex)
        {
            Error = ex;
            if (Stopped != null)
            {
                Stopped(this, new RunWorkerCompletedEventArgs(null, ex, false));
            }
        }

        /// <summary>
        /// Is this Worker stopped?
        /// </summary>
        public bool IsStopped { get; protected set; }

        /// <summary>
        /// Stops this Worker after the next loop iteration is finished.
        /// </summary>
        public Task StopAsync()
        {
            return Task.Run(() =>
                {
                    if (!IsStopped)
                    {
                        _stopWait = new ManualResetEventSlim();
                        StopRequested = true;
                        _stopWait.Wait();
                        Worker = null;
                    }
                });
        }

        public void ForceStop()
        {
            IsBackground = true;
            if (Worker != null)
            {
                Worker.IsBackground = true;
            }
            StopRequested = true;
        }

        public void Dispose()
        {
            StopAsync().Wait();
        }
    }

}
