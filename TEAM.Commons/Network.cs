﻿using System.Net;
using System.Linq;
using System.Net.Sockets;

namespace TEAM.Commons
{
    public static class Network
    {

        public static IPAddress[] GetLocalIpAddresses()
        {
            var ipE = Dns.GetHostEntry(Dns.GetHostName());
            return ipE.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork).ToArray();
        }

    }
}
