﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Fasterflect;
using TEAM.Commons;
using System.Diagnostics.Contracts;

public static class MapExtensions
{
    [ThreadStatic]
    public static IDictionary<string, IEnumerable<PropertyDTO>> PropertiesSettersCache;

    public static IEnumerable<PropertyDTO> GetCachedPropertiesSettersForType(Type t)
    {
        Contract.Requires(t != null, "t is null.");
        
        if (PropertiesSettersCache == null)
        {
            PropertiesSettersCache = new Dictionary<string, IEnumerable<PropertyDTO>>();
        }
        IEnumerable<PropertyDTO> result = null;
        if (!PropertiesSettersCache.TryGetValue(t.FullName, out result))
        {
            result = t.GetProperties().Where(x => x.CanWrite).Select(x =>
                new PropertyDTO()
                {
                    Getter = t.DelegateForGetPropertyValue(x.Name),
                    Name = x.Name,
                    OwnerType = t,
                    Setter = t.DelegateForSetPropertyValue(x.Name),
                    Type = x.PropertyType
                });
            PropertiesSettersCache.Add(t.FullName, result);
        }
        return result;
    }

    public static IEnumerable<PropertyDTO> GetCachedPropertiesForType<T>()
    {
        return GetCachedPropertiesSettersForType(typeof(T));
    }

    /// <summary>
    /// Maps columns from <paramref name="dataRecord"/> into properties of <paramref name="target"/>.
    /// Columns are mapped to properties with the same name which have a setter method (public, protected or private).
    /// Very useful to map DTOs from a IDataReader. Sample:
    /// <code>
    /// var reader = command.ExecuteQuery();
    /// MyDTO dto = new MyDTO();
    /// dto.MapFromDataRecord(reader);
    /// </code>
    /// </summary>
    /// <typeparam name="T">Type of the <paramref name="target"/>.</typeparam>
    /// <param name="target">Instance of <typeparamref name="T"/> to which you want to assign values from <paramref name="dataRecord"/>.</param>
    /// <param name="dataRecord">DataRecord containing the columns to map to <paramref name="target"/>. Usually, an open DataReader.</param>
    public static void MapFromDataRecord<T>(this T target, IDataRecord dataRecord)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(dataRecord != null, "dataRecord is null.");

        MapFromDataRecordWithCustomMap(target, dataRecord, propName => propName);
    }

    /// <summary>
    /// Maps columns from <paramref name="dataRecord"/> into properties of <paramref name="target"/>,
    /// using the <paramref name="mapper"/> function to determine which column maps to which property.
    /// Very useful to map DTOs from a IDataReader. Sample:
    /// <code>
    /// var reader = command.ExecuteQuery();
    /// MyDTO dto = new MyDTO();
    /// dto.MapFromDataRecordWithCustomMap(reader, x => "P_" + x));
    /// </code>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="target"></param>
    /// <param name="dataRecord"></param>
    /// <param name="mapper"></param>
    public static void MapFromDataRecordWithCustomMap<T>(this T target, IDataRecord dataRecord, Func<string, string> mapper)
    {
        Contract.Requires(target != null);
        Contract.Requires(dataRecord != null, "dataRecord is null.");
        Contract.Requires(mapper != null, "mapper is null.");
        
        Type t = target.GetType();
        object value = null;
        foreach (var property in GetCachedPropertiesSettersForType(t))
        {
            try
            {
                string columnName = mapper(property.Name);
                if (columnName != null)
                {
                    value = GetValueFromDataRecord(dataRecord, columnName);
                    if (value != null)
                    {
                        value = ConvertExtensions.ChangeType(value, property.Type);
                        property.Setter(target, value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error assigning value '" + (value == null ? "<NULL>'" : (value.ToString() + "'[Type='" + value.GetType().FullName + "']")) + " to property '" + property.Name + "'[Type='" + property.Type.FullName + "']", ex);
            }
        }
    }

    private static object GetValueFromDataRecord(IDataRecord dataRecord, string columnName)
    {
        Contract.Requires(dataRecord != null, "dataRecord is null.");
        Contract.Requires(!String.IsNullOrEmpty(columnName), "columnName is null or empty.");

        int columnOrdinal = -1;
        for (int i = 0; i < dataRecord.FieldCount; i++)
        {
            if (dataRecord.GetName(i) == columnName)
            {
                columnOrdinal = i;
                break;
            }
        }

        object result = null;
        if (columnOrdinal != -1 && !dataRecord.IsDBNull(columnOrdinal))
        {
            result = dataRecord.GetValue(columnOrdinal);
        }
        return result;
    }
}
