﻿using System;
using System.Xml.Linq;
using System.Diagnostics.Contracts;

public static class LinqToXMLExtensions
{

    public static T GetValue<T>(this XElement target, IFormatProvider formatProvider = null)
    {
        Contract.Requires(target != null, "target is null.");
        
        string valueStr = target.Value;
        try
        {
            var result = ConvertExtensions.ChangeType<T>(valueStr, formatProvider);
            return result;
        }
        catch (FormatException formatEx)
        {
            throw new Exception("Can not convert value '" + valueStr + "' of element '" + target.Name + "' to type '" + typeof(T).FullName + "'", formatEx);
        }
        catch (Exception ex)
        {
            throw new Exception("Getting value of element '" + target.Name + "' and converting it to type '" + typeof(T).FullName + "'", ex);
        }
    }

    public static T GetAttributeValueOrDefault<T>(this XElement target, XName attributeName, T defaultValue = default(T))
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(attributeName != null, "attributeName is null.");
        
        var attribute = target.Attribute(attributeName);
        if (attribute == null)
        {
            return defaultValue;
        }
        else
        {
            var result = attribute.GetValue<T>();
            return result;
        }
    }

    public static T GetRequiredAttributeValue<T>(this XElement target, XName attributeName)
    {
        Contract.Requires(target != null, "target is null.");
        
        var attribute = target.Attribute(attributeName);
        if (attribute == null)
        {
            throw new ArgumentException("Missing required attribute '" + attributeName + "'");
        }
        else
        {
            var result = attribute.GetValue<T>();
            return result;
        }
    }

    public static T GetValue<T>(this XAttribute target, IFormatProvider formatProvider = null)
    {
        Contract.Requires(target != null, "target is null.");

        string valueStr = target.Value;
        try
        {
            var result = ConvertExtensions.ChangeType<T>(valueStr, formatProvider);
            return result;
        }
        catch (FormatException formatEx)
        {
            throw new Exception("Can not convert value '" + valueStr + "' of attribute '" + target.Name + "' of element '" + target.Parent.Name + "' to type '" + typeof(T).FullName + "'", formatEx);
        }
        catch (Exception ex)
        {
            throw new Exception("Getting value of attribute '" + target.Name + "' of element '" + target.Parent.Name + "' and converting it to type '" + typeof(T).FullName + "'", ex);
        }
    }

}
