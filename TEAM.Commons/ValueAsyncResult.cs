﻿using System;

namespace TEAM.Commons
{
    public class ValueAsyncResult : IAsyncResult
    {
        public object AsyncState { get; } = new object();

        public System.Threading.WaitHandle AsyncWaitHandle
        {
            get { throw new NotImplementedException(); }
        }

        public bool CompletedSynchronously => true;

        public bool IsCompleted { get { return true; } }
    }
}
