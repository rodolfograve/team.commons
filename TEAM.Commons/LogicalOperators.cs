﻿using System.Diagnostics.Contracts;

public static class LogicalOperators
{

    [Pure]
    public static bool Implies(this bool leftSide, bool rightSide)
    {
        return !leftSide || rightSide;
    }

}
