﻿using System.Diagnostics.Contracts;

namespace System.Collections.Generic
{
    public class DelegateEqualityComparer<T> : IEqualityComparer<T>
    {

        public DelegateEqualityComparer(Func<T, T, bool> equalsFunc, Func<T, int> hashFunc)
        {
            Contract.Requires(equalsFunc != null, "equalsFunc is null.");
            Contract.Requires(hashFunc != null, "hashFunc is null.");
            
            if (equalsFunc == null)
            {
                throw new ArgumentNullException("comparer");
            }
            if (hashFunc == null)
            {
                throw new ArgumentNullException("hashFunc");
            }
            EqualsFunc = equalsFunc;
            HashFunc = hashFunc;
        }

        protected readonly Func<T, T, bool> EqualsFunc;
        protected readonly Func<T, int> HashFunc;

        public bool Equals(T x, T y)
        {
            return EqualsFunc(x, y);
        }

        public int GetHashCode(T obj)
        {
            return HashFunc(obj);
        }
    }
}
