﻿using System;

namespace TEAM.Commons
{
    public class RandomNumberGenerator : IRandomNumberGenerator
    {

        public RandomNumberGenerator()
        {
            Generator = new Random();
        }

        private readonly Random Generator;

        public int Next(int min = int.MinValue, int max = int.MaxValue) => Generator.Next(min, max);

        public long NextLong(long min = long.MinValue, long max = long.MaxValue)
        {
            var random = Generator.NextDouble();
            random = random * (max - min);
            random = min + random;
            return Convert.ToInt64(random);
        }

        public double NextDouble() => Generator.NextDouble();
    }
}
