﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

public static class LinqExtensions
{
    /// <summary>
    /// Returns the element at index <paramref name="index"/> if the index is valid in the <paramref name="target"/> array.
    /// Returns <paramref name="defaultValue"/> if the index is outside of the array
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="target"></param>
    /// <param name="index"></param>
    /// <param name="defaultValue"></param>
    public static T GetAtIndexOrDefault<T>(this T[] target, int index, T defaultValue = default(T))
    {
        Contract.Requires(target != null, "target is null");

        return (index >= 0 && index < target.Length) ? target[index] : defaultValue;
    }


    /// <summary>
    /// Performs action <paramref name="action"/> on all the elements of target.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="target"></param>
    /// <param name="action">Action to execute on all the elements.</param>
    public static void ForEach<T>(this IEnumerable<T> target, Action<T> action)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(action != null, "action is null.");
        
        foreach (var item in target)
        {
            action(item);
        }
    }

    public static IEnumerable<T> Distinct<T>(this IEnumerable<T> target, Func<T, T, bool> equalsFunc, Func<T, int> hashFunc)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(equalsFunc != null, "equalsFunc is null.");
        Contract.Requires(hashFunc != null, "hashFunc is null.");
        
        return target.Distinct(new DelegateEqualityComparer<T>(equalsFunc, hashFunc));
    }

    public static IEnumerable<T> Distinct<T>(this IEnumerable<T> target, Func<T, object> keyValueFunc)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(keyValueFunc != null, "keyValueFunc is null.");
        
        return target.Distinct(new DelegateEqualityComparer<T>((x, y) => keyValueFunc(x).Equals(keyValueFunc(y)), x => keyValueFunc(x).GetHashCode()));
    }

    public static bool Contains<T>(this IEnumerable<T> target, T value, Func<T, T, bool> equalsFunc, Func<T, int> hashFunc)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(equalsFunc != null, "equalsFunc is null.");
        Contract.Requires(hashFunc != null, "hashFunc is null.");
        
        return target.Contains(value, new DelegateEqualityComparer<T>(equalsFunc, hashFunc));
    }

    public static bool Contains<T>(this IEnumerable<T> target, T value, Func<T, object> keyValueFunc)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(keyValueFunc != null, "keyValueFunc is null.");
        
        return target.Contains(value, new DelegateEqualityComparer<T>((x, y) => keyValueFunc(x).Equals(keyValueFunc(y)), x => keyValueFunc(x).GetHashCode()));
    }

    public static bool IsDefaultValue<T>(this T target)
    {
        return target == null || target.Equals(default(T));
    }

    /// <summary>
    /// Groups consecutive TInputs in <paramref name="orderedTarget"/> into groups of type TGroupResults. Very similar to GroupBy on ordered lists.
    /// </summary>
    /// <typeparam name="TInput">The type of the inputs</typeparam>
    /// <typeparam name="TGroupResult">The type of the group results</typeparam>
    /// <param name="orderedTarget">The IEnumerable of inputs to group</param>
    /// <param name="belongsToGroupFunc">Must return true if an input belongs to a group</param>
    /// <param name="createGroupFunc">Creates a new group from an input</param>
    /// <param name="addInputToGroupFunc">Adds an input to a group result</param>
    /// <returns>An IEnumerable of TResults with all the groups formed from the consecutive inputs belonging to the same group</returns>
    /// <example>
    /// var result = 
    /// sut.GroupConsecutive&lt;SimpleClass, SimpleClassGroup&gt;(
    ///              (x, y) => x.IntProperty == y.GlobalIntProperty,
    ///              (x) => new SimpleClassGroup(),
    ///              (x, y) => y.AddSimpleClass(x)
    ///         );
    /// </example>
    public static IEnumerable<TGroupResult> GroupConsecutive<TInput, TGroupResult>(this IEnumerable<TInput> orderedTarget, Func<TInput, TGroupResult, bool> belongsToGroupFunc, Func<TInput, TGroupResult> createGroupFunc, Action<TInput, TGroupResult> addInputToGroupFunc)
    {
        int inputCount = 0;
        TGroupResult currentResult = default(TGroupResult);
        foreach (TInput input in orderedTarget)
        {
            inputCount++;
            if (currentResult.IsDefaultValue())
            {
                currentResult = createGroupFunc(input);
                addInputToGroupFunc(input, currentResult);
            }
            else
            {
                if (belongsToGroupFunc(input, currentResult))
                {
                    addInputToGroupFunc(input, currentResult);
                }
                else
                {
                    yield return currentResult;
                    currentResult = createGroupFunc(input);
                    addInputToGroupFunc(input, currentResult);
                }
            }
        }
        if (!currentResult.IsDefaultValue())
        {
            yield return currentResult;
        }
    }

    /// <summary>
    /// Similar to LINQ's Sum but returns null if an element is null. The original Sum ignores null elements.
    /// </summary>
    public static int? SumNullable<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector) => source.Aggregate(new int?(0), (sum, x) => sum + selector(x));

    /// <summary>
    /// Similar to LINQ's Sum but returns null if an element is null. The original Sum ignores null elements.
    /// </summary>
    public static long? SumNullable<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector) => source.Aggregate(new long?(0), (sum, x) => sum + selector(x));

    /// <summary>
    /// Similar to LINQ's Sum but returns null if an element is null. The original Sum ignores null elements.
    /// </summary>
    public static decimal? SumNullable<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector) => source.Aggregate(new decimal?(0), (sum, x) => sum + selector(x));

    /// <summary>
    /// Similar to LINQ's Sum but returns null if an element is null. The original Sum ignores null elements.
    /// </summary>
    public static float? SumNullable<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector) => source.Aggregate(new float?(0), (sum, x) => sum + selector(x));

    /// <summary>
    /// Similar to LINQ's Sum but returns null if an element is null. The original Sum ignores null elements.
    /// </summary>
    public static double? SumNullable<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector) => source.Aggregate(new double?(0), (sum, x) => sum + selector(x));


    /// <summary>
    /// Convenient overload for <seealso cref="Queryable.Concat{TSource}(IQueryable{TSource}, IEnumerable{TSource})"/>
    /// Concatenates two sequences.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
    /// <param name="target">The first sequence to concatenate.</param>
    /// <param name="second">The sequence to concatenate to the first sequence.</param>
    /// <returns>An System.Linq.IQueryable`1 that contains the concatenated elements of the two input sequences.</returns>
    public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> target, params TSource[] second) =>
        Enumerable.Concat(target, second);

    /// <summary>
    /// Convenient overload for <seealso cref="Queryable.Concat{TSource}(IQueryable{TSource}, IEnumerable{TSource})"/>
    /// Flattens the sequences before concatenating them.
    /// </summary>
    /// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
    /// <param name="target">The first sequence to concatenate.</param>
    /// <param name="second">The sequence to concatenate to the first sequence.</param>
    /// <returns>An System.Linq.IQueryable`1 that contains the concatenated elements of the two input sequences.</returns>
    public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> target, params IEnumerable<TSource>[] second) =>
        Enumerable.Concat(target, second.SelectMany(x => x));


}
