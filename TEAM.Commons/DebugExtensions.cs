﻿using System.Diagnostics;
using System.Threading;

namespace System.Threading
{
    public static class DebugExtensions
    {

        /// <summary>
        /// If the Debugger is attached calls target.WaitOne without a timeout.
        /// Otherwise, calls target.WaitOne with the specified timeout.
        /// </summary>
        /// <param name="target">The target of this extension.</param>
        /// <param name="timeoutWhenDebuggerIsNotAttached">Timeout to use when the Debugger is not attached.</param>
        public static bool WaitOneDebuggerFriendly(this WaitHandle target, int timeoutWhenDebuggerIsNotAttached = 1000)
        {
            return target.WaitOne(Debugger.IsAttached ? -1 : timeoutWhenDebuggerIsNotAttached);
        }

        /// <summary>
        /// If the Debugger is attached calls target.WaitOne without a timeout.
        /// Otherwise, calls target.WaitOne with the specified timeout.
        /// </summary>
        /// <param name="target">The target of this extension.</param>
        /// <param name="timeoutWhenDebuggerIsNotAttached">Timeout to use when the Debugger is not attached.</param>
        public static bool WaitDebuggerFriendly(this ManualResetEventSlim target, int timeoutWhenDebuggerIsNotAttached = 1000)
        {
            return target.Wait(Debugger.IsAttached ? -1 : timeoutWhenDebuggerIsNotAttached);
        }

    }
}
