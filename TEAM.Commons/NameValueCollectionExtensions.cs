﻿using System;
using System.Collections.Specialized;
using System.Diagnostics.Contracts;

public static class NameValueCollectionExtensions
{
    /// <summary>
    /// Retrieves the value of an optional entry in a NameValueCollection as an instance of the specified type. Useful for accessing
    /// ConfigurationSettings.AppSettings values in a type-validated way.
    /// </summary>
    /// <typeparam name="T">The type to which the string value must be converted.</typeparam>
    /// <param name="target">The NameValueCollection this method extends.</param>
    /// <param name="name">The name of the entry to retrieve.</param>
    /// <returns>The value of the <paramref name="name"/> entry converted to type <typeparamref name="T"/> if the entry exists. default(T) otherwise.</returns>
    public static T GetOptionalValue<T>(this NameValueCollection target, string name)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(name), "name is null or empty.");
        
        var result = target[name];
        if (result == null)
        {
            return default(T);
        }
        else
        {
            return ConvertExtensions.ChangeType<T>(result);
        }
    }

    /// <summary>
    /// Retrieves the value of a required entry in a NameValueCollection as an instance of the specified type. Useful for accessing
    /// ConfigurationSettings.AppSettings values in a type-validated way.
    /// </summary>
    /// <typeparam name="T">The type to which the string value must be converted.</typeparam>
    /// <param name="target">The NameValueCollection this method extends.</param>
    /// <param name="name">The name of the entry to retrieve.</param>
    /// <returns>The value of the <paramref name="name"/> entry converted to type <typeparamref name="T"/> if the entry exists. Throws an exception otherwise.</returns>
    public static T GetRequiredValue<T>(this NameValueCollection target, string name)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(!String.IsNullOrEmpty(name), "name is null or empty.");
        
        var result = target[name];
        if (result == null)
        {
            throw new ArgumentException("Missing required element: '" + name + "'");
        }
        return ConvertExtensions.ChangeType<T>(result);
    }
}
