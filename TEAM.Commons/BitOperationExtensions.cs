﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class BitOperationsExtensions
{

    /// <summary>
    /// Calculates the Hamming Weigth of the input. https://en.wikipedia.org/wiki/Hamming_weight
    /// </summary>
    /// <param name="target"></param>
    /// <returns>The number of set bits in <paramref name="target"/> </returns>
    public static int GetHammingWeight(this int target)
    {
        target = target - ((target >> 1) & 0x55555555);                    // reuse input as temporary
        target = (target & 0x33333333) + ((target >> 2) & 0x33333333);     // temp
        var result = ((target + (target >> 4) & 0xF0F0F0F) * 0x1010101) >> 24; // count
        return result;
    }

    /// <summary>
    /// Calculates the Hamming Weigth of the input. https://en.wikipedia.org/wiki/Hamming_weight
    /// </summary>
    /// <param name="target"></param>
    /// <returns>The number of set bits in <paramref name="target"/> </returns>
    public static int GetHammingWeight(this uint target)
    {
        target = target - ((target >> 1) & 0x55555555);                    // reuse input as temporary
        target = (target & 0x33333333) + ((target >> 2) & 0x33333333);     // temp
        var result = ((target + (target >> 4) & 0xF0F0F0F) * 0x1010101) >> 24; // count
        return (int)result;
    }

    /// <summary>
    /// Calculates the Hamming Weigth of the input. https://en.wikipedia.org/wiki/Hamming_weight
    /// </summary>
    /// <param name="target"></param>
    /// <returns>The number of set bits in <paramref name="target"/> </returns>
    //public static int GetHammingWeight(this long target)
    //{
    //    target = target - ((target >> 1) & (long)~(long)0 / 3);                           // temp
    //    target = (target & (long)~(long)0 / 15 * 3) +((target >> 2) & (long)~(long)0 / 15 * 3);      // temp
    //    target = (target + (target >> 4)) & (long)~(long)0 / 255 * 15;                      // temp
    //    var result = (long)(target * ((long)~(long)0 / 255)) >> (sizeof(long) - 1) * 8; // count
    //    return (int)result;
    //}
}
