﻿using System;
using System.Diagnostics.Contracts;

namespace TEAM.Commons
{
    [ContractClass(typeof(IRandomNumberGeneratorContract))]
    public interface IRandomNumberGenerator
    {

        int Next(int min = int.MinValue, int max = int.MaxValue);

        long NextLong(long min = long.MinValue, long max = long.MaxValue);

        double NextDouble();

    }

    [ContractClassFor(typeof(IRandomNumberGenerator))]
    public abstract class IRandomNumberGeneratorContract : IRandomNumberGenerator
    {
        public int Next(int min = int.MinValue, int max = int.MaxValue)
        {
            Contract.Requires(min <= max);
            Contract.Ensures(Contract.Result<int>() >= min);
            Contract.Ensures(Contract.Result<int>() <= max);
            throw new NotImplementedException();
        }

        public long NextLong(long min = long.MinValue, long max = long.MaxValue)
        {
            Contract.Requires(min <= max);
            Contract.Ensures(Contract.Result<long>() >= min);
            Contract.Ensures(Contract.Result<long>() <= max);
            throw new NotImplementedException();
        }

        public double NextDouble()
        {
            Contract.Ensures(Contract.Result<double>() >= 0);
            Contract.Ensures(Contract.Result<double>() <= 1.0);
            throw new NotImplementedException();
        }
    }

}
