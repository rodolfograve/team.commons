﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TEAM.Commons
{
    /// <summary>
    /// A reflection-based double dispatcher, that caches the reflection information.
    /// </summary>
    /// <typeparam name="ReceiverType">The type of the objects to which the messages will be dispatched.</typeparam>
    public class ReflectionDoubleDispatcher<ReceiverType> : IDoubleDispatcher<ReceiverType>
    {

        public ReflectionDoubleDispatcher(string methodName)
        {
            DispatchedMethodName = methodName;
            DispatchedMethodsDictionary = new Dictionary<Type, MethodInfo>();
            var instanceType = typeof(ReceiverType);
            var processMethods = instanceType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic)
                                             .Where(x => x.Name.Equals(methodName, StringComparison.Ordinal));

            foreach (var method in processMethods)
            {
                var paramaters = method.GetParameters();
                if (!method.IsGenericMethod && !method.IsAbstract && paramaters.Length == 1)
                {
                    DispatchedMethodsDictionary.Add(paramaters[0].ParameterType, method);
                }
            }
        }

        private readonly string DispatchedMethodName;
        private readonly Dictionary<Type, MethodInfo> DispatchedMethodsDictionary;

        public void DispatchStrict(object message, ReceiverType receiverInstance) => Dispatch(message, receiverInstance, true);
        public void DispatchIfTargetFound(object message, ReceiverType receiverInstance) => Dispatch(message, receiverInstance, true);

        private void Dispatch(object message, ReceiverType receiverInstance, bool strict)
        {
            var messageType = message.GetType();
            var targetMethod = DispatchedMethodsDictionary.GetOrDefault(messageType);
            if (targetMethod == null)
            {
                if (strict)
                {
                    throw new ArgumentOutOfRangeException(nameof(message), $"Type {typeof(ReceiverType).Name} has no method {DispatchedMethodName} accepting a parameter of type {messageType}");
                }
            }
            else
            {
                targetMethod.Invoke(receiverInstance, new[] { message });
            }
        }

    }
}
