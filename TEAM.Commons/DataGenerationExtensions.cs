﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

public static class DataGenerationExtensions
{

    public static string GenerateDifferent(this string target)
    {
        return "x" + target;
    }

    public static string GenerateDifferentWithSameLength(this string target)
    {
        Contract.Requires(!String.IsNullOrEmpty(target), "target is null or empty.");

        if (string.IsNullOrEmpty(target))
        {
            throw new ArgumentException("Cannot generate a string different from null or empty keeping the same length");
        }
        else
        {
            if (target[0] == 'x' || target[0] == 'X')
            {
                return "_" + target.Substring(1);
            }
            else
            {
                return "x" + target.Substring(1);
            }
        }
    }

    public static Guid GenerateDifferent(this Guid target)
    {
        return Data.GenerateDifferentFrom(target);
    }

    public static int GenerateDifferent(this int target)
    {
        return target == int.MaxValue ? target - 1 : target + 1;
    }

    public static long GenerateDifferent(this long target)
    {
        return target == long.MaxValue ? target - 1 : target + 1;
    }

    public static double GenerateDifferent(this double target)
    {
        return target == double.MaxValue ? target - 1 : target + 1;
    }

    public static Guid Different(this Guid target)
    {
        Contract.Ensures(!Contract.Result<Guid>().Equals(target));
        return Data.GenerateDifferentFrom(target);
    }

    public static T RandomPick<T>(this IList<T> target)
    {
        return Data.RandomPick(target);
    }

}
