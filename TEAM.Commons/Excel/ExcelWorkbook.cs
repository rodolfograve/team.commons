﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace TEAM.Commons.Excel
{
    public class ExcelWorkbook
    {

        public static ExcelWorkbook WithWorksheets(IEnumerable<ExcelWorksheet> worksheets)
        {
            return new ExcelWorkbook()
            {
                Worksheets = worksheets
            };
        }

        public static ExcelWorkbook WithWorksheets(params ExcelWorksheet[] worksheets)
        {
            return WithWorksheets((IEnumerable<ExcelWorksheet>)worksheets);
        }

        public IEnumerable<ExcelWorksheet> Worksheets { get; set; }

        [Obsolete("Use the GenerateAsMsPropietaryXml method")]
        public void Generate(Stream outputStream, Encoding encoding)
        {
            GenerateAsMsPropietaryXml(outputStream, encoding);
        }

        public void GenerateAsMsPropietaryXml(Stream outputStream, Encoding encoding)
        {
            using (var writer = new XmlTextWriter(outputStream, encoding))
            {
                writer.WriteStartDocument();
                writer.WriteProcessingInstruction("mso-application", "progid=\"Excel.Sheet\"");
                writer.WriteStartElement("Workbook");
                writer.WriteAttributeString("xmlns", "urn:schemas-microsoft-com:office:spreadsheet");
                writer.WriteAttributeString("xmlns:o", "urn:schemas-microsoft-com:office:office");
                writer.WriteAttributeString("xmlns:x", "urn:schemas-microsoft-com:office:excel");
                writer.WriteAttributeString("xmlns:ss", "urn:schemas-microsoft-com:office:spreadsheet");
                writer.WriteAttributeString("xmlns:html", "http://www.w3.org/TR/REC-html40");

                foreach (var worksheet in Worksheets)
                {
                    writer.WriteStartElement("Worksheet");
                    if (worksheet == null)
                    {
                        writer.WriteAttributeString("ss:Name", string.Empty);
                    }
                    else
                    {
                        writer.WriteAttributeString("ss:Name", worksheet.Name);

                        foreach (var table in worksheet.Tables)
                        {
                            writer.WriteStartElement("Table");
                            writer.WriteAttributeString("x:FullColumns", "1");
                            writer.WriteAttributeString("x:FullRows", "1");
                            if (table == null)
                            {
                            }
                            else
                            {
                                foreach (var row in table.Rows)
                                {
                                    writer.WriteStartElement("Row");
                                    if (row != null)
                                    {
                                        foreach (var cell in row.Cells)
                                        {
                                            writer.WriteStartElement("Cell");
                                            if (cell != null)
                                            {
                                                foreach (var data in cell.Data)
                                                {
                                                    writer.WriteStartElement("Data");
                                                    if (data == null)
                                                    {
                                                        writer.WriteAttributeString("ss:Type", "String");
                                                        writer.WriteString(string.Empty);
                                                    }
                                                    else
                                                    {
                                                        writer.WriteAttributeString("ss:Type", data.Type);
                                                        writer.WriteString(data.Content);
                                                    }
                                                    writer.WriteEndElement();
                                                }
                                            }
                                            writer.WriteEndElement();
                                        }
                                    }
                                    writer.WriteEndElement();
                                }
                            }
                            writer.WriteEndElement();
                        }
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        //public void GenerateAsOpenXML(Stream outputStream, Encoding encoding)
        //{
        //    using (var writer = new XmlTextWriter(outputStream, encoding))
        //    {
        //        writer.WriteStartDocument();
        //        writer.WriteStartElement("x:workbook");
        //        writer.WriteAttributeString("xmlns:r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
        //        writer.WriteAttributeString("xmlns:x", "http://schemas.openxmlformats.org/spreadsheetml/2006/main");

        //        writer.WriteStartElement("x:sheets");
        //        foreach (var worksheet in Worksheets)
        //        {
        //            writer.WriteStartElement("x:worksheet");
        //            if (worksheet == null)
        //            {
        //                writer.WriteAttributeString("name", string.Empty);
        //            }
        //            else
        //            {
        //                writer.WriteAttributeString("name", worksheet.Name);

        //                foreach (var table in worksheet.Tables)
        //                {
        //                    writer.WriteStartElement("Table");
        //                    writer.WriteAttributeString("x:FullColumns", "1");
        //                    writer.WriteAttributeString("x:FullRows", "1");
        //                    if (table == null)
        //                    {
        //                    }
        //                    else
        //                    {
        //                        foreach (var row in table.Rows)
        //                        {
        //                            writer.WriteStartElement("Row");
        //                            if (row != null)
        //                            {
        //                                foreach (var cell in row.Cells)
        //                                {
        //                                    writer.WriteStartElement("Cell");
        //                                    if (cell != null)
        //                                    {
        //                                        foreach (var data in cell.Data)
        //                                        {
        //                                            writer.WriteStartElement("Data");
        //                                            if (data == null)
        //                                            {
        //                                                writer.WriteAttributeString("ss:Type", "String");
        //                                                writer.WriteString(string.Empty);
        //                                            }
        //                                            else
        //                                            {
        //                                                writer.WriteAttributeString("ss:Type", data.Type);
        //                                                writer.WriteString(data.Content);
        //                                            }
        //                                            writer.WriteEndElement();
        //                                        }
        //                                    }
        //                                    writer.WriteEndElement();
        //                                }
        //                            }
        //                            writer.WriteEndElement();
        //                        }
        //                    }
        //                    writer.WriteEndElement();
        //                }
        //            }
        //            writer.WriteEndElement();
        //        }
        //        writer.WriteEndElement();
        //        writer.WriteEndElement();
        //    }
        //}

    }
}
