﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Excel
{
    public class ExcelCell
    {

        public static ExcelCell String(string content)
        {
            return new ExcelCell()
            {
                Data = new ExcelData[]
                {
                    new ExcelData()
                    {
                        Type = "String",
                        Content = content
                    }
                }
            };
        }

        public static ExcelCell Empty()
        {
            return new ExcelCell()
            {
                Data = new ExcelData[]
                {
                    new ExcelData()
                    {
                        Type = "String",
                        Content = string.Empty
                    }
                }
            };
        }

        public IEnumerable<ExcelData> Data { get; set; }

    }
}
