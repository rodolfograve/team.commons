﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Excel
{
    public class ExcelWorksheet
    {

        public static ExcelWorksheet WithRows(string name, IEnumerable<ExcelRow> rows)
        {
            return new ExcelWorksheet()
            {
                Name = name,
                Tables = new ExcelTable[]
                {
                    new ExcelTable()
                    {
                        Rows = rows
                    }
                }
            };
        }

        public static ExcelWorksheet WithRows(string name, params ExcelRow[] rows)
        {
            return WithRows(name, (IEnumerable<ExcelRow>)rows);
        }

        public string Name { get; set; }

        public IEnumerable<ExcelTable> Tables { get; set; }

    }
}
