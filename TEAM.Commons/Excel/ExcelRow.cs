﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Excel
{
    public class ExcelRow
    {

        public static ExcelRow WithCells(IEnumerable<ExcelCell> cells)
        {
            return new ExcelRow()
            {
                Cells = cells
            };
        }

        public static ExcelRow WithCells(params ExcelCell[] cells)
        {
            return WithCells((IEnumerable<ExcelCell>)cells);
        }

        public IEnumerable<ExcelCell> Cells { get; set; }

    }
}
