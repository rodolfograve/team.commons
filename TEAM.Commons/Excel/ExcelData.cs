﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEAM.Commons.Excel
{
    public class ExcelData
    {

        public string Type { get; set; }

        public string Content { get; set; }

    }
}
