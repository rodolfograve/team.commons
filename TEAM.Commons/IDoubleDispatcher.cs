﻿namespace TEAM.Commons
{
    /// <summary>
    /// An implementation of a double dispatching algorithm:
    /// https://en.wikipedia.org/wiki/Double_dispatch
    /// </summary>
    /// <typeparam name="ReceiverType"></typeparam>
    public interface IDoubleDispatcher<ReceiverType>
    {

        /// <summary>
        /// Dispatches the message to the correct method of the receiver instance.
        /// Throws if no method can be found.
        /// </summary>
        /// <param name="message">The message to dispatch.</param>
        /// <param name="receiverInstance">The instance to which the message will be dispatched.</param>
        void DispatchStrict(object message, ReceiverType receiverInstance);

        /// <summary>
        /// Dispatches the message to the correct method of the receiver instance.
        /// Does nothing if no method can be found.
        /// </summary>
        /// <param name="message">The message to dispatch.</param>
        /// <param name="receiverInstance">The instance to which the message will be dispatched.</param>
        void DispatchIfTargetFound(object message, ReceiverType receiverInstance);

    }
}
