﻿using System.Diagnostics.Contracts;

public static class ComparisonExtensions
{

    /// <summary>
    /// Compares 2 objects by comparing their public properties. Properties are obtained using reflection
    /// in order to make this method work on anonymous types.
    /// </summary>
    /// <param name="target">The first object to compare.</param>
    /// <param name="other">The second object to compare.</param>
    /// <returns>True if all the public properties of both objects to compare have the same value. False otherwise.</returns>
    public static bool AllPublicPropertiesEquals(this object target, object other)
    {
        Contract.Requires(target != null, "target is null.");
        Contract.Requires(other != null, "other is null.");

        var otherType = other.GetType();
        foreach (var prop in target.GetType().GetProperties())
        {
            var originalValue = prop.GetValue(target, null);

            var otherProperty = otherType.GetProperty(prop.Name);
            if (otherProperty == null) // 'target' has a property not available in 'other'
            {
                return false;
            }

            var otherValue = otherProperty.GetValue(other, null);
            if (originalValue == null)
            {
                return otherValue == null;
            }
            else if (!originalValue.Equals(otherValue))
            {
                return false;
            }
        }
        return true;
    }

}
