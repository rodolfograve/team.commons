﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

public static class TraceExtensions
{

    public static void TraceStartWithCallerInfo(this TraceSource target, string message = "", [CallerMemberName]string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0)
    {
        target.TraceEventWithCallerInfo(TraceEventType.Start, message, callerMemberName, callerFilePath, callerLineNumber);
    }

    public static void TraceStopWithCallerInfo(this TraceSource target, string message = "", [CallerMemberName]string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0)
    {
        target.TraceEventWithCallerInfo(TraceEventType.Stop, message, callerMemberName, callerFilePath, callerLineNumber);
    }

    public static void TraceCriticalWithCallerInfo(this TraceSource target, Exception ex, string message = "", [CallerMemberName]string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0)
    {
        target.TraceEventWithCallerInfo(TraceEventType.Critical, message + "->" + ex, callerMemberName, callerFilePath, callerLineNumber);
    }

    public static void TraceErrorWithCallerInfo(this TraceSource target, Exception ex, string message = "", [CallerMemberName]string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0)
    {
        target.TraceEventWithCallerInfo(TraceEventType.Error, message + "->" + ex, callerMemberName, callerFilePath, callerLineNumber);
    }

    public static void TraceEventWithCallerInfo(this TraceSource target, TraceEventType eventType, string message = "", [CallerMemberName]string callerMemberName = "", [CallerFilePath] string callerFilePath = "", [CallerLineNumber]int callerLineNumber = 0)
    {
        target.TraceEvent(eventType, 0, BuildTraceMessage(eventType, message, callerMemberName, callerFilePath, callerLineNumber));
    }

    public static string BuildTraceMessage(TraceEventType eventType, string message, string callerMemberName, string callerFilePath, int callerLineNumber)
    {
        return string.Format("[{1}@{2}-{0}] says: {3}.", callerMemberName, Path.GetFileName(callerFilePath), callerLineNumber, message);
    }

}
