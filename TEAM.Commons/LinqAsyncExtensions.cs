﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.Linq
{
    public static class LinqAsyncExtensions
    {
        /// <summary>
        /// Applies the <paramref name="selector"/> asynchronously to each item in <paramref name="target"/> 
        /// and returns an array with the resulting elements.
        /// The order of the elements is not guaranteed and will be the order in which the async operations
        /// are completed.
        /// </summary>
        /// <typeparam name="T">Type of the input elements.</typeparam>
        /// <typeparam name="TResult">Type of the output elements.</typeparam>
        /// <param name="target">The input enumerable.</param>
        /// <param name="selector">The async selector to apply to every item in <param name="target" />
        /// <returns>An array that contains the result of asynchronous evaluation of <param name="selector" /> to every item in <param name="target"/></returns>
        public static async Task<TResult[]> SelectAsync<T, TResult>(this IEnumerable<T> target, Func<T, Task<TResult>> selector)
        {
            var tasks = target.Select(async x => await selector(x).ConfigureAwait(false));
            var result = await Task.WhenAll(tasks);
            return result;
        }

        /// <summary>
        /// Filters a sequence of values based on an async predicate.
        ///   predicate:
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source</typeparam>
        /// <param name="target">An System.Collections.Generic.IEnumerable`1 to filter</param>
        /// <param name="predicateAsync">An async function to test each element for a condition.</param>
        /// <returns>An System.Collections.Generic.IEnumerable`1 that contains elements from the input sequence that satisfy the condition.</returns>
        public static async Task<IEnumerable<TSource>> WhereAsync<TSource>(this IEnumerable<TSource> target, Func<TSource, Task<bool>> predicateAsync)
        {
            var tasks = target.Select(async x => new { Item = x, PredicateResult = await predicateAsync(x).ConfigureAwait(false)});
            var predicateResults = await Task.WhenAll(tasks);
            var result = predicateResults.Where(x => x.PredicateResult == true).Select(x => x.Item);
            return result;
        }
    }
}
