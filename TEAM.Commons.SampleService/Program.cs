﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;
using TEAM.Commons.Logging;
using TEAM.Commons.WindowsService;

namespace TEAM.Commons.SampleService
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            var entryAssemblyName = entryAssembly.GetName();
            var uri = new UriBuilder(entryAssembly.CodeBase);
            string codeBasePath = Uri.UnescapeDataString(uri.Path);

            using (var dailyRollingFileLog = new DailyRollingFileLog(Path.GetDirectoryName(codeBasePath), "TEAM.Commons.SampleService", "log", new JsonLogEntrySerializer(new JsonSerializer()), new SystemDailyRollingFileLogDateTime()))
            {
                ILog log = dailyRollingFileLog;

                if (ServiceControllerFactory.IsInteractive)
                {
                    log = new CompositeLog(ConsoleLog.WithRandomColorsPerTag(), log);
                }

                //log = new ConcurrentLogDecorator(log);
                using (var serviceController = ServiceControllerFactory.For(new MyService(log)))
                {
                    serviceController.Log += e => log.Log(e.Message, e.Exception);
                    serviceController.Start();
                }
            }
        }
    }
}
