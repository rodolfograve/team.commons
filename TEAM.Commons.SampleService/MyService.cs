﻿using System.Diagnostics;
using System.Threading;
using TEAM.Commons.Logging;
using TEAM.Commons.WindowsService;

namespace TEAM.Commons.SampleService
{
    public class MyService : IService
    {
        public MyService(ILog log)
        {
            Log = log;
            WorkerThread = new DelegateWorkerThread("MyService", DoWork);
        }

        private readonly ILog Log;
        private readonly WorkerThread WorkerThread;

        public void Continue()
        {
        }

        public void Pause()
        {
        }

        public void Start(string[] args)
        {
            WorkerThread.Start();
        }

        private void DoWork()
        {
            for (int i = 0; i < 10; i++)
            {
                Log.Log("The service is still running", tags: $"Tag{i}");
            }
            Thread.Sleep(500);
        }

        public void Stop()
        {
            WorkerThread.ForceStop();
            Log.Log("Service has been stopped");
        }

        public void Dispose()
        {
            var stackTrace = new StackTrace();
            Log.Log($"Service has been disposed with StackTrace {stackTrace}");
        }
    }
}
